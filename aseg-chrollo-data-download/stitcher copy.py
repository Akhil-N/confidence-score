import os
import glob
import tqdm
import math
#import gdal
from multiprocessing import Pool,Process,Lock
from os import sep as separator
import time
import shutil

def num2deg(xtile, ytile, zoom):
  n = 2.0 ** zoom
  lon_deg = xtile / n * 360.0 - 180.0
  lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * ytile / n)))
  lat_deg = math.degrees(lat_rad)
  return (lat_deg, lon_deg)

def extractxyz(filepath):
    z, x, y = filepath.split('{}'.format(separator))[-1].split('_')
    y = os.path.splitext(y)[0]
    return int(x), int(y), z[-2:]  #Works all two digit zoom levels

def main(opts,filepaths,index,num_processes,temp_folder_path,num_columns,num_rows):
    print ("main called")
    total_length = len(filepaths)
    # print(total_length)
    #batch_processes = num_rows*(math.ceil(num_columns/num_processes))

    #batch_processes = int(math.ceil(total_length/num_processes))
    # print ("bat",batch_processes,num_rows)

    # print(index*batch_processes, (index+1)*batch_processes)

    # This section decides the number of images to be sent to each process
    new_list = []
    y_val = 0
    first_x = extractxyz(filepaths[0])[0]
    for i in range(0, len(filepaths)):
        x = extractxyz(filepaths[i])[0]
        if x == first_x:
            y_val = y_val + 1        
        else:
            first_x = x
            new_list.append(y_val)
            y_val = y_val+1
    new_list.append(y_val)
                    
    threshold = int(math.ceil(total_length / num_processes)+1)  # upper limit on the number of images for each process
    threshold_list = []
    for i in range(0, num_processes):
        threshold_list.append(threshold * (i+1))
          
    batch_list = []
    for k in range(0, len(threshold_list)):
        for j in range(0, len(new_list)):
            if new_list[j] > threshold_list[k]:
                batch_list.append(new_list[j-1])
                break
    batch_list.append(new_list[-1])
    batch_list.insert(0, 0)
    

    #for filepath in tqdm.tqdm(filepaths[int(index*batch_processes):int((index+1)*batch_processes)]):
    for filepath in tqdm.tqdm(filepaths[batch_list[index]:batch_list[index+1]]):
        # print (filepath)
        x, y,z = extractxyz(filepath)
        UPPER_LAT, LEFT_LON = num2deg(x, y, int(z))
        LOWER_LAT, RIGHT_LON = num2deg(x+1, y+1, int(z))
        #gdal.TranslateOptions()
        if opts['make_sub_folder'] == False:
            os.system(
                 'gdal_translate -of Gtiff -a_ullr {} {} {} {} {} {} > /dev/null'.format(
                     LEFT_LON, UPPER_LAT, RIGHT_LON, LOWER_LAT, filepath,os.path.join(temp_folder_path,'{}_{}_{}.tif'.format(z,x,y))))
        else:
            os.system('gdal_translate -of Gtiff -a_ullr {} {} {} {} {} {} > /dev/null'.format(
                     LEFT_LON, UPPER_LAT, RIGHT_LON, LOWER_LAT, filepath,os.path.join(temp_folder_path,'={}'.format(index),'{}_{}_{}.tif'.format(z,x,y))))

def merging(opts,index,temp_folder_path, input_dir, stitched_dir):
    os.system('gdalbuildvrt {} {} > /dev/null'.format(
        os.path.join(opts['output_dir'],'mosaic{}.vrt'.format(index)),
        os.path.join(temp_folder_path,"{}".format(index),'*.tif')
    ))
    os.system('gdal_translate -of GTiff  -a_srs EPSG:4326 {} {}'.format(
        os.path.join(opts['output_dir'] ,'mosaic{}.vrt'.format(index)),
        os.path.join(opts['output_dir'],opts['output_name']+"_{}".format(index+1)+".tif")))

def update_merged_tifs_list(tiffs_dir, name):
	"""
	Adds the name of the last merged tiff to the file 'merged_tifs.txt'
	If the file does not exists, creates one.
	Arguments:
		tiffs_dir {string} -- path to directory containing merged tifs
		tif {string} -- name of the last merged tiff
	"""
	txt_file = os.path.join(tiffs_dir,"merged_tifs.txt")
	if not os.path.isfile(txt_file):
		file = open(txt_file, "w")
		file.close()
	with open(txt_file, "a") as file:        		
		file.write(name)
		file.write(",")

def merged_tifs_list(tiffs_dir, tif):
	"""
	Returns the list conataining names of the tifs which has been processed.	
	Arguments:
		tiffs_dir {string} -- path to directory containing merged tifs
		tif {string} -- name of the tiff currently processed		
	"""
	txt_file = os.path.join(tiffs_dir,"merged_tifs.txt")
	if os.path.isfile(txt_file):
		file = open(txt_file, "r")
		tifs_list = file.read().split(",")
		file.close()
		#print("processed tifs", tifs_list)
	else:
		tifs_list = []
	return tifs_list


def stitcher(masterDir):
    input_dir = os.path.join(masterDir,'downloaded')
    stitched_dir = input_dir.replace("downloaded", "stitched")   
    tifs_dir = input_dir.replace("downloaded", "tifs")  
    os.makedirs(stitched_dir,exist_ok=True)
    os.makedirs(tifs_dir,exist_ok=True)
    aoi_downloaded = os.listdir(input_dir)
    # aoi_downloaded.sort()
    for j in range(0, len(aoi_downloaded)):
        aoi_downloaded[j] = int(aoi_downloaded[j])
    
    aoi_downloaded.sort()

    for k in range(0, len(aoi_downloaded)):
        aoi_downloaded[k] = str(aoi_downloaded[k])
    # print(aoi_downloaded)
    # for i in range(22,253):
    empty_folders = 0
    for j in aoi_downloaded:
        print ("-----------------------------------------------------",j)
        time_start = time.time()
        # id_ = str(i)
        id_ = j
        opts = {
            'input_dir' : os.path.join(input_dir,str(j)),
            'output_dir' : os.path.join(stitched_dir,format(id_)),
            'output_name' : "{}".format(id_),
            'processes_num' :10,
            'make_sub_folder': True
        }
        print(input_dir)
        merged_tiffs = merged_tifs_list(tifs_dir, j)
        name = j + "_combined.tif"
        if name in merged_tiffs:
            print("already merged ", name, "moving on to next tif")
            continue
        search_path = os.path.join(opts['input_dir'], '*_*_*.png')
        print(search_path)
        filepaths = glob.glob(search_path)
        filepaths = sorted(filepaths, key=extractxyz)

        if len(filepaths) != 0:
            x_list = []
            y_list = []
            for i in filepaths:
                x_list.append(extractxyz(i)[0])
                y_list.append(extractxyz(i)[1])
            x_list.sort()
            y_list.sort()
            first_x = x_list[0]
            last_x = x_list[-1]
            first_y = y_list[0]
            last_y = y_list[-1]
            
            
            #first_x,first_y,_ = extractxyz(filepaths[0])
            #last_x,last_y,_ = extractxyz(filepaths[-1])
            num_columns = last_x-first_x+1
            num_rows = last_y-first_y +1
            print ("ROWS AND COLS ",num_rows,num_columns)
            temp_folder_path = os.path.join(opts['output_dir'], 'temp_tif_files')
            if not os.path.exists(temp_folder_path):
                os.makedirs(temp_folder_path)

            pool = []
            no_processes = opts['processes_num']
            # print ("cols ",num_columns)
            if no_processes > num_columns:
                no_processes = num_columns

            # Convert all PNGs to TIFs, in different folders, if required

            for i in range(no_processes):
                print (i)
                if opts['make_sub_folder'] == True:
                    os.makedirs(os.path.join(temp_folder_path,str(i)),exist_ok=True)
                p = Process(target=main, args=(opts,filepaths,i,no_processes,temp_folder_path,num_columns,num_rows))
                p.start()
                pool.append(p)

            for p in pool:
                print (p)
                # print ("pool check")
                p.join()


            # Merge TIFs in a folder and merged again if multiple folders involved

            if opts['make_sub_folder'] == True:
                for i in range(no_processes):
                    p = Process(target=merging, args=(opts,i,temp_folder_path, input_dir, stitched_dir))
                    p.start()
                    pool.append(p)
                for p in pool:
                    p.join()

                os.system('gdalbuildvrt {} {}'.format(
                    os.path.join(opts['output_dir'],'mosaic_combined.vrt'),
                    os.path.join(opts['output_dir'] ,'*.tif')))

                os.system('gdal_translate -of GTiff -co COMPRESS=LZW -co BIGTIFF=YES -a_srs EPSG:4326 {} {}'.format(
                    os.path.join(opts['output_dir'],'mosaic_combined.vrt '),
                    os.path.join(opts['output_dir'] , opts['output_name'] + "_combined" +".tif")))
            else:
                print ("going")
                os.system('gdalbuildvrt {} {} > /dev/null'.format(
                    os.path.join(opts['output_dir'],'mosaic_combined.vrt '),
                    os.path.join(temp_folder_path,'*.tif')))
                os.system('gdal_translate -of GTiff  -co BIGTIFF=YES  -a_srs EPSG:4326 {} {} > /dev/null'.format(
                    os.path.join(opts['output_dir'],'mosaic_combined.vrt '),
                    os.path.join(opts['output_dir'],opts['output_name'] + "_combined" +".tif")))
            # copying the combined tif to the tifs folder
            print("Copying the combined tif to the tifs folder")
            from shutil import copyfile
            source = os.path.join(opts['output_dir'] ,str(j) + "_combined.tif")
            destination = os.path.join(tifs_dir, str(j) + "_combined.tif")
            copyfile(source, destination)
            
            # deleteting the temporary files
            shutil.rmtree(opts['output_dir'])
            update_merged_tifs_list(tifs_dir, name)
            
        else:
            print("Downloaded tiles = 0, moving on to next folder")
            empty_folders = empty_folders+1

        # print (time.time()-time_start)
    print ("no. of downloaded folders which were empty = ", empty_folders)

if __name__ == '__main__':
    masterDir = '/home/dev-24/Documents/Work/demoOwnData'
    stitcher(masterDir)
