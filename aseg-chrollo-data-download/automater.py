import os
import multiScraper_multiple_polygons
import geopandas as gpd 
def automater(input_shape_folder,master_folder,codesPath=''):
	input_shape_folder_clipped = os.path.join(master_folder,"aois_clipped")
	temp_list=os.listdir(input_shape_folder)
	list_of_shps=[shp for shp in temp_list if shp.endswith(".shp")]
	# list_of_shps = ['index.shp']
	geojson_dir= os.path.join(master_folder,"geojsons")
	os.makedirs(geojson_dir,exist_ok=True)
	os.makedirs(input_shape_folder_clipped,exist_ok=True)
	count_shp=1
	for shp in list_of_shps:
		flag=0
		source_data= os.path.join(input_shape_folder,shp)
		print (source_data)
		df=gpd.read_file(source_data)
		# list_poly=[poly for poly in df.geometry]
		# print (list_poly)
		for i in range (len(df.geometry)):
			# if i==0:
			newdata = gpd.GeoDataFrame(geometry=gpd.GeoSeries(df.geometry[i]))
			newdata.crs = {'init': 'epsg:4326'}
			newdata["area"] = newdata["geometry"].area
			# newdata.crs = from_epsg(4326)
			out=os.path.join(input_shape_folder_clipped,str(count_shp)+".shp")
			if not newdata.empty:
				newdata.to_file(out)
				count_shp+=1
		# if count_shp>1:
		# 	break
	list_of_clipped_temp_files=os.listdir(input_shape_folder_clipped)
	list_of_clipped_temp_shp=[shp for shp in list_of_clipped_temp_files if shp.endswith(".shp")]
	list_of_clipped_shp = [x for x in list_of_clipped_temp_shp if x not in list_of_shps]
	count = 0
	# list_vpn = ["singapore","France","Japan"]
	for shp in list_of_clipped_shp:
		count += 1
		# if count % 200 == 0:
		# 	os.system("expressvpn disconnect")
		# 	os.system("expressvpn connect {}".format(list_vpn[int(count%600)/200]))
		geojson_path= os.path.join(geojson_dir,shp.split('.')[0]+".geojson")
		# cmd="ogr2ogr -f "+"geojson "+geojson_path+" "+input_shape_folder_clipped+"/"+shp
		cmd = f"ogr2ogr -f geojson {geojson_path} {os.path.join(input_shape_folder_clipped,shp)}"
		os.system(cmd)
		out_path= os.path.join(master_folder,"downloaded")
		multiScraper_multiple_polygons.main(geojson_path,out_path,shp.split('.')[0],codesPath)

			# print (gpd.GeoSeries(df.geometry[i]))
				# print (str(df.geometry[i]).split(' ')[1])
		# geojson_path=geojson_dir+"/"+shp.split('.')[0]+".geojson"
		# print (geojson_path)
		# cmd="ogr2ogr -f "+"geojson "+geojson_path+" "+input_shape_folder+"/"+source_data
		# print (cmd)
		# os.system(cmd)

		# os.rename(geojson_path,geojson_dir+"/"+str(count_shp)+".geojson")
		# geojson_path=geojson_dir+"/"+str(count_shp)+".geojson"
		# out_path=master_folder+"/"+"downloaded"
		# multiScraper_multiple_polygons.main(geojson_path,out_path,count_shp)
		# count_shp+=1
		# if flag:
		# 	break

if __name__ == "__main__":
	input_shape_folder = "/media/catila/a5ee59fd-ca0b-4ee4-bc60-0a51e7ec42bd/Karthik/CBFV1_Training_Data/AOI"
	master_folder=input_shape_folder
	automater(input_shape_folder,master_folder)

