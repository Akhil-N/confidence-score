import os
from multiprocessing import Process
from tqdm import tqdm
import json

# geojson_file = "/media/ursamajor/66832096-52e4-43eb-8028-36a8f03ba2101/training_data/OD/Cars/aoi2.geojson" # single geojson containing all AOIs to be downloaded
# out_path = "/media/ursamajor/66832096-52e4-43eb-8028-36a8f03ba2101/training_data/OD/Cars/downloaded"
# with open(geojson_file, 'r') as geojf:
#         polygon_list = (json.load(geojf)).get('features')

# Currrent scripts downloads AOIs at zoom level of 30 cms
def multiple(i, batch_size,geojson_file,out_path,count_shp,codesPath=''):
    ##For Backup Roads
    #print (type(i),type(count_shp))
    os.system(f"python3 {os.path.join(codesPath,'scraper.py')} --poly {geojson_file} --zoom 21 --out-dir {os.path.join(out_path,count_shp)} --number {i} --minimum 1 --maximum 10000000")
    # os.system("python3 scraper.py --poly '/media/canis/DATA/Data_Center/geojsons/Chandrapur/temp_pombhurna.geojson' --zoom 19 --url  'http://www.google.cn/maps/vt?lyrs=s@189&gl=cn&x={x}&y={y}&z={z}'"+ " --out-dir /media/canis/DATA/Data_Center/Downloaded_Tiles/state_highway/4_1/ --number {} --minimum 1 --maximum 10000000".format(i))
    ##For RGB Imagery
    # os.system("python3 scraper.py --poly '/home/canis/Mahaveer/MapTileDownoadTor/AOI_10Km_test.geojson' --zoom 20"+" --out-dir /media/canis/DATA/Data_Center/Downloaded_Tiles/Delhi_Tiles/ --minimum 1 --maximum 10000")
    # os.system("python3 scraper.py --poly '/media/canis/DATA/Attentive_D/Here_Map_610_county/600_county_seprate_sate_wise_with_geojsons/Florida/geojsons/baker_final_geojson.geojson' --zoom 19 --out-dir /media/canis/DATA/Data_Center/ALL_TILES_HERE_MAP/Florida/baker --minimum {} --maximum {}".format(i*batch_size,(i+1)*batch_size))

def main(geojson_file,out_path,count_shp,codesPath=''):
	with open(geojson_file, 'r') as geojf:
		polygon_list = (json.load(geojf)).get('features')
	# print (polygon_list)
	# print (geojson_file)
	pool = []
	no_processes_1 = 0
	no_processes_2 = len(polygon_list)
	# no_processes_1 = 0
	# no_processes_2 = 30
	batch_size = 1250
	for i in (range(no_processes_1, no_processes_2)):
	    p = Process(target=multiple, args=(i, batch_size,geojson_file,out_path,(count_shp),codesPath))
	    # p = Process(target=multiple, args=(i, batch_size,geojson_file,out_path,count_shp))
	    p.start()
	    pool.append(p)
	for p in (pool):
	    p.join()
