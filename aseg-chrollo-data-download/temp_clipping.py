import os
# import gdal
from osgeo import gdal,ogr
from multiprocessing import Pool
from shutil import copyfile
import shutil
from shapely import geometry
import shapefile
import geopandas as gpd
# import ogr

def clip_tif(tif_path, shp_path, out_tif_dir): 
    try:
        out_tif_path = os.path.join(out_tif_dir, shp_path.split('/')[-1].split('.')[0] + '.tif')
        # img_boundary = get_shapely_object_from_raster(tif_path)
        # print(shp_path)
        # all_shapes = gpd.GeoDataFrame.from_file(shp_path)
        # print('got all shapes')
        # if img_boundary.intersects(geometry.shape(all_shapes['geometry'])):
            # print('clipping')
        os.system('gdalwarp -q -of GTiff -cutline {} -crop_to_cutline {} {}'.format(shp_path, tif_path, out_tif_path))
        # print('generating ', out_tif_path) 
    except Exception as e:
        print(e)
        print('some issue in ', tif_path)



def tempclip(masterDir):
    # Step 0: Clipping tifs 
    in_tifs_dir = os.path.join(masterDir,'tifs') 
    aoi_dir = os.path.join(masterDir,'geojsons')
    out_dir = os.path.join(masterDir,'clipped_tifs')
    
    num_processes = 10 
    os.makedirs(out_dir,exist_ok = True)
    task_list = []
    for tif in os.listdir(in_tifs_dir):
        if tif .endswith('.tif'):
            tif_path = os.path.join(in_tifs_dir, tif)
            # shp_num = str(int(tif.split('_')[0]) + 1) 
            # shp_path = os.path.join(aoi_dir, shp_num + '.geojson')           
            shp_path = os.path.join(aoi_dir, tif.replace('_combined.tif', '.geojson'))
            # shp_path = os.path.join(aoi_dir, tif.split('.')[0], tif.split('.')[0] + '.geojson')
            # print(shp_name)
            # shp_path = os.path.join(in_shp_dir, shp_name)
            task_list.append([tif_path, shp_path, out_dir])
    print('length of task list ', len(task_list))  
    p = Pool(num_processes)
    p.starmap(clip_tif, task_list)
    p.close()
    p.join()

if __name__ == "__main__":
    masterDir=''
    tempclip(masterDir)
