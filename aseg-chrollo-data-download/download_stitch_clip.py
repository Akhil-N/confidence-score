import os
from os.path import *
import time
from automater import automater
from stitcher import stitcher
from temp_clipping import tempclip

input_shape_folder = "/media/catila/a5ee59fd-ca0b-4ee4-bc60-0a51e7ec42bd/akhil/trugreen_v2/data/TG_V2/standard/parcels"
master_folder= "/media/catila/a5ee59fd-ca0b-4ee4-bc60-0a51e7ec42bd/akhil/lawns_buildings"

codesPath = "/media/catila/a5ee59fd-ca0b-4ee4-bc60-0a51e7ec42bd/akhil/codes/aseg-chrollo-data-download"

# download_cmd = f"python {join(codesPath,'automater.py')}"
# stitch_cmd = f"python {join(codesPath,'stitcher.py')}"
# clip_cmd = f"python {join(codesPath,'temp_clipping.py')}"

download = False
stitch = False
clip = False

if download:
    print('Sarting download')
    automater(input_shape_folder,master_folder,codesPath)
    time.sleep(20)
    print('Comleted downloading')

if stitch:
    print('Starting stitching')
    # time.sleep(20)
    stitcher(master_folder)
    time.sleep(20)
    print('Comleted Stitching')
if clip:
    print('Starting clipping')
    tempclip(master_folder)
    print('Comleted Clipping')