import os
import argparse
import shapely.geometry
import asyncio
import aiohttp
import json
from random import random
from utils_scrap import tile2latlon, latlon2tile
import logging
import random
import time
import sys
from datetime import datetime
logger = logging.getLogger()
logger.setLevel(logging.INFO)

BASE_WAIT = 0.01
global_count =0
#default url https://b.tiles.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA
def parse_args():
    parser = argparse.ArgumentParser(description='scrape tiles from a tiled map service')
    parser.add_argument('--poly', required=True, type=str, help='path to geojson containnig bounding polygon(s) to scrape within in lat lon')
    parser.add_argument('--zoom', required=True, type=int, help='zoom level to scrape at')
    # parser.add_argument('--url', type=str, default='https://a.tiles.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}@2x.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA', help='Map service url in http://...{z}/{x}/{y}... format')
    # parser.add_argument('--url', type=str, default='https://a.tiles.mapbox.com/v4/mapbox.satellite/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4M29iazA2Z2gycXA4N2pmbDZmangifQ.-g_vE53SD2WrJ6tFX7QHmA', help='Map service url in http://...{z}/{x}/{y}... format')
    token = "77e7b87d57411d9421df1614387d5c5e4a80a86cc1d275a619f8f6bbad9cfadb6c50f41d6c029f70e46e1eb51161d576e94774fae3fed96fddea713dafc2e0e64b174584b0409ce92687b76075606790"
    parser.add_argument('--url', type=str, default='https://api.gic.org/images/GetOrthoImageTile/bluesky-ultra-g/{z}/{x}/{y}/True/?AuthToken=%s'%(token), help='Map service url in http://...{z}/{x}/{y}... format')
    # z- zoom, z,y- title coordinates
    # https://api.gic.org/images/GetOrthoImageTile/bluesky-ultra-g/{z}/{x}/{y}/True/?AuthToken=eee4795705ce1d4f4c99d326afe9f2f418e7b9efe176723bd637eb13df89844f509b8dd05f16da03fc9ac66ae4ac477baca72af780464ee71a29fa33f52f70705fa96f9281fb3c405ec9ac26f7c53480
    # parser.add_argument('--url', type=str, default='http://www.google.cn/maps/vt?lyrs=s@189&gl=cn&x={x}&y={y}&z={z}')
    parser.add_argument('--out-dir', required=True, type=str, help='Folder to output to')
    parser.add_argument('--number', type=int, required=False, help="Polygon Number from 1 to 400")
    parser.add_argument('--max_connections', required=False, type=int, default=8, help='Max concurrent connections')
    parser.add_argument('--retries', required=False, type=int, default=3, help='Retries per tile')
    parser.add_argument('--minimum', required=False, type=int, default=1, help='Retries per tile')
    parser.add_argument('--maximum', type=int, required=False, default=50000, help="Polygon Number from 1 to 400")
    opts = parser.parse_args()

    in_path = opts.poly

    with open(opts.poly, 'r') as geojf:
        # polygon_list = (json.load(geojf)).get('coordinates')
        polygon_list = (json.load(geojf)).get('features')
        """
        if opts.number:
            opts.poly = [polygon_list[opts.number]]
            #print("opts number ", opts.number)

            # opts.poly = [polygon_list[0]]
        else:
            opts.poly = polygon_list
            #print("else condition reached")
        """
        opts.poly = [polygon_list[opts.number]]
    return opts, in_path


def tile_idxs_in_poly(poly : shapely.geometry.Polygon, zoom : int):
    min_lon, min_lat, max_lon, max_lat = poly.bounds
    #print("min_lon ", min_lon, "min_lat ", min_lat, "max_lon ", max_lon, "max_lat ", max_lat)
    (min_x, max_y), (max_x, min_y) = latlon2tile(min_lat, min_lon, zoom), latlon2tile(max_lat, max_lon, zoom)
    min_x = min_x-1
    max_y = max_y+1
    max_x = max_x +1
    min_y = min_y - 1    
    for x in range(int(min_x), int(max_x) + 1):  # also try with int(min_x) - 1; 
        #print("x ", x)
        for y in range(int(min_y), int(max_y) + 1):   # also try with int(min_y) - 1
            #print("y ", y)
            nw_pt = tile2latlon(x, y, zoom)[::-1]  # poly is defined in geojson form
            #print("nw_pt ", nw_pt)
            ne_pt = tile2latlon(x + 1, y, zoom)[::-1]  # poly is defined in geojson form
            #print("ne_pt ", ne_pt)
            sw_pt = tile2latlon(x, y + 1, zoom)[::-1]  # poly is defined in geojson form
            #print("sw_pt ", sw_pt)
            se_pt = tile2latlon(x + 1, y + 1, zoom)[::-1]  # poly is defined in geojson form
            #print("se_pt ", se_pt)
            # if any(map(lambda pt : shapely.geometry.Point(pt).within(poly),     # also try intersect function of the shapely
            #     (nw_pt, ne_pt, sw_pt, se_pt))):
            if poly.intersects(shapely.geometry.Polygon([(nw_pt),(ne_pt),(se_pt),(sw_pt)])):
                yield x,y
                #print ("yield x, y ", x, " ", y)
            else:
                continue


async def fetch_and_save(session : aiohttp.ClientSession, url : str, retries : int, filepath : str, **kwargs):
    wait_for = BASE_WAIT
    for retry in range(retries):
        response = await session.get(url, params=kwargs)
        try:
            global global_count
            response.raise_for_status()
            img = await response.read()
            with open(filepath, 'wb') as fp:
                fp.write(img)
                if global_count%100 == 0:
                    print("closed", filepath,"{}_tile_downloaded".format(global_count))
                # print("Downloading Tile_No. {},".format(global_count))
                global_count += 1
            return True
        except Exception as E:
            exc_type, exc_obj, exc_tb = sys.exc_info()
            fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
            print(exc_type, fname, exc_tb.tb_lineno)
            await asyncio.sleep(wait_for)
            wait_for = wait_for * (1.0 * random() + 1.0)
    return False


async def main():

    failed_urls = []

    opts, path = parse_args()

    os.makedirs(opts.out_dir, exist_ok=True)

    semaphore = asyncio.Semaphore(opts.max_connections)

    for feat in opts.poly:
        # poly = shapely.geometry.Polygon(feat)
        poly = shapely.geometry.shape(feat['geometry'])
        #print("In feat line 103 ")
        async with aiohttp.ClientSession() as session:
            #print("line 105 reached ")
            tasks = []
            urls = []
            count = 0
            same_count_new = 0
            for x, y in tile_idxs_in_poly(poly, opts.zoom):
                count += 1
                #print("count ", count)
                if opts.minimum <= count <= opts.maximum:
                    if count % 1000 == 0:
                        print("Adding Tiles No.{} to Downloading List".format(count))
                    url = opts.url.format(z=opts.zoom, x=x, y=y)
                    with (await semaphore):
                        filepath = os.path.join(opts.out_dir, '{}_{}_{}.png'.format(opts.zoom, x, y))
                        if os.path.isfile(filepath):
                            same_count_new +=1
                            # print("found tile", same_count_new)
                            continue
                        ret = fetch_and_save(session, url, opts.retries, filepath)
                        urls.append(url)
                        tasks.append(asyncio.ensure_future(ret))
            print("No. of Tiles to Download", count)
            print("Already Downloaded Tiles", same_count_new)

            res = await asyncio.gather(*tasks)

            n_failed = res.count(False)

            for i, url in enumerate(urls):
                if res[i] is False:
                    failed_urls.append(url)

    print('Downloaded {}/{}'.format(len(tasks) - n_failed, len(tasks)))
    return failed_urls


if __name__ == '__main__':
    # import ipdb; ipdb.set_trace()
    opts, grids_path = parse_args()

    msa_name = grids_path.split('/')[-2]
    grids_no = grids_path.split('/')[-1].split('.')[0]

    save_log_path = f"/media/ursamajor/66832096-52e4-43eb-8028-36a8f03ba210/training_data/object_detection_data/downloading_logs/{msa_name}.log"

    # fh = logging.FileHandler(save_log_path)
    # fh.setLevel(logging.INFO)
    # logger.addHandler(fh)


    def run_scraper():
        flag = True
        failed_urls = []
        try:
            while len(failed_urls) > 0 or flag:
                #print("inside while loop at line 159")
                flag = False
                failed_url_last = len(failed_urls)
                loop = asyncio.get_event_loop()
                failed_urls = loop.run_until_complete(main())
                if len(failed_urls) == 0:
                    return 0
                if len(failed_urls) == failed_url_last:
                    # logger.info("{}-------grid_no-------{}-------failed_urls-------{}".format(msa_name, grids_no,len(failed_urls)))
                    print("failed_url matched 2 times")
                    return -1
                print("No of Failed urls", len(failed_urls))
        except Exception as e:
            #print("Restarting main() in exception and changing expressvpn server line no 173")
            print(e)
            return -1

    downloading_status = 1
    retry_whole_count = 0
    while downloading_status != 0 and retry_whole_count <= 1:

        downloading_status = run_scraper()

        if downloading_status == -1:
            retry_whole_count += 1