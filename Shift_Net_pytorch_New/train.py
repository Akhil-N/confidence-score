import time,pickle

from tqdm import tqdm
from options.train_options import TrainOptions
from options.test_options import TestOptions
from data.data_loader import CreateDataLoader
from models import create_model
from util.visualizer import Visualizer

if __name__ == "__main__":
    # options='--dataroot /home/pictor/workspace/Akhil/Confidence/Data/RecData/Train/color_inputs'.split(' ')
    opt = TrainOptions().parse()#options=options
    data_loader = CreateDataLoader(opt)
    dataset = data_loader.load_data()
    dataset_size = len(data_loader)
    print('#training images = %d' % dataset_size)

    if opt.val_dataroot:
        options=f'--dataroot {opt.val_dataroot} --testing_mask_folder {opt.val_mask_folder}'.split(' ')
        val_opt = TestOptions().parse(options=options)
        val_opt.nThreads = 1   # test code only supports nThreads = 1
        val_opt.batchSize = opt.batchSize  # test code only supports batchSize = 1
        val_opt.serial_batches = True  # no shuffle
        val_opt.no_flip = True  # no flip
        val_opt.display_id = -1 # no visdom display
        val_opt.loadSize = val_opt.fineSize  # Do not scale!

        val_data_loader = CreateDataLoader(val_opt)
        val_dataset = data_loader.load_data()
        print('#Val images = %d' % len(val_data_loader))

    model = create_model(opt)
    visualizer = Visualizer(opt)

    total_steps = 0
    train_losses,val_losses = {},{}
    for epoch in range(opt.epoch_count, opt.niter + opt.niter_decay + 1):
        train_losses[epoch],val_losses[epoch] = [],[]
        epoch_start_time = time.time()
        iter_data_time = time.time()
        epoch_iter = 0

        for i, data in enumerate(dataset):
            iter_start_time = time.time()
            if total_steps % opt.print_freq == 0:
                t_data = iter_start_time - iter_data_time
            visualizer.reset()
            total_steps += opt.batchSize
            epoch_iter += opt.batchSize

            model.set_input(data) # it not only sets the input data with mask, but also sets the latent mask.

            # Additonal, should set it before 'optimize_parameters()'.
            if total_steps % opt.display_freq == 0:
                if opt.show_flow:
                    model.set_show_map_true()

            model.optimize_parameters()

            if total_steps % opt.display_freq == 0:
                save_result = total_steps % opt.update_html_freq == 0
                if opt.show_flow:
                    model.set_flow_src()
                    model.set_show_map_false()
                visualizer.display_current_results(model.get_current_visuals(), epoch, save_result)

            if total_steps % opt.print_freq == 0:
                losses = model.get_current_losses()
                train_losses[epoch].append(losses)
                t = (time.time() - iter_start_time) / opt.batchSize
                visualizer.print_current_losses(epoch, epoch_iter, losses, t, t_data)
                if opt.display_id > 0:
                    visualizer.plot_current_losses(epoch, float(epoch_iter) / dataset_size, opt, losses)

            if total_steps % opt.save_latest_freq == 0:
                print('saving the latest model (epoch %d, total_steps %d)' %
                        (epoch, total_steps))
                model.save_networks('latest')

            iter_data_time = time.time()
        if epoch % opt.save_epoch_freq == 0:
            print('saving the model at the end of epoch %d, iters %d' %
                    (epoch, total_steps))
            model.save_networks('latest')
            if not opt.only_lastest:
                model.save_networks(epoch)
        
        if epoch % opt.val_run_freq == 0:
            print("Running on Validation set")
            for i, val_data in tqdm(enumerate(val_dataset)):
                model.set_input(val_data)
                model.test()
                valLossesDct = model.get_current_losses()
                val_losses[epoch].append(valLossesDct)

        print('End of epoch %d / %d \t Time Taken: %d sec' %
                (epoch, opt.niter + opt.niter_decay, time.time() - epoch_start_time))
        model.update_learning_rate()

        with open(f'./{opt.name}.pkl','wb') as pklFl:
            lss = {'train':train_losses,'val':val_losses}
            pickle.dump(lss,pklFl)