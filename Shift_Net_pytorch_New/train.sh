python train.py --dataroot /home/pictor/workspace/Akhil/Confidence/Data/RecData/shrnkdBldngs/Train/images \
--val_dataroot /home/pictor/workspace/Akhil/Confidence/Data/RecData/shrnkdBldngs/Val/images \
--training_mask_folder /home/pictor/workspace/Akhil/Confidence/Data/RecData/shrnkdBldngs/Train/masks \
--val_mask_folder /home/pictor/workspace/Akhil/Confidence/Data/RecData/shrnkdBldngs/Val/masks \
--val_run_freq 5 \
--name shrinked_buildins \
--checkpoints_dir ./shrinked_buildins_log \
--niter 300
