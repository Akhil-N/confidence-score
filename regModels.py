import torchvision.models as models
import torch.nn as nn,torch

class RegModel(torch.nn.Module):
    def __init__(self,num_classes=2,modelling = 'classification'):
        """
        In the constructor we instantiate four parameters and assign them as
        member parameters.
        """
        super().__init__()
        resnet18 = models.resnet18(pretrained=True)
        pretrained_weights = resnet18.conv1.weight.clone()
        
        self.input_img_mask_conv = nn.Conv2d(4, 32, kernel_size=7, stride=2, padding=3, bias=False)
        self.input_img_mask_conv.weight[:,:3,:,:].data.copy_(torch.nn.Parameter(pretrained_weights[:32,:3,:,:]))
        self.input_img_mask_conv.weight[:,3,:,:].data.copy_(torch.nn.Parameter(pretrained_weights[:32,1,:,:]));

        self.input_diff_conv = nn.Conv2d(3, 32, kernel_size=7, stride=2, padding=3, bias=False)
        self.input_diff_conv.weight.data.copy_(torch.nn.Parameter(pretrained_weights[32:,:,:,:])); 

        self.out = nn.Linear(in_features=512,out_features=num_classes,bias=True)
        dense_wights = resnet18.fc.weight.clone()
        # resnet18.fc = out
        # resnet18.fc.weight.data.copy_(dense_wights[:num_classes]);
        self.out.weight.data.copy_(dense_wights[:num_classes]);

        self.resnet18_bakbone = nn.Sequential(*(list(resnet18.children())[1:-1]))
        
        self.moelling = modelling
        self.num_classes = num_classes

    def forward(self, x):
        """
        In the forward function we accept a Tensor of input data and we must return
        a Tensor of output data. We can use Modules defined in the constructor as
        well as arbitrary operators on Tensors.
        """
        input_img_mask = torch.cat((x['image'],x['pred_mask']),dim=1) # 4 channels
        concatInptAndMask = torch.cat((
            self.input_img_mask_conv(input_img_mask),
            self.input_diff_conv(x['diff'])
        ),dim = 1)

        out = self.resnet18_bakbone(concatInptAndMask)
        out = torch.flatten(out, 1)
        out = self.out(out)
        # if self.moelling == 'classification':
        #     if self.num_classes==1:
        #         out = nn.Sigmoid()(out)
        #     else:
        #         out = nn.Softmax(dim=-1)(x)
        return out

class inputMaskBlock(nn.Module):
    def __init__(self):
        super().__init__()
        resnet18 = models.resnet18(pretrained=True)
        pretrained_weights = resnet18.conv1.weight.clone()
        self.baseBlock = nn.Sequential(*list(resnet18.children())[1:6])

        self.input_img_mask_conv = nn.Conv2d(4, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.input_img_mask_conv.weight[:,:3,:,:].data.copy_(torch.nn.Parameter(pretrained_weights[:,:3,:,:]))
        self.input_img_mask_conv.weight[:,3,:,:].data.copy_(torch.nn.Parameter(pretrained_weights[:,1,:,:]));

    def forward(self,x):
        input_img_mask = torch.cat((x['image'],x['pred_mask']),dim=1)
        out = self.input_img_mask_conv(input_img_mask)
        out = self.baseBlock(out)
        return out

class inputRecDiffBlock(nn.Module):
    def __init__(self):
        super().__init__()
        resnet18 = models.resnet18(pretrained=True)
        self.baseBlock = nn.Sequential(*list(resnet18.children())[:6])


    def forward(self,x):
        out = self.baseBlock(x['diff'])
        return out
        

class RegModelSeperateBlocks(torch.nn.Module):
    def __init__(self,num_classes=2):
        """
        In the constructor we instantiate four parameters and assign them as
        member parameters.
        """
        super().__init__()
        
        self.input_mask_conv_block = inputMaskBlock()
        self.input_rec_diff_bloc = inputRecDiffBlock()

        resnet18 = models.resnet18(pretrained=True)

        self.resnet_block = nn.Sequential(*list(resnet18.children())[6:-1])

        self.outLayer = nn.Linear(in_features=512,out_features=num_classes,bias=True)
        dense_wights = resnet18.fc.weight.clone()
        self.outLayer.weight.data.copy_(dense_wights[:num_classes]);

        self.num_classes = num_classes

    def forward(self, x):
        input_mask_conv_block_out = self.input_mask_conv_block(x)
        input_rec_diff_bloc_out = self.input_rec_diff_bloc(x)

        out = input_mask_conv_block_out + input_rec_diff_bloc_out
        out = self.resnet_block(out)
        out = torch.flatten(out, 1)
        out = self.outLayer(out)
        return out

class RegModelOnlyInputandMask(torch.nn.Module):
    def __init__(self,num_classes=2,modelling = 'classification'):
        """
        In the constructor we instantiate four parameters and assign them as
        member parameters.
        """
        super().__init__()
        resnet18 = models.resnet18(pretrained=True)
        pretrained_weights = resnet18.conv1.weight.clone()
        
        self.input_img_mask_conv = nn.Conv2d(4, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.input_img_mask_conv.weight[:,:3,:,:].data.copy_(torch.nn.Parameter(pretrained_weights[:,:3,:,:]))
        self.input_img_mask_conv.weight[:,3,:,:].data.copy_(torch.nn.Parameter(pretrained_weights[:,1,:,:]));

        self.out = nn.Linear(in_features=512,out_features=num_classes,bias=True)
        dense_wights = resnet18.fc.weight.clone()

        self.out.weight.data.copy_(dense_wights[:num_classes]);

        self.resnet18_bakbone = nn.Sequential(*(list(resnet18.children())[1:-1]))
        
        self.moelling = modelling
        self.num_classes = num_classes

    def forward(self, x):
        input_img_mask = torch.cat((x['image'],x['pred_mask']),dim=1) # 4 channels
        concatInptAndMaskOut = self.input_img_mask_conv(input_img_mask)

        out = self.resnet18_bakbone(concatInptAndMaskOut)
        out = torch.flatten(out, 1)
        out = self.out(out)
        return out

class RegModelOnlyDiff(torch.nn.Module):
    def __init__(self,num_classes=2,modelling = 'classification'):
        """
        In the constructor we instantiate four parameters and assign them as
        member parameters.
        """
        super().__init__()
        resnet18 = models.resnet18(pretrained=False)
        
        self.input_diff_conv = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, bias=False)

        self.out = nn.Linear(in_features=512,out_features=num_classes,bias=True)

        self.resnet18_bakbone = nn.Sequential(*(list(resnet18.children())[1:-1]))
        
        self.moelling = modelling
        self.num_classes = num_classes

    def forward(self, x):
        """
        In the forward function we accept a Tensor of input data and we must return
        a Tensor of output data. We can use Modules defined in the constructor as
        well as arbitrary operators on Tensors.
        """
        diffOut = self.input_diff_conv(x['diff'])

        out = self.resnet18_bakbone(diffOut)
        out = torch.flatten(out, 1)
        out = self.out(out)

        return out
