import os
import sys
#sys.path.insert(0,'/home/mahaveer_attentive_ai/workspace/projects/aseg-chrollo/')
import time
import random
import argparse
import sys 
from distutils.version import LooseVersion
import torch
import torch.nn as nn
import torch.multiprocessing as mp
import torch.distributed as dist
import torch.utils.data.distributed
from config import cfg
from dataset import TrainDataset, user_collate_fn
from models import ModelBuilder, SegmentationModule
from utils import AverageMeter, parse_devices, setup_logger
# from lib.nn import UserScatteredDataParallel, user_scattered_collate, patch_replication_callback
from eval import main as validate_main
from torch.utils.tensorboard import SummaryWriter
#import wandb
#from wandbeval import main as wandb_main
# from trains import Task

# train one epoch
def train(segmentation_module, loader, optimizers, history, epoch, cfg, args,gpus, writer, scaler=None, autocast=None):
    batch_time = AverageMeter()
    data_time = AverageMeter()
    ave_total_loss = AverageMeter()
    ave_acc = AverageMeter()

    segmentation_module.train(not cfg.TRAIN.fix_bn)

    # main loop
    tic = time.time()
    iterator = iter(loader)
    for i in range(cfg.TRAIN.epoch_iters):
        # set learning rate
        cur_iter = i + (epoch - 1) * cfg.TRAIN.epoch_iters
        adjust_learning_rate(optimizers, cur_iter, cfg)

        # load a batch of data
        # if len(gpus) > 1:
        #     batch_data = next(iterator)
        # else:
        #     batch_data = next(iterator)[0]
        #     for k in batch_data.keys():
        #         batch_data[k] = batch_data[k].cuda()
        
        batch_data = next(iterator)
        data_time.update(time.time() - tic)
        segmentation_module.zero_grad()

        # forward pass
        if args.amp:
            with autocast():
                loss, acc = segmentation_module(batch_data)
                loss = loss.mean()
                acc = acc.mean()
        else:
            loss, acc = segmentation_module(batch_data)
            loss = loss.mean()
            acc = acc.mean()

        # Backward      
        if args.amp:
            scaler.scale(loss).backward()

        else:
            loss.backward()

        for optimizer in optimizers:
            if optimizer:
                if args.amp:
                    scaler.step(optimizer)
                else:
                    optimizer.step()
        
        if args.amp:
            scaler.update()

        # measure elapsed time
        batch_time.update(time.time() - tic)
        tic = time.time()

        # update average loss and acc
        ave_total_loss.update(loss.data.item())
        ave_acc.update(acc.data.item()*100)  

        if len(gpus) > 1:
            var_print = batch_data['img_data'].shape[0]/batch_time.average()
        else:
            var_print = batch_data['img_data'].shape[0]/batch_time.average()

        # calculate accuracy, and display
        #wandb.log({"lr_encoder":cfg.TRAIN.running_lr_encoder,"lr_decoder":cfg.TRAIN.running_lr_decoder,"Avg_Train_Loss":ave_total_loss.average(),"Avg_Train_Accuracy":ave_acc.average()}) 
        # calculate accuracy, and display
        if i % cfg.TRAIN.disp_iter == 0:
            # print('Epoch: [{}][{}/{}], Time: {:.2f}, Data: {:.2f}, '
            print('Epoch: [{}][{}/{}], Img/s: {:.2f} Time: {:.2f}, Data: {:.2f}, '
                  'lr_encoder: {:.6f}, lr_decoder: {:.6f}, '
                  'Accuracy: {:4.2f}, Loss: {:.6f}'
                  .format(epoch, i, cfg.TRAIN.epoch_iters,
                        #   batch_data[0]['img_data'].shape[0]/batch_time.average(),
                          var_print,
                          batch_time.average(), data_time.average(),
                          cfg.TRAIN.running_lr_encoder, cfg.TRAIN.running_lr_decoder,
                          ave_acc.average(), ave_total_loss.average()))

            fractional_epoch = epoch - 1 + 1. * i / cfg.TRAIN.epoch_iters
            history['train']['epoch'].append(fractional_epoch)
            history['train']['loss'].append(loss.data.item())
            history['train']['acc'].append(acc.data.item())
    writer.add_scalar('Train_Accuracy/Accuracy',ave_acc.average(),epoch)
    writer.add_scalar('Train_Loss/Loss',ave_total_loss.average(),epoch)

def checkpoint(nets, history, cfg, epoch):
    print('Saving checkpoints...')
    (net_encoder, net_decoder, crit) = nets

    if net_encoder:
        dict_encoder = net_encoder.state_dict()
    dict_decoder = net_decoder.state_dict()

    torch.save(
        history,
        '{}/history_epoch_{}.pth'.format(cfg.DIR, epoch))
    if net_encoder:
        torch.save(
            dict_encoder,
            '{}/encoder_epoch_{}.pth'.format(cfg.DIR, epoch))
    torch.save(
        dict_decoder,
        '{}/decoder_epoch_{}.pth'.format(cfg.DIR, epoch))


def group_weight(module):
    group_decay = []
    group_no_decay = []
    for m in module.modules():
        if isinstance(m, nn.Linear):
            group_decay.append(m.weight)
            if m.bias is not None:
                group_no_decay.append(m.bias)
        elif isinstance(m, nn.modules.conv._ConvNd):
            group_decay.append(m.weight)
            if m.bias is not None:
                group_no_decay.append(m.bias)
        elif isinstance(m, nn.modules.batchnorm._BatchNorm):
            if m.weight is not None:
                group_no_decay.append(m.weight)
            if m.bias is not None:
                group_no_decay.append(m.bias)
        elif isinstance(m, nn.modules.normalization.GroupNorm):
            if m.weight is not None:
                group_no_decay.append(m.weight)
            if m.bias is not None:
                group_no_decay.append(m.bias)
    assert len(list(module.parameters())) == len(
        group_decay) + len(group_no_decay)
    groups = [dict(params=group_decay), dict(
        params=group_no_decay, weight_decay=.0)]
    return groups


def create_optimizers(nets, cfg):
    (net_encoder, net_decoder, crit) = nets

    if net_encoder:
        optimizer_encoder = torch.optim.SGD(
            group_weight(net_encoder),
            lr=cfg.TRAIN.lr_encoder,
            momentum=cfg.TRAIN.beta1,
            weight_decay=cfg.TRAIN.weight_decay)
    else:
        optimizer_encoder = None
        
    optimizer_decoder = torch.optim.SGD(
        group_weight(net_decoder),
        lr=cfg.TRAIN.lr_decoder,
        momentum=cfg.TRAIN.beta1,
        weight_decay=cfg.TRAIN.weight_decay)
    return (optimizer_encoder, optimizer_decoder)

def adjust_learning_rate(optimizers, cur_iter, cfg):
    scale_running_lr = (
        (1. - float(cur_iter) / cfg.TRAIN.max_iters) ** cfg.TRAIN.lr_pow)
    cfg.TRAIN.running_lr_encoder = cfg.TRAIN.lr_encoder * scale_running_lr
    cfg.TRAIN.running_lr_decoder = cfg.TRAIN.lr_decoder * scale_running_lr

    (optimizer_encoder, optimizer_decoder) = optimizers
    if optimizer_encoder:
        for param_group in optimizer_encoder.param_groups:
            param_group['lr'] = cfg.TRAIN.running_lr_encoder
    for param_group in optimizer_decoder.param_groups:
        param_group['lr'] = cfg.TRAIN.running_lr_decoder


# def main(cfg, gpus, writer):
def main_worker(rank, cfg,args,gpus, rank2gpu):
    # Param setup

    writer = SummaryWriter()
    num_gpus = len(rank2gpu.keys())
    gpu = rank2gpu[rank]
    print("Launch GPU: {} for training".format(gpu))
    dist.init_process_group(
        backend='nccl', init_method='tcp://127.0.0.1:1234',
        world_size=num_gpus, rank=rank)

    # Network Builders
    #wandb.init()
    #wandb.config.update(cfg)
    net_encoder = ModelBuilder.build_encoder(
        cfg = cfg,
        arch=cfg.MODEL.arch_encoder.lower(),
        fc_dim=cfg.MODEL.fc_dim,
        weights=cfg.MODEL.weights_encoder
        )
    net_decoder = ModelBuilder.build_decoder(
        arch=cfg.MODEL.arch_decoder.lower(),
        fc_dim=cfg.MODEL.fc_dim,
        num_class=cfg.DATASET.num_class,
        weights=cfg.MODEL.weights_decoder)
    
    
    loss_weights_list = cfg.TRAIN.loss_weights
    if (all(loss_weight == loss_weights_list[0] for loss_weight in loss_weights_list)):
        print("all loss weights are equal")
        crit = nn.NLLLoss(ignore_index=-1)
    else:
        loss_weights = torch.tensor(loss_weights_list)     
        crit = nn.NLLLoss(ignore_index=-1, weight = loss_weights, reduction='none')   
    

    if cfg.MODEL.arch_decoder.endswith('deepsup'):
        segmentation_module = SegmentationModule(
            net_encoder, net_decoder, crit, cfg.TRAIN.deep_sup_scale)
    else:
        segmentation_module = SegmentationModule(
            net_encoder, net_decoder, crit)

    segmentation_module = torch.nn.SyncBatchNorm.convert_sync_batchnorm(segmentation_module)

    torch.cuda.set_device(gpu)
    segmentation_module.cuda(gpu)
    segmentation_module = torch.nn.parallel.DistributedDataParallel(segmentation_module, device_ids=[gpu])

    #wandb.watch(segmentation_module, log="all")
    # Dataset and Loader
    dataset_train = TrainDataset(
        cfg.DATASET.root_dataset,
        cfg.DATASET.list_train,
        cfg.DATASET,
        cfg,
        batch_per_gpu=cfg.TRAIN.batch_size_per_gpu,
        world_size=num_gpus, rank=rank)

    loader_train = torch.utils.data.DataLoader(
        dataset_train,
        batch_size=1,   # modified: each batch is a dict of multiple samples
        collate_fn=user_collate_fn,
        num_workers=cfg.TRAIN.workers ,
        drop_last=False,
        pin_memory=True)
    # print('1 Epoch = {} iters'.format(cfg.TRAIN.epoch_iters))

    # # create loader iterator
    # iterator_train = iter(loader_train)

    # # load nets into gpu
    
    # segmentation_module.cuda()

    # Set up optimizers
    nets = (net_encoder, net_decoder, crit)
    optimizers = create_optimizers(nets, cfg)    

    # pytorch 1.6 additions for amp
    if args.amp:       
        scaler = torch.cuda.amp.GradScaler()
        autocast = torch.cuda.amp.autocast 
    else:
        scaler = None
        autocast = None 
               
    
    # if len(gpus) > 1:
    #     segmentation_module = UserScatteredDataParallel(segmentation_module, device_ids=gpus)
	# # segmentation_module = torch.nn.parallel.DistributedDataParallel(segmentation_module, device_ids=gpus)
    #     # For sync bn
    #     patch_replication_callback(segmentation_module)   
    

    # Main loop
    history = {'train': {'epoch': [], 'loss': [], 'acc': []}}

    cfg.TRAIN.epoch_iters = len(dataset_train) // cfg.TRAIN.batch_size_per_gpu
    cfg.TRAIN.max_iters = cfg.TRAIN.epoch_iters * cfg.TRAIN.num_epoch
    for epoch in range(cfg.TRAIN.start_epoch, cfg.TRAIN.num_epoch):
        # deterministic data shuffling
        dataset_train.shuffle(epoch)
        train(segmentation_module, loader_train,
              optimizers, history, epoch+1, cfg,args, gpus, writer, scaler, autocast)
        
        # checkpointing
        if rank == 0:
            checkpoint(nets, history, cfg, epoch+1)

        # validate based on validate frequency
        if (epoch+1) % cfg.TRAIN.validate_frequency == 0 and rank == 0:
            validate_main(cfg, gpu, str(epoch+1), writer)   
       # if (epoch+1) % cfg.TRAIN.wandb_frequency == 0:
        #    wandb_main(cfg, gpus[0], str(epoch+1), writer)         
            
        
            
    writer.close()
    print('Training Done!')




if __name__ == '__main__':
    assert LooseVersion(torch.__version__) >= LooseVersion('0.4.0'), \
        'PyTorch>=0.4.0 is required'

    parser = argparse.ArgumentParser(
        description="PyTorch Semantic Segmentation Training"
    )
    parser.add_argument(
        "--cfg",
        default="config/attentive-hrnetv2.yaml",
        metavar="FILE",
        help="path to config file",
        type=str,
    )
    parser.add_argument(
        "--gpus",
        default="0",#"0-3"
        help="gpus to use, e.g. 0-3 or 0,1,2,3"
    )
    parser.add_argument(
        "opts",
        help="Modify config options using the command-line",
        default=None,
        nargs=argparse.REMAINDER,
    )

    # getting current pid
    # pid = os.getpid()
    # print('Current process pid is ', pid)

    # Mixed precision training parameters
    parser.add_argument('--amp', action = 'store_true', help = 'Use Automatic Mixed Precision training')


    args = parser.parse_args()



    # task = Task.init('Chrollo', '{}'.format(args.cfg.split('/')[-1]))
    

    cfg.merge_from_file(args.cfg)
    cfg.merge_from_list(args.opts)
    # task.connect(cfg)

    # cfg.freeze()

    logger = setup_logger(distributed_rank=0)   # TODO
    logger.info("Loaded configuration file {}".format(args.cfg))
    logger.info("Running with config:\n{}".format(cfg))

    # Output directory
    if not os.path.isdir(cfg.DIR):
        os.makedirs(cfg.DIR)
    logger.info("Outputing checkpoints to: {}".format(cfg.DIR))
    with open(os.path.join(cfg.DIR, 'config.yaml'), 'w') as f:
        f.write("{}".format(cfg))

    # Start from checkpoint
    if cfg.TRAIN.start_epoch > 0:
        cfg.MODEL.weights_encoder = os.path.join(
            cfg.DIR, 'encoder_epoch_{}.pth'.format(cfg.TRAIN.start_epoch))
        cfg.MODEL.weights_decoder = os.path.join(
            cfg.DIR, 'decoder_epoch_{}.pth'.format(cfg.TRAIN.start_epoch))
        assert os.path.exists(
                cfg.MODEL.weights_decoder), "checkpoint does not exist!"

    
    # Parse gpu ids
    gpus = parse_devices(args.gpus)
    gpus = [x.replace('gpu', '') for x in gpus]
    gpus = [int(x) for x in gpus]
    num_gpus = len(gpus)
    cfg.TRAIN.batch_size = num_gpus * cfg.TRAIN.batch_size_per_gpu

    # cfg.TRAIN.max_iters = cfg.TRAIN.epoch_iters * cfg.TRAIN.num_epoch
    # cfg.TRAIN.running_lr_encoder = cfg.TRAIN.lr_encoder
    # cfg.TRAIN.running_lr_decoder = cfg.TRAIN.lr_decoder

    random.seed(cfg.TRAIN.seed)
    torch.manual_seed(cfg.TRAIN.seed)
    # main(cfg, gpus , writer )
    rank2gpu = {i: gpus[i] for i in range(num_gpus)}
    mp.spawn(main_worker, nprocs=num_gpus, args=(cfg, args,gpus,rank2gpu))
    #main_worker(0, cfg, rank2gpu, writer)
    # writer.close()
