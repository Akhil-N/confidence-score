import os
import cv2
from skimage import io
from osgeo import gdal
import warnings
warnings.filterwarnings('ignore')
import numpy as np
from os import makedirs
from tqdm import tqdm
from multiprocessing import Process, Pool
from shapely import geometry
import shapefile


DIM = 1024
STRIDE = 512
num_processes = 11
ROOT_DIR = "/media/ursamajor/1df17740-bab5-4762-9749-b349b34bd2f3/FM_global/sp_temp/clipped_tifs/"

filepaths = os.listdir(ROOT_DIR)
only_tif = []   # a list containing names of all the tiff files present in the root directory

for i in filepaths:
    if ".tif" in i:
        only_tif.append(i)

top_list = []   # a list which is subset of the only_tif list. It contains names of all the original tiff (clipped)
gt_list = []    # a list which is subset of the only_tif list. It contains names of all the tiff files which are ground truth
id_list =[]     
for tif in only_tif:
    if "gt" in tif:
        gt_list.append(tif)
        id_list.append(tif.split('_')[0])
    else:
        top_list.append(tif)


print(top_list, gt_list)
if len(top_list)==len(gt_list):
    print("Found top and gt$$$$$$$$$$$, all tifs have corresponding groundtruths tiffs")
else:
    print("top and gt are not equal##############")
    # exit()

# DATA_FOLDER = '/media/canis/DATA/Mahaveer/Train_Data/PARKINH_HIGH_BUFF/'
# top = DATA_FOLDER+'{}/'.format('top')
# gt = DATA_FOLDER+'{}/'.format('gt'

# if 'trainA' not in os.listdir(DATA_FOLDER):
#     os.mkdir(DATA_FOLDER+'trainA')
id_list = sorted(id_list)  #TODO: this may not function as desired. First we need to convert the contents of the id_list into integers and then perform the sorting

def get_shapely_object_from_raster(input_raster,pixel_bounds = None):
    ds = gdal.Open(input_raster)
    gt = ds.GetGeoTransform()
    xsize,ysize = ds.RasterXSize,ds.RasterYSize
    if not pixel_bounds:
        minx = gt[0]
        miny = gt[3] + xsize * gt[4] + ysize * gt[5]
        maxx = gt[0] + xsize * gt[1] + ysize * gt[2]
        maxy = gt[3]
        return geometry.Polygon([[minx,miny],[maxx,miny],[maxx,maxy],[minx,maxy]])
        # return minx, miny, maxx, maxy
    else:
        minx = gt[0] + pixel_bounds[0][0] * gt[1] + pixel_bounds[0][1] * gt[2]
        miny = gt[3] + pixel_bounds[1][0] * gt[4] + pixel_bounds[1][1] * gt[5]
        maxx = gt[0] + pixel_bounds[1][0] * gt[1] + pixel_bounds[1][1] * gt[2]
        maxy = gt[3] + pixel_bounds[0][0] * gt[4] + pixel_bounds[0][1] * gt[5]
        return geometry.Polygon([[minx, miny], [maxx, miny], [maxx, maxy], [minx, maxy]])
        # return minx, miny, maxx, maxy

def slice_gdal(in_top,in_gt,A,B,full_li,k, shp_path):  
    j,i,save_id = full_li[k]
    # out_A = A + '{}.jpg'.format(save_id)
    out_A = A+'{}.png'.format(save_id)
    out_B = B+'{}.png'.format(save_id)   
    
    # os.system("gdal_translate -ot byte {} {} -of 'PNG' -srcwin {} {} {} {} > /dev/null".format(in_top,out_A,i,j,DIM,DIM))
    # os.system("gdal_translate -ot byte {} {} -of 'PNG' -srcwin {} {} {} {} > /dev/null".format(in_gt, out_B, i, j, DIM, DIM))
    img_boundary = get_shapely_object_from_raster(top, pixel_bounds=[(i, j),(i + DIM, j + DIM)])
    # shp_path = top2.replace("")
    # print(img_boundary)
    all_shapes = shapefile.Reader(shp_path).shapes()    
    for s in range(len(all_shapes)):        
        if img_boundary.intersects(geometry.shape(all_shapes[s])):
            # print("creating chips")
            # gdal.Translate(out_A, in_top, srcWin=[i, j, DIM, DIM], format="JPEG", outputType=gdal.GDT_Byte)
            gdal.Translate(out_A, in_top, srcWin=[i, j, DIM, DIM], format="PNG", outputType=gdal.GDT_Byte)
            gdal.Translate(out_B, in_gt, srcWin=[i + 1025, j + 1025, DIM, DIM], format="PNG", outputType=gdal.GDT_Byte)
    # img = np.array(gdal.Open(out_B).GetRasterBand(1).ReadAsArray())
    # np.where(img == 5, 0, img)
    # im = Image.fromarray(img)
    # im.save(out_B)
    # print(out_B)    

for id in id_list:    
    print("Creating Training_Data for {} aoi".format(id))
    DATA_FOLDER = ROOT_DIR +"{}".format(id)
    try:
        os.makedirs(DATA_FOLDER+"/trainA")
        os.makedirs(DATA_FOLDER+"/trainB")
    except:
        pass
    # top = ROOT_DIR + "{}_combined.tif".format(id)
    top = ROOT_DIR + "{}.tif".format(id)
    top2 = ROOT_DIR + "{}_resized.tif".format(id)

    gt  = ROOT_DIR + "{}_gt.tif".format(id)
    gt2 = ROOT_DIR + "{}_gt_resized.tif".format(id)

    A = DATA_FOLDER+'/{}/'.format('trainA')
    B = DATA_FOLDER+'/{}/'.format('trainB')

    save_id = 0
    c = 0

    print("loading top and gt in memory")
    top_im = io.imread(top)
    gt_im = io.imread(gt)
    # top_im = gdal.Open(top)
    # gt_im = gdal.Open(gt)

    # top_im = cv2.imread(top)
    # print("top_im dimensions ", top_im.shape[0:2])
    # gt_im = cv2.imread(gt)

    print("loading Completed")
    # w = gt_im.RasterXSize
    # h = gt_im.RasterYSize
    w, h = gt_im.shape[0:2]
    print("w",w, "h", h)
    # import ipdb; ipdb.set_trace()
    # h,w = np.shape(gt_im)[0:2]
    # resize_shape = (int(h), int(w))
    resize_factor = 1
    resize_shape = (int(h / resize_factor), int(w / resize_factor))     
    
    

    if resize_factor != 1:
    
        top_im = cv2.resize(top_im, resize_shape)   # NOTE: The new resized image does not contain the CRS information, however we do not require this information
        gt_im = cv2.resize(gt_im, resize_shape)
        print("Saving resized Tiff")
        # cv2.imwrite(top2, top_im)
        # cv2.imwrite(gt2, gt_im)
        io.imsave(top2, top_im)  # TODO: perform this step only if we are actually reshaping the image.
        io.imsave(gt2,gt_im)

        # top2 = top.split('.')+'resized.tif'


        # kernel = np.array([[-1,-1,-1], [-1,10,-1], [-1,-1,-1]])/2.0
        # top_im = cv2.filter2D(top_im, -1, kernel)
        print("resized image and ground truth dimensions")
        print(top_im.shape,gt_im.shape)
    
    # print w/2, h/2

    full_li = []
    for i in range(0, (w//resize_factor)+1, STRIDE):
        for j in range(0,(h//resize_factor)+1, STRIDE):
            try:
                # if gt_im[i:i+DIM, j:j+DIM].shape[0] == DIM and gt_im[i:i+DIM, j:j+DIM].shape[1] == DIM:
                    #io.imsave(A+'{}.png'.format(save_id), top_im[i:i+DIM, j:j+DIM])
                    #io.imsave(B+'{}.png'.format(save_id), gt_im[i:i+DIM, j:j+DIM])
                    # A_ = A+'{}.png'.format(save_id)
                    # B_ = B+'{}.png'.format(save_id)
                full_li.append((i,j,save_id))
                save_id  = save_id +1
                    # print(B+'{}.png'.format(save_id))
            except Exception as E:
                print(E)
                pass
                #
                # os.system("gdal_translate {} {} -of 'PNG' -srcwin {} {} {} {} > /dev/null".format(top,A+'{}.png'.format(save_id),i,j,DIM,DIM))
                # os.system("gdal_translate {} {} -of 'PNG' -srcwin {} {} {} {} > /dev/null".format(gt,B+'{}.png'.format(save_id),i,j,DIM,DIM))
    
    range_list = []
    
    p = Pool(num_processes)
    print("length of full_li is", len(full_li))
    shp_path = ROOT_DIR + "{}.shp".format(id)
    if resize_factor != 1:    
        for i in range(len(full_li)):
            range_list.append((top2, gt2, A, B, full_li, i, shp_path))
        # range_list.append((top_im,gt_im,A,B,full_li,i))1
        #range_list.append((i,batch_size))
    else:
        for i in range(len(full_li)):
            range_list.append((top, gt, A, B, full_li, i, shp_path))
        # range_list.append((top_im,gt_im,A,B,full_li,i))1
    print(len(range_list))
    p.starmap(slice_gdal, range_list)
    top_im = None
    gt_im = None
    p.close()

