import os
from osgeo import gdal
import numpy as np
import geopandas as gpd
from multiprocessing import Pool


def clip_tif(tif_path, shp_path, out_tif_dir):           
    out_tif_path = os.path.join(out_tif_dir, shp_path.split('/')[-1].split('.')[0] + '.tif')
    os.system('gdalwarp -q -of GTiff -cutline {} -crop_to_cutline {} {}'.format(shp_path, tif_path, out_tif_path))
    print('generating ', out_tif_path)

def clip_tif_GIC_imagery(tif_path, shp_dir, out_tif_dir):           
    shp_path = os.path.join(shp_dir, tif_path.split('/')[-1].replace('.tif', '.shp'))
    out_tif_path = os.path.join(out_tif_dir, shp_path.split('/')[-1].split('.')[0] + '.tif')
    os.system('gdalwarp -q -of GTiff -cutline {} -crop_to_cutline {} {}'.format(shp_path, tif_path, out_tif_path))
    print('generating ', out_tif_path)

def delete_empty_raster(raster_path): ## TODO: only because can't find a inbuilt gdalwarp function to skip clipping if raster is not intersecting with shapefile
    raster_np = np.array(gdal.Open(raster_path).ReadAsArray())
    if np.all(raster_np == 0):
        os.remove(raster_path)

def split_aoi_shapefile(shp_path, out_path, tif_name):
    df = gpd.read_file(shp_path)        
    for i in range (len(df.geometry)):				
        newdata = gpd.GeoDataFrame(geometry = gpd.GeoSeries(df.geometry[i]))
        newdata.crs = df.crs
        if len(df.geometry) > 1:            
            save_path = os.path.join(out_path, shp_path.split('/')[-1].split('.')[0] + '_' + str(i) + '_' + tif_name + '.shp')		
        else:
            save_path = os.path.join(out_path, shp_path.split('/')[-1].split('.')[0] + '_' + tif_name + '.shp')
        if not newdata.empty:            		
            newdata.to_file(save_path)
            			

if __name__ == '__main__':   
    
    in_tifs_dir = '' # path of a single tiff or a dir containing tifs
    shapefiles_dir = ''
    out_tif_dir = ''
    num_processes = 11

    os.makedirs(out_tif_dir, exist_ok=True)
    if os.path.isdir(in_tifs_dir):    
        tifs_list = [os.path.join(in_tifs_dir, tiff_name) for tiff_name in os.listdir(in_tifs_dir) if tiff_name.endswith('.tif')]
    else:
        tifs_list = [in_tifs_dir]
    shps_list = [os.path.join(shapefiles_dir, shp_name) for shp_name in os.listdir(shapefiles_dir) if shp_name.endswith('.shp') or shp_name.endswith('.geojson')]
    
    ## STEP 1: split multipolygon aoi shapefile (if any) to single polygon
    print('Splitting multipolygon aoi shapefile (if any) to single polygon')
    task1_list = []
    save_dir = os.path.join(out_tif_dir, 'clipped_aois')
    os.makedirs(save_dir, exist_ok=True)
    for tif in tifs_list:
        for shp in shps_list:
            task1_list.append([shp, save_dir, tif.split('/')[-1].split('.')[0]])
    p = Pool(num_processes)
    p.starmap(split_aoi_shapefile, task1_list)
    p.close()
    p.join()
    
    ## STEP 2: clip rasters
    print('Clipping Rasters')
    task2_list = []
    shps_list = [os.path.join(save_dir, shp_name) for shp_name in os.listdir(save_dir) if shp_name.endswith('.shp') or shp_name.endswith('.geojson')]
    for tif in tifs_list:
        for shp in shps_list:
            task2_list.append([tif, shp, out_tif_dir])    
    p = Pool(num_processes)
    p.starmap(clip_tif, task2_list)
    p.close()
    p.join()

    ## STEP 3: remove empty rasters
    print('Removing empty rasters')
    task3_list = [[os.path.join(out_tif_dir, tiff_name)] for tiff_name in os.listdir(out_tif_dir) if tiff_name.endswith('.tif')]    
    p = Pool(num_processes)
    p.starmap(delete_empty_raster, task3_list)
    p.close()
    p.join()

    
