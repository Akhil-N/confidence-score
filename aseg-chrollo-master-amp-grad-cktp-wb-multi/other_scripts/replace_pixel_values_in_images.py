import os
from PIL import Image
import numpy as np
from multiprocessing import Pool, Manager, Process
from tqdm import tqdm
import time


def calulate_pixels_count_in_image(class_values, images_folder):
    images_paths = [os.path.join(images_folder, path) for path in os.listdir(images_folder)]
    class_pixel_count_dict = {}
    for class_value in class_values:
        class_pixel_count_dict[class_value] = 0
    

    #calculate pixel_count

    for image_path in tqdm(images_paths):
        pil_img = Image.open(image_path)
        img_np = np.array(pil_img)
        if 5 in img_np :
            img_np[img_np == 5] = 3
            img_pil = Image.fromarray(img_np)
            img_pil.save(image_path)






if __name__ == "__main__":
    images_folder = "/media/canis/DATA/Naman/FM_global/imagery_dataset/entire_dataset/2-class-1856-18Jan/trainB"
    class_values = [1, 2, 3]
    calulate_pixels_count_in_image(class_values, images_folder)