import os
from osgeo import gdal
import numpy as np
from tqdm import tqdm
from glob import glob
from multiprocessing import Pool

def clip_shp(aoi_path, out_shp_path, in_shp_path):
    cmd = f'ogr2ogr -clipsrc {aoi_path} {out_shp_path} {in_shp_path} -nlt POLYGON -skipfailures'
    os.system(cmd)

def rasterize(rgb_raster_path, shp_path, num_classes, gt_raster_path, padding):
    rgb_image = gdal.Open(rgb_raster_path)       
    rgb_x = rgb_image.RasterXSize
    rgb_y = rgb_image.RasterYSize
    gt = rgb_image.GetGeoTransform()
    xmin = gt[0]
    ymax = gt[3]
    xmax = xmin + gt[1] * rgb_x
    ymin = ymax + gt[5] * rgb_y
    
    layer_name = shp_path.split('/')[-1].split('.')[0]    
    xmin_new = gt[0] - gt[1] * padding
    ymax_new = gt[3] - gt[5] * padding
    gt_new = (xmin_new, gt[1], gt[2], ymax_new, gt[4], gt[5])
    
    # creating base raster with all values = num_classes
    driver = gdal.GetDriverByName('GTiff')
    base_gt = driver.Create(gt_raster_path, rgb_x + 2*padding, rgb_y + 2*padding, 1, gdal.GDT_Byte)
    base_gt.SetGeoTransform(gt_new)
    proj = rgb_image.GetProjection()
    base_gt.SetProjection(proj)
    raster = np.ones((rgb_y + 2*padding, rgb_x + 2*padding)) * (num_classes + 1)
    base_gt.GetRasterBand(1).WriteArray(raster)
    base_gt = None 
    for i in range(num_classes, 0, -1):        
        temp = '{}'.format(i)                           
        os.system(f'gdal_rasterize -q -a feature -where "feature={temp}" -l {layer_name} {shp_path} {gt_raster_path}')
        # gdal.Rasterize(gt_raster_path, final_vector, format = 'GTiff', where = 'feature={}'.format(temp))      


if __name__ == "__main__":
    # Path to the directory containing the tiff images
    root_dir = '/home/pictor/workspace/Akhil/Confidence/Data/ResTrueGreenV2' 
    # Path to the features shapefile
    in_shp_path = '/home/pictor/workspace/Akhil/Confidence/Data/ResTrueGreenV2/shrnkdBldngs/shrinkdBldngs.shp'    
    padding = 0         # Extra padding (in pixels) that will be added to the rasterized images
    num_classes = 1     # No. of classes in the annotations shapefiles.
    num_processes = 16   # advisable to keep it ~ quarter the number of cores available (due to memory restrictions)
    #Input
    aoi_shps_dir = os.path.join(root_dir, 'aois_clipped')
    #Output
    gt_dir = os.path.join(root_dir, f'gt_{num_classes+1}_classes_shrinkdBldngs')
    clipped_shp_dir = os.path.join(root_dir, 'clipped_features_shps_shrinkdBldngs')
    os.makedirs(gt_dir, exist_ok=True)
    os.makedirs(clipped_shp_dir, exist_ok=True)            
    
    task_list = []
    for tif in tqdm(glob(f"{root_dir}/clipped_tifs/*tif")):
         # TODO: add try exception
        tif = os.path.basename(tif)
        tif_name = tif.split('.')[0]
        rgb_raster_path = os.path.join(root_dir,'clipped_tifs', tif)
        gt_raster_path = os.path.join(gt_dir, f'{tif_name}.tif')
        aoi_path = os.path.join(aoi_shps_dir, f'{tif_name}.shp')
        out_shp_path = os.path.join(clipped_shp_dir, f'{tif_name}_final.shp')
        task_list.append([rgb_raster_path, out_shp_path, num_classes, gt_raster_path, padding])
        clip_shp(aoi_path, out_shp_path, in_shp_path)

    p = Pool(num_processes)
    p.starmap(rasterize, task_list)
    p.close()
    p.join()