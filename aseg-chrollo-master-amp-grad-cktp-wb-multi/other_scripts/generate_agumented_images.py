import os
import PIL
from PIL import Image 





root_in = "/media/canis/DATA/InsurTech/Test-Data-Real/nearmap/1_yaml_rotated_90"
root_out_flipped = root_in + "_rotated_270"

"""
PIL.Image.FLIP_LEFT_RIGHT
PIL.Image.FLIP_TOP_BOTTOM
PIL.Image.ROTATE_90
PIL.Image.ROTATE_180
PIL.Image.ROTATE_270

"""
img_transform = PIL.Image.ROTATE_270
if not os.path.exists(root_out_flipped):
    os.makedirs(root_out_flipped)

list_images = [ os.path.join(root_in, img_path) for img_path in os.listdir(root_in)]

for image_path in list_images:
    img = Image.open(image_path)

    out = img.transpose(img_transform)

    out.save(os.path.join(root_out_flipped, image_path.split("/")[-1]))
