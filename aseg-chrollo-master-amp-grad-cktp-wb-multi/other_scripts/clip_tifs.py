from defaults_clip_tifs import _C as cfg
import argparse
import os
from trains import Task
from multiprocessing import Pool
from clip_tifs_trains import split_aoi_shapefile as split_aoi_shapefile
from clip_tifs_trains import clip_tif as clip_tif
from clip_tifs_trains import delete_empty_raster as delete_empty_raster

#-----------------------------------------------------------------------

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description = 'aseg-chrollo dataset creation: clip tifs')
    parser.add_argument('--cfg', metavar = 'FILE', help = 'path to config file', type = str)
    args = parser.parse_args()

    cfg.merge_from_file(args.cfg)

    task = Task.init('Clip tifs', 'Semantic segmentation data creation: clip tifs')
    task.connect(cfg)

    if cfg.CLIP_TIFS.clip:
        in_tifs_dir = cfg.CLIP_TIFS.in_tifs_dir
        shapefiles_dir = cfg.CLIP_TIFS.shapefiles_dir
        out_tif_dir = cfg.CLIP_TIFS.out_tif_dir
        num_processes = cfg.CLIP_TIFS.num_processes

        os.makedirs(out_tif_dir, exist_ok=True)
        if os.path.isdir(in_tifs_dir):    
            tifs_list = [os.path.join(in_tifs_dir, tiff_name) for tiff_name in os.listdir(in_tifs_dir) if tiff_name.endswith('.tif')]
        else:
            tifs_list = [in_tifs_dir]
        shps_list = [os.path.join(shapefiles_dir, shp_name) for shp_name in os.listdir(shapefiles_dir) if shp_name.endswith('.shp') or shp_name.endswith('.geojson')]
        
        ## STEP 1: split multipolygon aoi shapefile (if any) to single polygon
        print('Splitting multipolygon aoi shapefile (if any) to single polygon')
        task1_list = []
        save_dir = os.path.join(out_tif_dir, 'clipped_aois')
        os.makedirs(save_dir, exist_ok=True)
        for tif in tifs_list:
            for shp in shps_list:
                task1_list.append([shp, save_dir, tif.split('/')[-1].split('.')[0]])
        p = Pool(num_processes)
        p.starmap(split_aoi_shapefile, task1_list)
        p.close()
        p.join()
        
        ## STEP 2: clip rasters
        print('Clipping Rasters')
        task2_list = []
        shps_list = [os.path.join(save_dir, shp_name) for shp_name in os.listdir(save_dir) if shp_name.endswith('.shp') or shp_name.endswith('.geojson')]
        for tif in tifs_list:
            for shp in shps_list:
                task2_list.append([tif, shp, out_tif_dir])    
        p = Pool(num_processes)
        p.starmap(clip_tif, task2_list)
        p.close()
        p.join()

        ## STEP 3: remove empty rasters
        print('Removing empty rasters')
        task3_list = [[os.path.join(out_tif_dir, tiff_name)] for tiff_name in os.listdir(out_tif_dir) if tiff_name.endswith('.tif')]    
        p = Pool(num_processes)
        p.starmap(delete_empty_raster, task3_list)
        p.close()
        p.join()