import os
import shutil
from tqdm import tqdm

trainA_dir = "/media/canis/DATA/Naman/FM_global/imagery_dataset/entire_dataset/2-class-1856-18Jan/trainA"
# trainB_dir = ""
temp_out_dir = "/media/canis/DATA/Naman/FM_global/imagery_dataset/entire_dataset/removed_4-class-1856-18Jan/"

trainA_images_paths = [os.path.join(trainA_dir, path) for path in os.listdir(trainA_dir)]
count = 0
for trainA_image_path in tqdm(trainA_images_paths):
    size_of_image = os.path.getsize(trainA_image_path)
    bytes_in_mb = 1000000
    size_of_image_in_mb = size_of_image / bytes_in_mb
    
    if size_of_image_in_mb < 1.0 :
        out_A = os.path.join(temp_out_dir, "trainA", trainA_image_path.split("/")[-1])
        out_B = os.path.join(temp_out_dir, "trainB", trainA_image_path.split("/")[-1])
        trainB_image_path = trainA_image_path.replace('trainA', 'trainB')
        shutil.move(trainA_image_path, out_A)
        shutil.move(trainB_image_path, out_B)
        count += 1

print("total files moved", count)

    