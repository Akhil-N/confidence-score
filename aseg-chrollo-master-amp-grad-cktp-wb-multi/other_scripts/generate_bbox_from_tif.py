import os



def create_bbox_from_tif(input_tif, output_shp):
    cmd = "gdaltindex {} {}".format(output_shp, input_tif)
    os.system(cmd)


def run_func_over_the_folder(input_dir):
    input_tifs_paths = [os.path.join(input_dir, path) for path in os.listdir(input_dir)]
    output_shps_paths = [os.path.join(input_dir, path.replace('.tif', '.shp')) for path in os.listdir(input_dir)]

    for index, input_tif_path in enumerate(input_tifs_paths):
        create_bbox_from_tif(input_tif_path, output_shps_paths[index])
        


if __name__ == "__main__":
    input_dir = "/media/canis/DATA/InsurTech/Preparing-dataset/till53/clipped_tifs"
    run_func_over_the_folder(input_dir)