from yacs.config import CfgNode as CN

# -----------------------------------------------------------------------------
# Config definition
# -----------------------------------------------------------------------------

_C = CN()

# -----------------------------------------------------------------------------
# batching_data
# -----------------------------------------------------------------------------
_C.BATCHING = CN()
_C.BATCHING.perform_batching = True    # Whether to preform batching (create image chips) or not.
_C.BATCHING.dim = 1400                 # dimension (in pixels) of the image chips to  be generated
_C.BATCHING.stride = 750               # stride (in pixels) to be kept
_C.BATCHING.padding = 0                # Padding (in pixels) present in the PIXELSd images
_C.BATCHING.num_processes = 1          # No. of processes for multiprocessing
_C.BATCHING.num_classes = 6            # Number of classes in rasterized tiff image. Used for path reference.
_C.BATCHING.rgb_tif_dir = ''           # Path to the directory containing tiff (rgb) images
_C.BATCHING.out_dir = ''               # Path to the directory where you want to generate the chips

# -----------------------------------------------------------------------------
# compute pixel ratio and replace pixels
# -----------------------------------------------------------------------------

_C.PIXELS = CN()
_C.PIXELS.compute_pixel_ratio = True   # Whether to calculate pixels ratio (distribution) or not
_C.PIXELS.imgs_dir = ''                # Path to the directory containing the images
_C.PIXELS.class_values = [1, 2, 3, 4, 5, 6, 7]       # Expected classes in the images.
_C.PIXELS.replace_values = True        # If found unwanted value, whether to replace or not
_C.PIXELS.value_to_be_replaced = 0     # pixel value that needs to be replaced
_C.PIXELS.replacing_value = 7          # pixel value that will over-write the value_to_be_replaced
_C.PIXELS.num_processes = 1           # No. of processes for multiprocessing


# -----------------------------------------------------------------------------
# Remove images having black portion above a certain threshold 
# -----------------------------------------------------------------------------

_C.REMOVE_IMAGES = CN()
_C.REMOVE_IMAGES.remove_images = True   # Whether to prune training data for rgb images having black portion above a certain threshold
_C.REMOVE_IMAGES.images_folder = ''     # Path to the directory containing the trainA and trainB folder
_C.REMOVE_IMAGES.percent_threshold = 70 # Percent thresold for removing images
_C.REMOVE_IMAGES.num_processes = 1     # No. of processes for multiprocessing

# -----------------------------------------------------------------------------
# Make .odgt file 
# -----------------------------------------------------------------------------

_C.MAKE_ODGT = CN()
_C.MAKE_ODGT.make_odgt = True           # Whether to make .odgt file or not
_C.MAKE_ODGT.in_dir = ''                # Path to the directory containing the trainA and trainB folder
_C.MAKE_ODGT.out_dir = ''               # Path to the directory where you want to generate the .odgt files
_C.MAKE_ODGT.val_split_percent = 5      # Validation split
_C.MAKE_ODGT.width = 1400               # Width of the images
_C.MAKE_ODGT.height = 1400              # Height of the images