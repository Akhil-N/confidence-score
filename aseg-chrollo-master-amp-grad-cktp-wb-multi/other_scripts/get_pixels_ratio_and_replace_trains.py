import os
from PIL import Image
import numpy as np
from multiprocessing import Pool
from tqdm import tqdm

def get_pixel_count_in_image(imgs_dir, class_values, value_to_be_replaced, replace_values = True):
    imgs_paths = [os.path.join(imgs_dir, path) for path in os.listdir(imgs_dir)]
    class_pixel_count_dict = {}
    for class_value in class_values:
        class_pixel_count_dict[class_value] = 0   
    for img_path in tqdm(imgs_paths):        
        img_np = np.array(Image.open(img_path))
        unique, counts = np.unique(img_np, return_counts=True)
        dict_temp = dict(zip(unique, counts))
        for key in dict_temp.keys():
            if key == value_to_be_replaced:
                if replace_values:
                    return 'replace'
                else:
                    print('found {} pixel value in '.format(value_to_be_replaced), img_path)
            if key not in class_pixel_count_dict:
                class_pixel_count_dict[key] = 0
            class_pixel_count_dict[key] += dict_temp[key]
    return class_pixel_count_dict

def replace_pixel_value(img_path, value_to_be_replaced, replacing_value):
    img_np = np.array(Image.open(img_path))
    if value_to_be_replaced in img_np:
        img_np[img_np == value_to_be_replaced] = replacing_value
        Image.fromarray(img_np).save(img_path)  

def get_pixels_percentage(class_pixel_count_dict, imgs_dir):
    total_no_pixels = 0
    for key in class_pixel_count_dict:
        total_no_pixels += class_pixel_count_dict[key]
    for key in class_pixel_count_dict:
        percent_of_value = (class_pixel_count_dict[key]/total_no_pixels)*100
        print("Class - {}, Percentage - {} ".format(key, percent_of_value))      
        with open(os.path.join(imgs_dir.replace('/' + imgs_dir.split('/')[-1], ''), 'pixels_distribution.txt'), 'a') as pixels_distribution:
                pixels_distribution.write('Class: {}, Percentage: {}\n'.format(key, percent_of_value))

        
#----------------------------------------------------------------------------------------

if __name__ == '__main__':
    imgs_dir = ''
    num_processes = 11
    class_values = [1, 2, 3, 4, 5, 6, 7]
    replace_values = True
    value_to_be_replaced = 0
    replacing_value = 7

    print('Getting class wise pixel percentage for all images.')
    bool_replace_values = get_pixel_count_in_image(imgs_dir, class_values, value_to_be_replaced, replace_values = replace_values)

    if bool_replace_values == 'replace':
        print('Found {} pixel value in a image. Running replacing function over all images.'.format(value_to_be_replaced))
        task_list = []
        for img_name in os.listdir(imgs_dir):
            task_list.append([os.path.join(imgs_dir, img_name), value_to_be_replaced, replacing_value])
        p = Pool(num_processes)
        p.starmap(replace_pixel_value, task_list)
        p.close()
        p.join()
        print('Replaced values')
        print('Getting class wise pixel percentage for all images.')
        class_pixel_count_dict = get_pixel_count_in_image(imgs_dir, class_values, value_to_be_replaced, replace_values = replace_values)
    else:
        class_pixel_count_dict = bool_replace_values

    get_pixels_percentage(class_pixel_count_dict, imgs_dir)
