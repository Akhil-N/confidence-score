import trains
from torch.utils.tensorboard import SummaryWriter
import argparse
import torch
import numpy as np
import glob
from trains import Task
parser = argparse.ArgumentParser(
        description="Plotting history.pth"
    )
parser.add_argument(
        "--dir",
        default="",
        metavar="FILE",
        help="path to history pth files",
        type=str,
    )
parser.add_argument(
        "--name",
        default="training",
        metavar="FILE",
        help="name to give to project in trains ui",
        type=str,
    )
    
args = parser.parse_args()
writer = SummaryWriter()
#task = Task.init('Plots of history',args.name)
for files in sorted(glob.glob(args.dir + '/history_*.pth')):

    epoch = int(files.split('_')[-1].split('.')[0])
    history = torch.load(files)
    writer.add_scalar('Loss/loss_epoch',np.mean(history['train']['loss']),epoch)
    writer.add_scalar('Accuracy/accuracy_epoch',np.mean(history['train']['acc']),epoch)
