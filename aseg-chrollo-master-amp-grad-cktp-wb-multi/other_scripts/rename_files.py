import os



root_path = "/media/canis/DATA/InsurTech/Test-Data-Real/FINAL_TEST/images"

file_paths = [ os.path.join(root_path, file_name) for file_name in os.listdir(root_path)]

for file_path in file_paths:
    os.rename(file_path, file_path.replace(".jpg", ".png"))