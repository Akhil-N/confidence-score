from defaults_batching_to_odgt import _C as cfg
import argparse
import os
# from trains import Task
from multiprocessing import Pool
from batching_data_gdal_trains import verify_gt_and_rgb_tiff ,get_image_dimensions,create_slice_task_list,slice_gdal
from get_pixels_ratio_and_replace_trains import get_pixel_count_in_image ,replace_pixel_value ,get_pixels_percentage
from remove_images import move_images as move_images
from make_odgt_file_trains import make_odgt as make_odgt

#-----------------------------------------------------------------------

if __name__ == '__main__':

    # parser = argparse.ArgumentParser(description = 'aseg-chrollo dataset creation: Batching; Count pixels ratio, prune images, make .odgt file')
    # parser.add_argument('--cfg', metavar = 'FILE', help = 'path to config file', type = str)
    # args = parser.parse_args()
    args = {
        'cfg' : "/media/catila/a5ee59fd-ca0b-4ee4-bc60-0a51e7ec42bd/akhil/codes/aseg-chrollo-master-amp-grad-cktp-wb-multi/data_creation_configs/batching_to_odgt.yaml"
    }
    cfg.merge_from_file(args['cfg'])

    # task = Task.init('Batching to odgt', 'Semantic segmentation data creation: Batching, compute pixel ratio, replace pixels, remove images, make .odgt file')
    # task.connect(cfg)

#-----------------------------------------------------------------------
    if cfg.BATCHING.perform_batching:
        print('Batching data (Creating image chips)')        
        dim = cfg.BATCHING.dim
        stride = cfg.BATCHING.stride
        padding = cfg.BATCHING.padding
        num_processes = cfg.BATCHING.num_processes
        num_classes = cfg.BATCHING.num_classes
        rgb_tif_dir = cfg.BATCHING.rgb_tif_dir
        out_dir = cfg.BATCHING.out_dir

        gt_tif_dir = os.path.join(rgb_tif_dir, 'gt_{}_classes'.format(num_classes + 1))        
        aoi_shp_dir = os.path.join(rgb_tif_dir, 'aois_clipped')    
        save_id = 0

        out_trainA_path = os.path.join(out_dir, 'trainA')
        out_trainB_path = os.path.join(out_dir, 'trainB')
        os.makedirs(out_trainA_path, exist_ok=True)
        os.makedirs(out_trainB_path, exist_ok=True)    
        
        rgb_tif_dir = os.path.join(rgb_tif_dir,'clipped_tifs')
        verify_gt_and_rgb_tiff(rgb_tif_dir, gt_tif_dir)

        
        for rgb_tif in os.listdir(rgb_tif_dir):
            if rgb_tif.endswith('.tif'): 
                rgb_path = os.path.join(rgb_tif_dir, rgb_tif)
                gt_path = os.path.join(gt_tif_dir, rgb_tif.replace('.tif', '_gt.tif'))
                width, height = get_image_dimensions(rgb_path)
                aoi_shp_path = os.path.join(aoi_shp_dir, rgb_tif.replace('.tif', '.shp'))            

                with open(os.path.join(out_dir, 'batching_numbers.txt'), 'a') as batching_file:
                    batching_file.write('Image Path: {}, Start chip id: {}\n'.format(rgb_path, save_id))

                task_list, save_id = create_slice_task_list(rgb_path, gt_path, out_trainA_path, out_trainB_path, aoi_shp_path, save_id, width, height, stride, dim, padding)           
                p = Pool(num_processes)
                p.starmap(slice_gdal, task_list)
                p.close()
                p.join()
#-----------------------------------------------------------------------

    if cfg.PIXELS.compute_pixel_ratio: 
        if cfg.BATCHING.perform_batching:
            imgs_dir = os.path.join(cfg.BATCHING.out_dir, 'trainB')
        else:      
            imgs_dir = cfg.PIXELS.imgs_dir
        class_values = cfg.PIXELS.class_values
        replace_values = cfg.PIXELS.replace_values
        value_to_be_replaced = cfg.PIXELS.value_to_be_replaced
        replacing_value = cfg.PIXELS.replacing_value
        num_processes =  cfg.PIXELS.num_processes

        print('Getting class wise pixel percentage for all images.')
        bool_replace_values = get_pixel_count_in_image(imgs_dir, class_values, value_to_be_replaced, replace_values = replace_values)

        if bool_replace_values == 'replace':
            print('Found {} pixel value in a image. Running replacing function over all images.'.format(value_to_be_replaced))
            task_list = []
            for img_name in os.listdir(imgs_dir):
                task_list.append([os.path.join(imgs_dir, img_name), value_to_be_replaced, replacing_value])
            p = Pool(num_processes)
            p.starmap(replace_pixel_value, task_list)
            p.close()
            p.join()
            print('Getting class wise pixel percentage for all images.')
            class_pixel_count_dict = get_pixel_count_in_image(imgs_dir, class_values, value_to_be_replaced, replace_values = replace_values)
        else:
            class_pixel_count_dict = bool_replace_values
        get_pixels_percentage(class_pixel_count_dict, imgs_dir)
#-----------------------------------------------------------------------

    if cfg.REMOVE_IMAGES.remove_images:
        print('Removing images')
        if cfg.BATCHING.perform_batching:
            images_folder = cfg.BATCHING.out_dir
        else:      
            images_folder = cfg.REMOVE_IMAGES.images_folder        
        percent_threshold = cfg.REMOVE_IMAGES.percent_threshold
        num_processes = cfg.REMOVE_IMAGES.num_processes

        trainA_dir = os.path.join(images_folder, 'trainA')
        trainB_dir = os.path.join(images_folder, 'trainB')
        removed_dir_trainA = os.path.join(images_folder, 'removed_images/trainA')
        removed_dir_trainB = os.path.join(images_folder, 'removed_images/trainB')    
        os.makedirs(removed_dir_trainA, exist_ok=True)
        os.makedirs(removed_dir_trainB, exist_ok=True)
        
        task_list = []
        p = Pool(num_processes)
        images_paths = [os.path.join(trainA_dir, path) for path in os.listdir(trainA_dir)]        
        for image_path in images_paths:
            task_list.append((image_path, trainB_dir, removed_dir_trainA, removed_dir_trainB, percent_threshold))
        p.starmap(move_images, task_list)    
        p.close()
#-----------------------------------------------------------------------

    if cfg.MAKE_ODGT.make_odgt:
        print('Creating .odgt file')
        if cfg.BATCHING.perform_batching:
            in_dir = cfg.BATCHING.out_dir
            out_dir = cfg.BATCHING.out_dir
        else:
            in_dir = cfg.MAKE_ODGT.in_dir
            out_dir = cfg.MAKE_ODGT.out_dir             
        val_split_percent = cfg.MAKE_ODGT.val_split_percent
        width = cfg.MAKE_ODGT.width
        height = cfg.MAKE_ODGT.height

        make_odgt(in_dir, out_dir, val_split_percent, width, height)