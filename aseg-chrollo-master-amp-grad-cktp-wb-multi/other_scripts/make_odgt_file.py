import os

input_dir = "/media/canis/DATA/InsurTech/Prepared-Datasets/2-class-1856-18Jan"
output_dir = "/media/canis/DATA/InsurTech/Prepared-Datasets/2-class-1856-18Jan"

WIDTH_CONSTANT = 1024
HEIGHT_CONSTANT = 1024
validation_split_percent = 5

trainA_path = os.path.join(input_dir, "trainA")
trainB_path = os.path.join(input_dir, "trainB")
trainA_images_list = os.listdir(trainA_path)
validation_images_list = trainA_images_list[: int((len(trainA_images_list) * validation_split_percent/100))]
validation_images_dir = os.path.join(input_dir, "validation", "images")
os.makedirs((validation_images_dir), exist_ok=True)
validation_annotations_dir = os.path.join(input_dir, "validation", "annotations")
os.makedirs((validation_annotations_dir), exist_ok=True)
for img in validation_images_list:
    os.rename(os.path.join(trainA_path, img), os.path.join(validation_images_dir, img))
    os.rename(os.path.join(trainB_path, img), os.path.join(validation_annotations_dir, img))

os.makedirs((os.path.join(input_dir, "training", "images")), exist_ok=True)
os.makedirs((os.path.join(input_dir, "training", "annotations")), exist_ok=True)
os.rename(trainA_path, os.path.join(input_dir, "training", "images"))
os.rename(trainB_path, os.path.join(input_dir, "training", "annotations"))

training_images = os.path.join(input_dir, "training", "images")
training_annotation = os.path.join(input_dir, "training", "annotations")
validation_images = os.path.join(input_dir, "validation", "images")
validation_annotation = os.path.join(
    input_dir, "validation", "annotations")

odgt_training_file = os.path.join(
    output_dir, "training.odgt")
odgt_validation_file = os.path.join(
    output_dir, "validation.odgt")



f = open(odgt_training_file, "w+")

for image in os.listdir(training_images):
    elem_dict = {}
    elem_dict["fpath_img"] = os.path.join(training_images, image)
    elem_dict["fpath_segm"] = os.path.join(training_annotation, image)
    elem_dict["width"] = WIDTH_CONSTANT
    elem_dict["height"] = HEIGHT_CONSTANT

    f.write(str(elem_dict).replace("'", '"') + "\n")
f.close()

f = open(odgt_validation_file, "w+")

for image in os.listdir(validation_images):
    elem_dict = {}
    elem_dict["fpath_img"] = os.path.join(validation_images, image)
    elem_dict["fpath_segm"] = os.path.join(validation_annotation, image)
    elem_dict["width"] = WIDTH_CONSTANT
    elem_dict["height"] = HEIGHT_CONSTANT

    f.write(str(elem_dict).replace("'", '"')+"\n")
f.close()
