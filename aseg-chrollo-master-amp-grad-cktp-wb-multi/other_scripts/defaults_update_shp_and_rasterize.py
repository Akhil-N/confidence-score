from yacs.config import CfgNode as CN

# -----------------------------------------------------------------------------
# Config definition
# -----------------------------------------------------------------------------

_C = CN()

# -----------------------------------------------------------------------------
# update_shapefiles
# -----------------------------------------------------------------------------
_C.UPDATE_SHAPEFILES = CN()
_C.UPDATE_SHAPEFILES.update_shapefiles = True            # Whether to update the shapefiles or not.
_C.UPDATE_SHAPEFILES.shapefiles_dir = ''
_C.UPDATE_SHAPEFILES.mapping_dict = [{'tree':1, 'swimming':2, 'road':3, 'parking': 4, 'solar':5, 'building':6}] # Values to be populated in the shapfiles
_C.UPDATE_SHAPEFILES.merged_individual_shapefiles = True # Whether to merge individual shapefiles as sent by cartography team
_C.UPDATE_SHAPEFILES.update_field_in_a_shapefile = True  # Whether to update the field in the shapefiles
_C.UPDATE_SHAPEFILES.feature_name = 'feature'            # Name of the field to be updated
_C.UPDATE_SHAPEFILES.merge_combined_shapefiles = True    # Whether to merge the combined shapefiles (corresponding to each class)
_C.UPDATE_SHAPEFILES.fix_geometry = False                # Whether to fix geometry of the merged shapefile.

# -----------------------------------------------------------------------------
# Rasterize
# -----------------------------------------------------------------------------

_C.RASTERIZE = CN()
_C.RASTERIZE.rasterize = True                            # Whether to rasterize the shapefiles or not.
_C.RASTERIZE.num_processes = 1                           # No. of processes for multiprocessing
_C.RASTERIZE.num_classes = 6                             # No. of classes in the shapefiles.
_C.RASTERIZE.padding = 0                                 # Extra padding (in pixels) that will be added to the rasterized images
_C.RASTERIZE.root_dir = ''                               # Path to the directory containing tiff images
_C.RASTERIZE.in_shp_path = ''                            # Path to the features shapefile
