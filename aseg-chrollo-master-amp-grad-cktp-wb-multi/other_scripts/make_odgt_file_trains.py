import os


def make_odgt(in_dir, out_dir, val_split_percent, width, height):
    trainA_path = os.path.join(in_dir, 'trainA')
    trainB_path = os.path.join(in_dir, 'trainB')
    trainA_images_list = os.listdir(trainA_path)
    validation_images_list = trainA_images_list[: int((len(trainA_images_list) * val_split_percent/100))]
    validation_images_dir = os.path.join(in_dir, 'validation', 'images')
    os.makedirs((validation_images_dir), exist_ok=True)
    validation_annotations_dir = os.path.join(in_dir, 'validation', 'annotations')
    os.makedirs((validation_annotations_dir), exist_ok=True)
    for img in validation_images_list:
        os.rename(os.path.join(trainA_path, img), os.path.join(validation_images_dir, img))
        os.rename(os.path.join(trainB_path, img), os.path.join(validation_annotations_dir, img))

    os.makedirs((os.path.join(in_dir, 'training', 'images')), exist_ok=True)
    os.makedirs((os.path.join(in_dir, 'training', 'annotations')), exist_ok=True)
    os.rename(trainA_path, os.path.join(in_dir, 'training', 'images'))
    os.rename(trainB_path, os.path.join(in_dir, 'training', 'annotations'))

    training_images = os.path.join(in_dir, 'training', 'images')
    training_annotation = os.path.join(in_dir, 'training', 'annotations')
    validation_images = os.path.join(in_dir, 'validation', 'images')
    validation_annotation = os.path.join(
        in_dir, 'validation', 'annotations')

    odgt_training_file = os.path.join(
        out_dir, 'training.odgt')
    odgt_validation_file = os.path.join(
        out_dir, 'validation.odgt')

    f = open(odgt_training_file, 'w+')

    for image in os.listdir(training_images):
        elem_dict = {}
        elem_dict['fpath_img'] = os.path.join(training_images, image)
        elem_dict['fpath_segm'] = os.path.join(training_annotation, image)
        elem_dict['width'] = width
        elem_dict['height'] = height

        f.write(str(elem_dict).replace("'", '"') + '\n')
    f.close()

    f = open(odgt_validation_file, 'w+')

    for image in os.listdir(validation_images):
        elem_dict = {}
        elem_dict['fpath_img'] = os.path.join(validation_images, image)
        elem_dict['fpath_segm'] = os.path.join(validation_annotation, image)
        elem_dict['width'] = width
        elem_dict['height'] = height

        f.write(str(elem_dict).replace("'", '"')+'\n')
    f.close()

#------------------------------------------------------------------
if __name__ == '__main__':

    in_dir = ''
    out_dir = ''

    width = 1400
    height = 1400
    val_split_percent = 5

    make_odgt(in_dir, out_dir, val_split_percent, width, height)
