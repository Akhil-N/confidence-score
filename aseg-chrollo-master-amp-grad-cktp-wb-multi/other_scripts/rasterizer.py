import os
from osgeo import gdal
import numpy as np

num_classes = 5       # num_classes=(n+1), where n = types of features (eg. trees, buildings, road)
root_dir = "/media/ursamajor/1df17740-bab5-4762-9749-b349b34bd2f3/FM_global/sp_temp/clipped_tifs/"
in_vector_path = "/media/ursamajor/1df17740-bab5-4762-9749-b349b34bd2f3/FM_global/sp_temp/clipped_tifs/shp/merged_swimming_pool.shp"
stride = 1500
# root_dir is the directory containing the tifs; and corresponding geojsons of aoi for each tif
# this root_dir should contain a folder named shp, which should contain features shapefile corresponding to each tif

filepaths = os.listdir(root_dir)
only_tif = []

for i in filepaths:
    if ".tif" in i:
        only_tif.append(root_dir + i)
# print(only_tif)
# only_tif is the list containing paths of all the tifs

count = 0
for single_rgb_tif in only_tif:

    id =single_rgb_tif.split('.')[0].split('/')[-1]

    print("############CURRENT ID IS {} ############".format(id))
    rgb_path   = root_dir + "{}.tif".format(id)

    raster_path = root_dir +"{}_gt.tif".format(id.split('_')[0])
#     in_vector_path = root_dir+"shp/{}.shp".format(id.split('_')[0])
#     in_vector_path = "/media/ursamajor/66832096-52e4-43eb-8028-36a8f03ba2101/training_data/Naman/FM_global/pilot_1_building/root_dir/shp/Building.shp"
#     geojson_path = root_dir + "{}.geojson".format(id)
    geojson_path = root_dir+"{}.shp".format(id)
    final_vector = root_dir+"shp/{}_final.shp".format(id.split('_')[0])

    print("Step-1 Vector Clipping")
    cmd = "ogr2ogr -clipsrc {} {} {} -nlt POLYGON -skipfailures"
    os.system(cmd.format(geojson_path, final_vector, in_vector_path))
    # the format is "ogr2ogr -clipsrc clipping_polygon.shp output.shp input.shp"

    rgb_image = gdal.Open(rgb_path)        
    global_x = rgb_image.RasterXSize
    global_y = rgb_image.RasterYSize
    gt = rgb_image.GetGeoTransform()
    xmin = gt[0]
    ymax = gt[3]
    xmax = xmin + gt[1] * global_x
    ymin = ymax + gt[5] * global_y
    print ("SHAPE OF ALL RASTERS ", global_x,global_y)
    
    layer_name = final_vector.split("/")[-1].split('.')[0]    
    xmin_new = gt[0] - gt[1] * (stride+1)
    ymax_new = gt[3] - gt[5] * (stride+1)
    gt_new = (xmin_new, gt[1], gt[2], ymax_new, gt[4], gt[5])
    
    # creating base raster with all values = num_classes
    driver = gdal.GetDriverByName('GTiff')
    base_gt = driver.Create(raster_path, global_x + 2*(stride+1), global_y + 2*(stride+1), 1, gdal.GDT_Byte)
    base_gt.SetGeoTransform(gt_new)
    proj = rgb_image.GetProjection()
    base_gt.SetProjection(proj)
    raster = np.ones((global_y + 2*(stride+1), global_x + 2*(stride+1))) * num_classes    
    base_gt.GetRasterBand(1).WriteArray(raster)
    base_gt = None
    
#     os.system('gdal_rasterize -ot byte -burn 255 -burn 255 -burn 255 -l {} -ts {} {} {} {}'.format(layer_name, global_x, global_y, final_vector, raster_path))

#   The following command rasterizes the shapefile based on the values of the attribute named "feature"    
    # print("gdal_rasterize -ot byte -a feature -where 'feature={}' -l {} -te {} {} {} {} -ts {} {} {} {}".format(temp, layer_name, xmin, ymin, xmax, ymax, global_x, global_y, final_vector, raster_path))
    # os.system("gdal_rasterize -ot byte -a feature -where 'feature={}' -l {} -te {} {} {} {} -ts {} {} {} {} -a_nodata 5".format(temp, layer_name, xmin, ymin, xmax, ymax, global_x, global_y, final_vector, raster_path))
    # os.system("gdal_rasterize -ot byte -a feature -where 'feature={}' -l {} -te {} {} {} {} -ts {} {} {} {}".format(temp, layer_name, xmin, ymin, xmax, ymax, global_x, global_y, final_vector, raster_path))
    for i in range(num_classes, 0, -1):        
        temp = '{}'.format(i)                    
        # os.system("gdal_rasterize -ot byte -a feature -where 'feature={}' -l {} -te {} {} {} {} -ts {} {} {} {}".format(temp, layer_name, xmin, ymin, xmax, ymax, global_x, global_y, final_vector, raster_path))
        os.system("gdal_rasterize -a feature -where 'feature={}' -l {} {} {}".format(temp, layer_name, final_vector, raster_path))
        # gdal.Rasterize(raster_path, final_vector, format = "GTiff", where = 'feature={}'.format(temp))
    print("Rasterization Completed for {} RGB Image".format(id))
    count = count + 1      

print("rasterized total ", count, "tifs")