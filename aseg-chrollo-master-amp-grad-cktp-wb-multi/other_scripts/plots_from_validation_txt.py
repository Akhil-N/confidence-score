import pandas as pd
from torch.utils.tensorboard import SummaryWriter
import argparse
import os
parser = argparse.ArgumentParser(
        description="Plotting validation file"
    )
parser.add_argument(
        "--file",
        default="/",
        metavar="FILE",
        help="path to validation.txt file",
        type=str,
    )
args = parser.parse_args()
writer = SummaryWriter()
with open(args.file) as f:
    a = f.readlines()
#print(a[0].split(','))
for i in range(len(a)):
    temp = a[i].split(',')
    temp1 = ','.join(a[i].split(',')[5:]).split('\'')
    writer.add_scalar('{}/mean_iou'.format(os.path.basename(os.path.dirname((args.file)))),float(temp[1].split(':')[1]),i+1)
    writer.add_scalar('{}/Accuracy'.format(os.path.basename(os.path.dirname((args.file)))),float(temp[2].split(':')[1][:-1]),i+1)
    writer.add_scalar('{}/Loss'.format(os.path.basename(os.path.dirname((args.file)))),float(temp[4].split(':')[1][:-1]),i+1)
    for j in range(int((len(temp1)-1)/2)):
        writer.add_scalar('{}/IOU/class_{}'.format(os.path.basename(os.path.dirname((args.file))),int(temp1[2*j+1].split('IoU:')[0].split('class [')[1][0])),float(temp1[2*j+1].split('IoU:')[1]),i+1)
#a = pd.read_csv(args.file,sep=" ",header = None)
writer.close()
