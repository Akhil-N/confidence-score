import os
import cv2
import numpy as np
from multiprocessing import Pool, Manager, Process
from tqdm import tqdm
import time


def RGB_Mean_StdDev(imgs_dir, save_txt_path, ignore_value=0):
    count = 0
    r_mean_cumulative, g_mean_cumulative, b_mean_cumulative = 0, 0, 0
    r_stdDev_cumulative, g_stdDev_cumulative, b_stdDev_cumulative = 0, 0, 0

    for filename in tqdm(os.listdir(imgs_dir)):
        img = cv2.imread(os.path.join(imgs_dir, filename))
        if img is not None:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

            redChannel = np.reshape(img[:, :, 0], -1)
            r_mean_cumulative += redChannel[redChannel != ignore_value].mean()
            r_stdDev_cumulative += redChannel[redChannel != ignore_value].std()
            # r_mean_cumulative += np.mean(redChannel)
            # r_stdDev_cumulative += np.std(redChannel) * np.std(redChannel)

            greenChannel = np.reshape(img[:, :, 1], -1)
            g_mean_cumulative += greenChannel[greenChannel != ignore_value].mean()
            g_stdDev_cumulative += greenChannel[greenChannel != ignore_value].std()
            # g_mean_cumulative += np.mean(greenChannel)
            # g_stdDev_cumulative += np.std(greenChannel) * np.std(greenChannel)

            blueChannel = np.reshape(img[:, :, 2], -1)
            b_mean_cumulative += blueChannel[blueChannel != ignore_value].mean()
            b_stdDev_cumulative += blueChannel[blueChannel != ignore_value].std()
            # b_mean_cumulative += np.mean(blueChannel)
            # b_stdDev_cumulative += np.std(blueChannel) * np.std(blueChannel)

            count += 1
            img = None
    r_mean, g_mean, b_mean = r_mean_cumulative / \
        count, g_mean_cumulative / count, b_mean_cumulative / count
    r_stdDev, g_stdDev, b_stdDev = np.sqrt(r_stdDev_cumulative / count), np.sqrt(g_stdDev_cumulative / count), \
        np.sqrt(b_stdDev_cumulative / count)
    print("r_mean {}".format(r_mean/255.0))
    print("g_mean {}".format(g_mean/255.0))
    print("b_mean {}".format(b_mean/255.0))
    print("r_stdDev {}".format(r_stdDev/255.0))
    print("g_stdDev {}".format(g_stdDev/255.0))
    print("b_stdDev {}".format(b_stdDev/255.0))
    # return [r_mean, g_mean, b_mean, r_stdDev, g_stdDev, b_stdDev]
    with open(save_txt_path, 'a') as txt_file:
        txt_file.write("r_mean {}\n".format(r_mean/255.0))
        txt_file.write("g_mean {}\n".format(g_mean/255.0))
        txt_file.write("b_mean {}\n".format(b_mean/255.0))
        txt_file.write("r_stdDev {}\n".format(r_stdDev/255.0))
        txt_file.write("g_stdDev {}\n".format(g_stdDev/255.0))
        txt_file.write("b_stdDev {}\n".format(b_stdDev/255.0))

def RGB_Mean_StdDev_multiprocesing(images_dir, image_path, count_cumulative, r_mean_cumulative, g_mean_cumulative, b_mean_cumulative, r_stdDev_cumulative, g_stdDev_cumulative, b_stdDev_cumulative):
    img = cv2.imread(os.path.join(images_dir, image_path))    
    if img is not None:        
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        redChannel = np.reshape(img[:, :, 0], -1)        
        r_mean_cumulative[0] += np.mean(redChannel)
        r_stdDev_cumulative[0] += np.std(redChannel) * np.std(redChannel)

        greenChannel = np.reshape(img[:, :, 1], -1)
        g_mean_cumulative[0] += np.mean(greenChannel)
        g_stdDev_cumulative[0] += np.std(greenChannel) * np.std(greenChannel)

        blueChannel = np.reshape(img[:, :, 2], -1)
        b_mean_cumulative[0] += np.mean(blueChannel)
        b_stdDev_cumulative[0] += np.std(blueChannel) * np.std(blueChannel)        

        count_cumulative[0] += 1        
        progress_bar.update(num_processes)    


if __name__ == '__main__':
    
    imgs_dir = "/media/catila/a5ee59fd-ca0b-4ee4-bc60-0a51e7ec42bd/akhil/lawns_buildings/train_data_final/training/images"
    save_txt_path = "/media/catila/a5ee59fd-ca0b-4ee4-bc60-0a51e7ec42bd/akhil/lawns_buildings/train_data_final/normalization_values.txt"
    # num_processes = 12
    time_start = time.time()
    RGB_Mean_StdDev(imgs_dir, save_txt_path)   
    print('total time taken = {} seconds'.format(time.time() - time_start))

    # NOTE: Currently, there are some issues with multiprocessing. Everytime the multiprocessing function is used, we get different 
    # normalization values (beacuse not all the images are used) For now, use the function 'RGB_Mean_StdDev' only.

    # image_paths = os.listdir(imgs_dir)
    # r_mean_cumulative, g_mean_cumulative, b_mean_cumulative, r_stdDev_cumulative = Manager().list(), Manager().list(), Manager().list(), Manager().list()
    # g_stdDev_cumulative, b_stdDev_cumulative, count_cumulative = Manager().list(), Manager().list(), Manager().list()

    # r_mean_cumulative.append(0), g_mean_cumulative.append(0), b_mean_cumulative.append(0), r_stdDev_cumulative.append(0)
    # g_stdDev_cumulative.append(0), b_stdDev_cumulative.append(0), count_cumulative.append(0)

    # progress_bar = tqdm(total = len(image_paths))

    # task_list = []
    # for i in range(len(image_paths)):
    #     task_list.append([imgs_dir, image_paths[i], count_cumulative, r_mean_cumulative, g_mean_cumulative, b_mean_cumulative, r_stdDev_cumulative, g_stdDev_cumulative, b_stdDev_cumulative])        

    # p = Pool(num_processes)    
    # p.starmap(RGB_Mean_StdDev_multiprocesing, task_list)    
    # p.close()   # ideally, this should ensure that all processes have comleted. But this is not the case.  
    # p.join()    
    
    # print("r_mean ", r_mean_cumulative[0]/len(image_paths)/255.0)
    # print("g_mean ", g_mean_cumulative[0]/len(image_paths)/255.0)
    # print("b_mean ", b_mean_cumulative[0]/len(image_paths)/255.0)
    # print("r_stdDev ", np.sqrt(r_stdDev_cumulative[0]/len(image_paths))/255.0)
    # print("g_stdDev ", np.sqrt(g_stdDev_cumulative[0]/len(image_paths))/255.0)
    # print("b_stdDev ", np.sqrt(b_stdDev_cumulative[0]/len(image_paths))/255.0)
    # print('count ', count_cumulative)
    # print('total time taken = {} seconds'.format(time.time() - time_start))
