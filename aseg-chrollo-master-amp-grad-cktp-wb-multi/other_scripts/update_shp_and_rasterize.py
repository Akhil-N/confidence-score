from defaults_update_shp_and_rasterizer import _C as cfg
import argparse
import os
from trains import Task
from multiprocessing import Pool
from update_shapefiles import merge_shapefiles as merge_shapefiles
from update_shapefiles import update_field_in_a_shapefile as update_field_in_a_shapefile
from update_shapefiles import fix_geometry as fix_geometry
from rasterizer_trains import clip_shp as clip_shp
from rasterizer_trains import rasterize as rasterize

#-----------------------------------------------------------------------

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description = 'aseg-chrollo dataset creation: update shapefiles and rasterize')
    parser.add_argument('--cfg', metavar = 'FILE', help = 'path to config file', type = str)
    args = parser.parse_args()

    cfg.merge_from_file(args.cfg)

    task = Task.init('Update shapefile and rasterize', 'Semantic segmentation data creation: Update shapefile and rasterize')
    task.connect(cfg)

    if cfg.UPDATE_SHAPEFILES.update_shapefiles:
        print('Updating Shapefiles')
        shapefiles_dir = cfg.UPDATE_SHAPEFILES.shapefiles_dir
        mapping_dict = cfg.UPDATE_SHAPEFILES.mapping_dict[0]
        feature_name = cfg.UPDATE_SHAPEFILES.feature_name

        ## STEP 1: Merging class-wise individual shapefiles (consisting of features)   
        if cfg.UPDATE_SHAPEFILES.merged_individual_shapefiles:            
            os.makedirs(os.path.join(shapefiles_dir, 'merged_shps'), exist_ok=True) 
            for class_name in mapping_dict:
                merge_shapefiles(os.path.join(shapefiles_dir, class_name), save_name = class_name)

        ## STEP 2: Addition of field named 'feature' and corresponding value
        if cfg.UPDATE_SHAPEFILES.update_field_in_a_shapefile:        
            update_field_in_a_shapefile(os.path.join(shapefiles_dir, 'merged_shps'), mapping_dict, feature_name = feature_name)

        ## STEP 3: Merge the shapefiles to create a common feature shapefile, which will be used later for rasterization
        if cfg.UPDATE_SHAPEFILES.merge_combined_shapefiles:            
            merged_shapefile_name = merge_shapefiles(os.path.join(shapefiles_dir, 'merged_shps'))

        if cfg.UPDATE_SHAPEFILES.fix_geometry:
            ## STEP 4: Check validilty and Fix geometry. Alternative: v.clean            
            fix_geometry(shapefiles_dir, merged_shapefile_name, fix_geom = True)

#-----------------------------------------------------------------------
    if cfg.RASTERIZE.rasterize:
        print('Rasterizing')
        num_processes = cfg.RASTERIZE.num_processes  # advisable to keep it ~ quarter the number of cores available (due to memory restrictions)
        num_classes = cfg.RASTERIZE.num_classes      
        root_dir = cfg.RASTERIZE.root_dir    
        padding = cfg.RASTERIZE.padding
        if cfg.UPDATE_SHAPEFILES.update_shapefiles:
            if cfg.UPDATE_SHAPEFILES.merge_combined_shapefiles:            
                in_shp_path = os.path.join(shapefiles_dir, 'merged_shps', merged_shapefile_name + '.shp')
        else:        
            in_shp_path = cfg.RASTERIZE.in_shp_path        
        aoi_shps_dir = os.path.join(root_dir, 'clipped_aois')
        gt_dir = os.path.join(root_dir, 'gt_{}_classes'.format(num_classes+1))
        clipped_shp_dir = os.path.join(root_dir, 'clipped_features_shps')
        os.makedirs(gt_dir, exist_ok=True)
        os.makedirs(clipped_shp_dir, exist_ok=True)     

        task_list = []
        for tif in os.listdir(root_dir):
            if tif.endswith('.tif'): 
                tif_name = tif.split('.')[0]
                rgb_raster_path = os.path.join(root_dir, tif)
                gt_raster_path = os.path.join(gt_dir, '{}_gt.tif'.format(tif_name))
                aoi_path = os.path.join(aoi_shps_dir, '{}.shp'.format(tif_name))
                out_shp_path = os.path.join(clipped_shp_dir, '{}_final.shp'.format(tif_name))
                task_list.append([rgb_raster_path, out_shp_path, num_classes, gt_raster_path, padding])
                clip_shp(aoi_path, out_shp_path, in_shp_path)

        p = Pool(num_processes)
        p.starmap(rasterize, task_list)
        p.close()
        p.join()