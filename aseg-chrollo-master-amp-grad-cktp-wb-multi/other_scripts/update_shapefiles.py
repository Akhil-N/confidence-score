import os
from osgeo import ogr
import os

# ---------------------------------------------------------------------------------------------------------------------------
# This script expects the input 'shapefile_dir' to contain folders named as keys of the 'mapping_dict'.
# These folders should contain the individual feature shapefiles. A merged shapefile for each class (key) 
# will be generated in a folder named 'merged_shps'. Then a field 'feature' will be added in each of these merged shapefile
# and will be populated according to the values in 'mapping_dict'.
# Lastly, a merged shapefile will be generated consisting of all these merged classes (keys).
# ---------------------------------------------------------------------------------------------------------------------------

def update_field_in_a_shapefile(shapefiles_dir, mapping_dict, feature_name = 'feature'):
    for key in mapping_dict:
        for shapefile_name in os.listdir(shapefiles_dir):
            if shapefile_name.endswith('.shp') and key in shapefile_name:            
                delete_field_from_a_shapefile(shapefiles_dir, shapefile_name, feature_name)
                add_field_in_a_shapefile(shapefiles_dir, shapefile_name, mapping_dict[key], feature_name)

def delete_field_from_a_shapefile(shapefiles_dir, shapefile_name, feature_name = 'feature'):
    source = ogr.Open(os.path.join(shapefiles_dir, shapefile_name), update=True)
    layer = source.GetLayer()            
    layer_defn = layer.GetLayerDefn()
    for _ in range(layer_defn.GetFieldCount()):
        field_name = layer_defn.GetFieldDefn(_).name                
        if field_name == feature_name:                    
            layer.DeleteField(_)
            break
    source.Destroy()

def add_field_in_a_shapefile(shapefiles_dir, shapefile_name, value, feature_name = 'feature'):
    source = ogr.Open(os.path.join(shapefiles_dir, shapefile_name), update=True)
    layer = source.GetLayer()
    new_field = ogr.FieldDefn(feature_name, ogr.OFTInteger64)
    layer.CreateField(new_field) 
    for feature in layer:                
        feature.SetField(feature_name, value)
        layer.SetFeature(feature)
    source.Destroy()

def merge_shapefiles(shapefiles_dir, save_name = ''):    
    merged_name = 'merged'
    temp = ''
    for shapefile in os.listdir(shapefiles_dir):
        if shapefile.endswith('.shp'):        
            merged_name += '_{}'.format(shapefile.split('.')[0])
            temp += ' {}'.format(os.path.join(shapefiles_dir, shapefile))
    if save_name == '':        
        out_path = os.path.join(shapefiles_dir, merged_name + '.shp')    
    else:
        shapefiles_dir = shapefiles_dir.replace('/' + shapefiles_dir.split('/')[-1], '')
        out_path = os.path.join(shapefiles_dir, 'merged_shps', save_name + '.shp')    
    cmd = 'ogrmerge.py -single -o {} {}'.format(out_path, temp)
    os.system(cmd)
    return merged_name

def fix_geometry(shapefiles_dir, merged_shapefile_name, fix_geom = False):
    if fix_geom:
        from qgis.core import QgsApplication
        QgsApplication.setPrefixPath("/usr", True)
        qgs = QgsApplication([], False, None)
        qgs.initQgis()
        sys.path.append('/usr/lib/qgis')
        sys.path.append('/usr/share/qgis/python/plugins')
        from qgis.core import (QgsApplication, QgsProcessingFeedback, QgsVectorLayer, QgsVectorFileWriter)
        from qgis.analysis import QgsNativeAlgorithms
        import processing
        from processing.core.Processing import Processing
        in_path = os.path.join(shapefiles_dir, merged_shapefile_name + '.shp')
        out_path = os.path.join(shapefiles_dir, 'fixed_geom_' + merged_shapefile_name + '.shp')
        processing.run("native:fixgeometries", parameters={"INPUT": in_path, "OUTPUT": out_path})



if __name__ == "__main__":
    shapefiles_dir = '/media/catila/a5ee59fd-ca0b-4ee4-bc60-0a51e7ec42bd/akhil/trugreen_v2/data/TG_V2/standard'
    mapping_dict = {'Front':1, 'Back':1, 'Left':1, 'Right': 1,'Building':2}
    
    ## STEP 1: Merging class-wise individual shapefiles (consisting of features)   
    os.makedirs(os.path.join(shapefiles_dir, 'merged_shps'), exist_ok=True)
    for class_name in mapping_dict:
        merge_shapefiles(os.path.join(shapefiles_dir, class_name), save_name = class_name)
        # merge_shapefiles(os.path.join(shapefiles_dir, class_name), save_name = class_name)
    # merge_shapefiles(shapefiles_dir, save_name = 'lawns')

    ## STEP 2: Addition of field named 'feature' and corresponding value
    update_field_in_a_shapefile(os.path.join(shapefiles_dir, 'merged_shps'), mapping_dict, feature_name = 'feature')

    ## STEP 3: Merge the shapefiles to create a common feature shapefile, which will be used later for rasterization
    merged_shapefile_name = merge_shapefiles(os.path.join(shapefiles_dir, 'merged_shps'))

    ## STEP 4: Check validilty and Fix geometry. Alternative: v.clean
    # TODO: This function has not been tested as of now.
    fix_geometry(shapefiles_dir, merged_shapefile_name, fix_geom = False)


    
