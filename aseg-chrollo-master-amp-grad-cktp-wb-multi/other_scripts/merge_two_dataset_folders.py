import os
import shutil
from tqdm import tqdm


def merge_two_dataset_folders(first_folder, second_folder):
    image_list = os.listdir(second_folder+"/trainA")

    for image in tqdm(image_list):
        src_trainA = os.path.join(second_folder, "trainA", image)
        src_trainB = os.path.join(second_folder, "trainB", image)
        des_trainA = os.path.join(first_folder, "trainA", image.replace(".", "_2."))
        des_trainB = os.path.join(first_folder, "trainB", image.replace(".", "_2."))


        shutil.copyfile(src_trainA, des_trainA)
        shutil.copyfile(src_trainB, des_trainB)
        



if __name__ == "__main__":
    first_folder = "/media/canis/DATA/Naman/FM_global/imagery_dataset/entire_dataset/2-class-1856-18Jan/"
    second_folder = "/media/canis/DATA/Naman/FM_global/imagery_dataset/entire_dataset/2-class-1856-18Jan_2"
    merge_two_dataset_folders(first_folder, second_folder)
    