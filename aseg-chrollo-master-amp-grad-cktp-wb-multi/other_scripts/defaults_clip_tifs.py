from yacs.config import CfgNode as CN

# -----------------------------------------------------------------------------
# Config definition
# -----------------------------------------------------------------------------

_C = CN()

# -----------------------------------------------------------------------------
# CLIP_TIFS
# -----------------------------------------------------------------------------
_C.CLIP_TIFS = CN()
_C.CLIP_TIFS.clip = True                    # Whether to clip tifs or not
_C.CLIP_TIFS.in_tifs_dir = ''               # path of a single tiff or a dir containing tifs
_C.CLIP_TIFS.shapefiles_dir = ''            # path to directory containing aoi shapefiles
_C.CLIP_TIFS.out_tif_dir = ''               # path to the directory where the clipped tifs will be generated
_C.CLIP_TIFS.num_processes = 1             # No. of processes for multiprocessing