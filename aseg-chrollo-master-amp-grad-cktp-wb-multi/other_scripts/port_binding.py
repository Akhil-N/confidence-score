#Run this code on local system
import os
import argparse
parser = argparse.ArgumentParser(
        description="Run this on local system to start bind ports on localhost"
    )
parser.add_argument(
        "--ip",
        help="IP address",
        type=str,
    )
args = parser.parse_args()

os.system('sudo kill -9 $(sudo lsof -t -i:9080)')
os.system('sudo kill -9 $(sudo lsof -t -i:8081)')
os.system('sudo kill -9 $(sudo lsof -t -i:8008)')
os.system('sudo kill -9 $(sudo lsof -t -i:6006)')

os.system('ssh -N -f -L localhost:8081:localhost:8081 mahaveer_attentive_ai@{}'.format(args.ip))
os.system('ssh -N -f -L localhost:8008:localhost:8008 mahaveer_attentive_ai@{}'.format(args.ip))
os.system('ssh -N -f -L localhost:9080:localhost:9080 mahaveer_attentive_ai@{}'.format(args.ip))
os.system('ssh -N -f -L localhost:6006:localhost:6006 mahaveer_attentive_ai@{}'.format(args.ip))
