import os
from skimage import io
from osgeo import gdal
import warnings
warnings.filterwarnings('ignore')
from multiprocessing import Pool
from shapely import geometry
import shapefile


def verify_gt_and_rgb_tiff(rgb_tif_dir, gt_tif_dir):
    rgb_tif_names = [rgb_tif for rgb_tif in os.listdir(rgb_tif_dir) if rgb_tif.endswith('.tif')]    
    gt_tif_names = [gt_tif for gt_tif in os.listdir(gt_tif_dir) if gt_tif.endswith('.tif')]    
    if len(rgb_tif_names) == len(gt_tif_names):        
        for rgb_tif in rgb_tif_names:
            gt_tif_name = rgb_tif.replace('.tif', '_gt.tif')
            if gt_tif_name not in gt_tif_names:
                print('All RGB tiffs do not have corresponding grount truth raster. Still moving on.')
    else:
        print('All RGB tiffs do not have corresponding grount truth raster. Still moving on.')


def get_image_dimensions(rgb_tif_path):    
    rgb_raster = io.imread(rgb_tif_path)    
    width_rgb, height_rgb = rgb_raster.shape[0:2]    
    return width_rgb, height_rgb        


def get_shapely_object_from_raster(input_raster, pixel_bounds = None):
    ds = gdal.Open(input_raster)
    gt = ds.GetGeoTransform()
    xsize,ysize = ds.RasterXSize,ds.RasterYSize
    if not pixel_bounds:
        minx = gt[0]
        miny = gt[3] + xsize * gt[4] + ysize * gt[5]
        maxx = gt[0] + xsize * gt[1] + ysize * gt[2]
        maxy = gt[3]
        return geometry.Polygon([[minx,miny],[maxx,miny],[maxx,maxy],[minx,maxy]])        
    else:
        minx = gt[0] + pixel_bounds[0][0] * gt[1] + pixel_bounds[0][1] * gt[2]
        miny = gt[3] + pixel_bounds[1][0] * gt[4] + pixel_bounds[1][1] * gt[5]
        maxx = gt[0] + pixel_bounds[1][0] * gt[1] + pixel_bounds[1][1] * gt[2]
        maxy = gt[3] + pixel_bounds[0][0] * gt[4] + pixel_bounds[0][1] * gt[5]
        return geometry.Polygon([[minx, miny], [maxx, miny], [maxx, maxy], [minx, maxy]])        

 
def slice_gdal(in_top, in_gt, out_trainA, out_trainB, j, i, save_id, shp_path, dim, padding=0):      
    out_A = os.path.join(out_trainA, '{}.png'.format(save_id))
    out_B = os.path.join(out_trainB, '{}.png'.format(save_id))
    # img_boundary = get_shapely_object_from_raster(in_top, pixel_bounds=[(i, j),(i + dim, j + dim)])     
    # all_shapes = shapefile.Reader(shp_path).shapes()     
    # for s in range(len(all_shapes)):                                 
    #     if img_boundary.intersects(geometry.shape(all_shapes[s])):                                        
    gdal.Translate(out_A, in_top, srcWin=[i, j, dim, dim], format="PNG", outputType=gdal.GDT_Byte)
    gdal.Translate(out_B, in_gt, srcWin=[i + (padding), j + (padding), dim, dim], format="PNG", outputType=gdal.GDT_Byte)
    try:
        os.remove(out_A + '.aux.xml')
        os.remove(out_B + '.aux.xml')
    except Exception as e:
        print(e)
        print('Moving on.')


def create_slice_task_list(rgb_raster_path, gt_raster_path, out_trainA_path, out_trainB_path, aoi_shp_path, save_id, width, height, stride, dim, padding=0):
    task_list = []
    for i in range(0, width+1, stride):
        for j in range(0, height+1, stride):                           
            task_list.append([rgb_raster_path, gt_raster_path, out_trainA_path, out_trainB_path, i, j, save_id, aoi_shp_path, dim, padding])
            save_id  = save_id +1      
    return task_list, save_id               
               

def temp_save_geometry_shp(img_boundary, out_trainA, save_id):
    from shapely.geometry import mapping
    import fiona    
    schema = {'geometry': 'Polygon','properties': {'id': 'int'}}
    # Write a new Shapefile
    with fiona.open(os.path.join(out_trainA, '{}.shp'.format(save_id)), 'w', 'ESRI Shapefile', schema) as c:        
        c.write({'geometry': mapping(img_boundary),'properties': {'id': 123}})


#------------------------------------------------------------------------------

if __name__ == "__main__":    

    dim = 1500                      # dimension (in pixels) of the image chips to  be generated
    stride = 750                    # stride (in pixels) to be kept
    padding = 0                     # Padding (in pixels) present in the rasterized images
    num_processes = 11              # No. of processes for multiprocessing
    num_classes = 6                 # Number of classes in rasterized tiff image. Used for path reference.
    rgb_tif_dir = ''                # Path to the directory containing tiff (rgb) images
    out_dir = ''                    # Path to the directory where you want to generate the chips

    gt_tif_dir = os.path.join(rgb_tif_dir, 'gt_{}_classes'.format(num_classes + 1))
    aoi_shp_dir = os.path.join(rgb_tif_dir, 'clipped_aois')    
    save_id = 0

    out_trainA_path = os.path.join(out_dir, 'trainA')
    out_trainB_path = os.path.join(out_dir, 'trainB')
    os.makedirs(out_trainA_path, exist_ok=True)
    os.makedirs(out_trainB_path, exist_ok=True)    
    verify_gt_and_rgb_tiff(rgb_tif_dir, gt_tif_dir)

    for rgb_tif in os.listdir(rgb_tif_dir):
        if rgb_tif.endswith('.tif'): 
            rgb_path = os.path.join(rgb_tif_dir, rgb_tif)
            gt_path = os.path.join(gt_tif_dir, rgb_tif.replace('.tif', '_gt.tif'))
            width, height = get_image_dimensions(rgb_path)
            aoi_shp_path = os.path.join(aoi_shp_dir, rgb_tif.replace('.tif', '.shp'))            

            with open(os.path.join(out_dir, 'batching_numbers.txt'), 'a') as batching_file:
                batching_file.write('Image Path: {}, Start chip id: {}\n'.format(rgb_path, save_id))

            task_list, save_id = create_slice_task_list(rgb_path, gt_path, out_trainA_path, out_trainB_path, aoi_shp_path, save_id, width, height, stride, dim, padding)           
            p = Pool(num_processes)
            p.starmap(slice_gdal, task_list)
            p.close()
            p.join()


