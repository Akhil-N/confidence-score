import os
from PIL import Image
import numpy as np
from tqdm import tqdm
from shutil import move
from multiprocessing import Pool

def move_images(image_path, trainB_dir, removed_dir_trainA, removed_dir_trainB, percent_threshold):
    img_name = image_path.split('/')[-1]    
    pil_img = Image.open(image_path)
    img_np = np.array(pil_img)    
    if 0 in img_np :        
        # num_pixels_0 = (img_np[img_np == 0]).size  
        num_pixels_0 = (img_np[:,:,0][img_np[:,:,0] == 0]).size +  (img_np[:,:,1][img_np[:,:,1] == 0]).size + (img_np[:,:,2][img_np[:,:,2] == 0]).size 
        percent_pixels_0 = (float(num_pixels_0)/img_np.size) * 100        
        if percent_pixels_0 >= percent_threshold:            
            move(image_path, os.path.join(removed_dir_trainA, img_name))            
            move(os.path.join(trainB_dir, img_name), os.path.join(removed_dir_trainB, img_name))
            # progress_bar.update(num_processes)    

if __name__ == "__main__":
    
    images_folder = ''
    percent_threshold = 70
    num_processes = 20
    
    trainA_dir = os.path.join(images_folder, 'trainA')
    trainB_dir = os.path.join(images_folder, 'trainB')
    removed_dir_trainA = os.path.join(images_folder, 'removed_images/trainA')
    removed_dir_trainB = os.path.join(images_folder, 'removed_images/trainB')    

    os.makedirs(removed_dir_trainA, exist_ok=True)
    os.makedirs(removed_dir_trainB, exist_ok=True)

    
    task_list = []
    p = Pool(num_processes)
    images_paths = [os.path.join(trainA_dir, path) for path in os.listdir(trainA_dir)]
    # progress_bar = tqdm(total = len(images_paths))    
    for image_path in images_paths:
        task_list.append((image_path, trainB_dir, removed_dir_trainA, removed_dir_trainB, percent_threshold))
    p.starmap(move_images, task_list)    
    p.close()

