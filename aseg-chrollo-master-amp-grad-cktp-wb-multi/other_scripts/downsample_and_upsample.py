import os
import time
from osgeo import gdal
from PIL import Image

img_dir = "/media/ursamajor/1df17740-bab5-4762-9749-b349b34bd2f3/FM_global/normalization_test/new_images_downsampled"
out_path = "/media/ursamajor/1df17740-bab5-4762-9749-b349b34bd2f3/FM_global/normalization_test/nearmap_images_20cm"
downsampling_factor = 2.667
upsampling_factor = 4
desired_output_format = "JPEG"

os.makedirs((out_path), exist_ok=True)

def downsample_with_gdal(img_dir, downsampling_factor, desired_output_format):
    images = os.listdir(img_dir)
    time_start_global = time.time()
    for image in images:
        if image.endswith(".tif") or image.endswith(".png") or image.endswith(".jpg"):
            time_start = time.time()
            img_path = os.path.join(img_dir, image)            
            downsampled_img_path = os.path.join(out_path, image)
            im = gdal.Open(img_path)
            im_width, im_height = im.RasterXSize, im.RasterYSize                
            gdal.Translate(downsampled_img_path, img_path, width=(im_width // downsampling_factor), height=(im_height // downsampling_factor), format=desired_output_format, outputType=gdal.GDT_Byte)  # TODO target width and height should be detetrmined through some other way
            # gdal.Translate(downsampled_img_path, img_path, width=(im_width // downsampling_factor), height=(im_height // downsampling_factor), widthPct=100, heightPct=100,format=desired_output_format, outputType=gdal.GDT_Byte) 
            print("resized {} of width {} and height {} in {} seconds".format(image, im_width, im_height, time.time()-time_start))
    print("total time taken = {} seconds".format(time.time() - time_start_global))    

def upsample_with_gdal(img_dir, upsampling_factor, desired_output_format):
    images = os.listdir(img_dir)
    for image in images:
        if image.endswith(".tif") or image.endswith(".png") or image.endswith(".jpg"):
            time_start = time.time()
            img_path = os.path.join(img_dir, image)            
            upsampled_img_path = os.path.join(out_path, image)
            im = gdal.Open(img_path)
            im_width, im_height = im.RasterXSize, im.RasterYSize                            
            gdal.Translate(upsampled_img_path, img_path, width=(im_width*upsampling_factor), height=(im_height*upsampling_factor), format=desired_output_format, outputType=gdal.GDT_Byte) # TODO target width and height should be detetrmined through some other way
            print("resized {} of width {} and height {} in {} seconds".format(image, im_width, im_height, time.time()-time_start))

def upsample_predictions_with_gdal(img_dir, upsampling_factor, desired_output_format):
    folders = os.listdir(img_dir)
    for folder_id in folders:
        print(folder_id)
        # if image.endswith(".tif") or image.endswith(".png"):
        # images = os.listdir(folder)
        # for image in images:
        time_start = time.time()
        img_path = os.path.join(img_dir, folder_id, "{}.png".format(folder_id))            
        upsampled_img_path = os.path.join(out_path, "{}.png".format(folder_id))
        im = gdal.Open(img_path)
        im_width, im_height = im.RasterXSize, im.RasterYSize                            
        gdal.Translate(upsampled_img_path, img_path, width=(im_width*upsampling_factor), height=(im_height*upsampling_factor), format=desired_output_format, outputType=gdal.GDT_Byte) # TODO target width and height should be detetrmined through some other way
        print("resized {} of width {} and height {} in {} seconds".format("{}.png".format(folder_id), im_width, im_height, time.time()-time_start))

def downsample_with_PIL(img_dir, downsampling_factor, desired_output_format):
    images = os.listdir(img_dir)
    time_start_global = time.time()
    for image in images:
        if image.endswith(".tif") or image.endswith(".png") or image.endswith(".jpg"):
            time_start = time.time()
            img_path = os.path.join(img_dir, image)            
            downsampled_img_path = os.path.join(out_path, image)
            im = Image.open(img_path)
            im_width, im_height = im.size                    
            im_resized = im.resize((int(im_width // downsampling_factor), int(im_height // downsampling_factor)), Image.ANTIALIAS)
            im_resized.save(downsampled_img_path, "PNG")
            print("resized {} of width {} and height {} in {} seconds".format(image, im_width, im_height, time.time() - time_start))
    print("total time taken = {} seconds".format(time.time() - time_start_global))

if __name__ == "__main__":

    downsample_with_gdal(img_dir, downsampling_factor, desired_output_format)
