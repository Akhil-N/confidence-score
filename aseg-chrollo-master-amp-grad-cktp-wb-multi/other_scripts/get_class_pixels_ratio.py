import os
from PIL import Image
import numpy as np
from multiprocessing import Pool, Manager, Process
from tqdm import tqdm
import time


def calulate_pixels_count_in_image(class_values, images_folder):
    images_paths = [os.path.join(images_folder, path) for path in os.listdir(images_folder)]
    class_pixel_count_dict = {}
    for class_value in class_values:
        class_pixel_count_dict[class_value] = 0
    

    #calculate pixel_count

    for image_path in tqdm(images_paths):
        pil_img = Image.open(image_path)
        img_np = np.array(pil_img)
        unique, counts = np.unique(img_np, return_counts=True)
        dict_temp = dict(zip(unique, counts))
        for key in dict_temp.keys():
            if key == 0:
                print(image_path)

            if key not in class_pixel_count_dict:
                class_pixel_count_dict[key] = 0
            class_pixel_count_dict[key] += dict_temp[key]
        # print(dict_temp)
        # print(class_pixel_count_dict)
        
    total_no_pixels = 0
    for key in class_pixel_count_dict:
        total_no_pixels += class_pixel_count_dict[key]
    print(total_no_pixels)

    for key in class_pixel_count_dict:
        percent_of_value = (class_pixel_count_dict[key]/total_no_pixels)*100
        print("Class - {}, Percentage - {} ".format(key, percent_of_value))





if __name__ == "__main__":
    images_folder = "/media/canis/DATA/Naman/FM_global/imagery_dataset/entire_dataset/2-class-1856-18Jan/trainB"
    class_values = [1, 2, 3]
    calulate_pixels_count_in_image(class_values, images_folder)