# System libs
import os
import time
import argparse
from distutils.version import LooseVersion
# Numerical libs
import numpy as np
import torch
import torch.nn as nn
from scipy.io import loadmat
# Our libs
from config import cfg
from dataset import ValDataset
from models import ModelBuilder, SegmentationModule
from utils import AverageMeter, colorEncode, accuracy, intersectionAndUnion, setup_logger
from lib.nn import user_scattered_collate, async_copy_to
from lib.utils import as_numpy
from PIL import Image
from tqdm import tqdm
# import cv2
from torch.utils.tensorboard import SummaryWriter

colors = loadmat('data/color150.mat')['colors']


def visualize_result(data, pred, dir_result, writer):
    (img, seg, info) = data

    # segmentation
    seg_color = colorEncode(seg, colors)

    # prediction
    pred_color = colorEncode(pred, colors)

    # aggregate images and save
    im_vis = np.concatenate((img, seg_color, pred_color),
                            axis=1).astype(np.uint8)

    img_name = info.split('/')[-1]

    os.makedirs(dir_result, exist_ok=True)
    Image.fromarray(im_vis).save(os.path.join(dir_result, img_name.replace('.jpg', '.png')))
    # im_vis = cv2.resize(im_vis,(1024,512))
    img_name = img_name.split('.')[0]
    #writer.add_image(img_name, im_vis.transpose(2,0,1),int(dir_result.split('_')[-1]))
    writer.add_image('{}'.format(dir_result.split('_')[-1]), im_vis.transpose(2,0,1),int(img_name))
def evaluate(segmentation_module, loader, cfg, gpu, epoch_id, writer):
    acc_meter = AverageMeter()
    intersection_meter = AverageMeter()
    union_meter = AverageMeter()
    time_meter = AverageMeter()

    segmentation_module.eval()

    pbar = tqdm(total=len(loader))
    for batch_data in loader:
        # process data
        batch_data = batch_data[0]
        seg_label = as_numpy(batch_data['seg_label'][0])
        img_resized_list = batch_data['img_data']

        torch.cuda.synchronize()
        tic = time.perf_counter()
        with torch.no_grad():
            segSize = (seg_label.shape[0], seg_label.shape[1])
            scores = torch.zeros(1, cfg.DATASET.num_class, segSize[0], segSize[1])
            scores = async_copy_to(scores, gpu)

            for img in img_resized_list:
                feed_dict = batch_data.copy()
                feed_dict['img_data'] = img
                del feed_dict['img_ori']
                del feed_dict['info']
                feed_dict = async_copy_to(feed_dict, gpu)

                # forward pass
                # scores_tmp = segmentation_module(feed_dict, segSize=segSize)
                scores_tmp, loss = segmentation_module(feed_dict, segSize=segSize)
                loss = loss.mean()
                scores = scores + scores_tmp / len(cfg.DATASET.imgSizes)

            _, pred = torch.max(scores, dim=1)
            pred = as_numpy(pred.squeeze(0).cpu())

        torch.cuda.synchronize()
        time_meter.update(time.perf_counter() - tic)

        # calculate accuracy
        acc, pix = accuracy(pred, seg_label)
        intersection, union = intersectionAndUnion(pred, seg_label, cfg.DATASET.num_class)
        acc_meter.update(acc, pix)
        intersection_meter.update(intersection)
        union_meter.update(union)

        # visualization
        if cfg.VAL.visualize:
            visualize_result(
                (batch_data['img_ori'], seg_label, batch_data['info']),
                pred,
                os.path.join(cfg.DIR, 'result_epoch_new_val' + epoch_id), writer)

        pbar.update(1)

    # summary    
    iou = intersection_meter.sum / (union_meter.sum + 1e-10)
    iou_values = []    
    for i, _iou in enumerate(iou):
        print('class [{}], IoU: {:.4f}'.format(i, _iou))
        iou_values.append('class [{}], IoU: {:.4f}'.format(i, _iou))
        writer.add_scalar('IOU/Class {}'.format(i),_iou,epoch_id)


    print('[Eval Summary]:')
    print('Mean IoU: {:.4f}, Accuracy: {:.2f}%, Inference Time: {:.4f}s, loss: {:.8f}'
          .format(iou.mean(), acc_meter.average()*100, time_meter.average(), loss))
    writer.add_scalar('Val_Loss',loss,epoch_id)

    # print('Mean IoU: {:.4f}, Accuracy: {:.2f}%, Inference Time: {:.4f}s'
    #       .format(iou.mean(), acc_meter.average()*100, time_meter.average()))

    with open(os.path.join(cfg.DIR, 'validation_values.txt'), 'a') as val_file:
        val_file.write('Epoch: {}, Mean IoU: {:.4f}, Accuracy: {:.2f}%, Inference Time: {:.4f}s, Loss: {:.8f}, IoU values: {}\n'
          .format(epoch_id, iou.mean(), acc_meter.average()*100, time_meter.average(), loss, iou_values))


def main(cfg, gpu, epoch_id, writer):
# def main(cfg, gpu):
    torch.cuda.set_device(gpu)

    # absolute paths of model weights
    cfg.MODEL.weights_encoder = os.path.join(
    cfg.DIR, 'encoder_epoch_' + epoch_id + '.pth')
    cfg.MODEL.weights_decoder = os.path.join(
        cfg.DIR, 'decoder_epoch_' + epoch_id + '.pth')
    
    assert os.path.exists(cfg.MODEL.weights_encoder) and \
        os.path.exists(cfg.MODEL.weights_decoder), "checkpoint does not exitst!"
    assert os.path.exists(
            cfg.MODEL.weights_decoder), "checkpoint does not exitst!"

    # Network Builders
    net_encoder = ModelBuilder.build_encoder(
	cfg,
        arch=cfg.MODEL.arch_encoder.lower(),
        fc_dim=cfg.MODEL.fc_dim,
        weights=cfg.MODEL.weights_encoder)    
    net_decoder = ModelBuilder.build_decoder(
        arch=cfg.MODEL.arch_decoder.lower(),
        fc_dim=cfg.MODEL.fc_dim,
        num_class=cfg.DATASET.num_class,
        weights=cfg.MODEL.weights_decoder,
        use_softmax=True)    

    # crit = nn.NLLLoss(ignore_index=-1)
    loss_weights_list = cfg.TRAIN.loss_weights
    if (all(loss_weight == loss_weights_list[0] for loss_weight in loss_weights_list)):
        print("all loss weights are equal")
        crit = nn.NLLLoss(ignore_index=-1)
    else:
        loss_weights = torch.tensor(loss_weights_list)     
        crit = nn.NLLLoss(ignore_index=-1, weight = loss_weights)

    segmentation_module = SegmentationModule(net_encoder, net_decoder, crit)

    # Dataset and Loader
    dataset_val = ValDataset(
        cfg.DATASET.root_dataset,
        cfg.DATASET.list_val,
        cfg.DATASET,
        cfg)
    loader_val = torch.utils.data.DataLoader(
        dataset_val,
        batch_size=cfg.VAL.batch_size,
        shuffle=False,
        collate_fn=user_scattered_collate,
        num_workers=5,
        drop_last=True)

    segmentation_module.cuda()

    # Main loop
    evaluate(segmentation_module, loader_val, cfg, gpu, epoch_id, writer)    

    print('Evaluation Done!')


if __name__ == '__main__':
    writer = SummaryWriter()
    assert LooseVersion(torch.__version__) >= LooseVersion('0.4.0'), \
        'PyTorch>=0.4.0 is required'

    parser = argparse.ArgumentParser(
        description="PyTorch Semantic Segmentation Validation"
    )
    parser.add_argument(
        "--cfg",
        default="config/attentive-hrnetv2.yaml",
        metavar="FILE",
        help="path to config file",
        type=str,
    )
    parser.add_argument(
        "--gpu",
        default=0,
        help="gpu to use"
    )
    parser.add_argument(
        "opts",
        help="Modify config options using the command-line",
        default=None,
        nargs=argparse.REMAINDER,
    )
    args = parser.parse_args()

    cfg.merge_from_file(args.cfg)
    cfg.merge_from_list(args.opts)
    # cfg.freeze()

    logger = setup_logger(distributed_rank=0)   # TODO
    logger.info("Loaded configuration file {}".format(args.cfg))
    logger.info("Running with config:\n{}".format(cfg))

    # absolute paths of model weights
    # cfg.MODEL.weights_encoder = os.path.join(
    #     cfg.DIR, 'encoder_' + cfg.VAL.checkpoint)
    # cfg.MODEL.weights_decoder = os.path.join(
    #     cfg.DIR, 'decoder_' + cfg.VAL.checkpoint)
    # assert os.path.exists(cfg.MODEL.weights_encoder) and \
    #     os.path.exists(cfg.MODEL.weights_decoder), "checkpoint does not exitst!"
    epoch_id = cfg.VAL.checkpoint.split(".")[0].split("_")[-1]

    if not os.path.isdir(os.path.join(cfg.DIR, "result_epoch_new_val" + epoch_id)):
        print("not directory")
        os.makedirs(os.path.join(cfg.DIR, "result_epoch_new_val" + epoch_id))           
    main(cfg, args.gpu, epoch_id, writer)
