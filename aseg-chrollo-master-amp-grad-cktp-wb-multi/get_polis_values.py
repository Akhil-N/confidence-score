
import os
from test_multi_gpu import multi_main as multi_main
import polis


input_dir = "/media/ursamajor/1df17740-bab5-4762-9749-b349b34bd2f3/FM_global/phase3/polis_metric/temp2"
config_file = "/media/ursamajor/1df17740-bab5-4762-9749-b349b34bd2f3/FM_global/phase3/phase2_building_baseline/aseg-chrollo/config/test.yaml"
out_path = "/media/ursamajor/1df17740-bab5-4762-9749-b349b34bd2f3/FM_global/phase3/temp"
num_classes = 5
aug_mode = 0
start_ckpt = 65
end_ckpt = 66 # Will not run on this ckpt
gpus = 0
window_size = 1400
stride = 700
ignore_edge_pixels = 20
keep_preds = True

gt_shps_dir = '/media/ursamajor/1df17740-bab5-4762-9749-b349b34bd2f3/FM_global/phase3/temp/gt_shps'

if __name__ == '__main__': 
    multi_main(input_dir,config_file,gpus,out_path,num_classes,aug_mode,use_gt=False,start_ckpt = start_ckpt,end_ckpt = end_ckpt,window_size= window_size,stride=stride,ignore_edge_pixels=ignore_edge_pixels,keep_preds = keep_preds)                    
    while start_ckpt < end_ckpt:    
        vectorize_dir = os.path.join(out_path, 'epoch_{}'.format(start_ckpt))           
        polis.raster_to_shape(vectorize_dir)        
        os.makedirs(os.path.join(vectorize_dir, 'polis_shps'), exist_ok=True)
        global_ratio_list = []
        for shp in os.listdir(vectorize_dir):
            if shp.endswith('.shp'):
                print('starting ', shp)
                polis.remove_features(os.path.join(vectorize_dir, shp), 4)
                polis.simplify_geom(os.path.join(vectorize_dir, shp))
                polis_shp_path = os.path.join(vectorize_dir, 'polis_shps', 'polis_'+shp)
                gt_shp = os.path.join(gt_shps_dir, shp.replace('pred', 'final'))
                polis.score(os.path.join(vectorize_dir, shp), gt_shp, polis_shp_path)
                polis.add_perimeter_field(polis_shp_path)
                polis.add_ratio_field(polis_shp_path)
                ratio_list = polis.generate_txt_file(polis_shp_path, polis_shp_path.replace('.shp', '.txt'), global_ratio_list)
                polis.save_histogram(ratio_list, polis_shp_path.replace('pred.shp', 'hist.png'))
        polis.generate_global_txt(global_ratio_list, os.path.join(vectorize_dir, 'polis_shps', 'global_values.txt'))
        polis.save_histogram(global_ratio_list, os.path.join(vectorize_dir, 'polis_shps', 'global_hist.png'))                
        start_ckpt += 1

