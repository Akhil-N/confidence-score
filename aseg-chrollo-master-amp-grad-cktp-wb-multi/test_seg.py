import os
import torch
import torch.nn.functional as F
from PIL import Image
# from unet.dinknet import DinkNet34, DinkNet50, DinkNet101
# from utils_segmentation.utils_seg import resize_and_crop, normalize, hwc_to_chw
# from utils_segmentation.crf import dense_crf
from torchvision import transforms
import numpy as np
from skimage import io
from skimage.transform import resize as io_resize
import matplotlib.pyplot as plt
import time
from osgeo import gdal
Image.MAX_IMAGE_PIXELS=None
import traceback


def georeferencing_from_array(array_input, ref_tiff_file, tiff_output,scale):
	x_pixels = array_input.shape[2]  # number of pixels in x
	y_pixels = array_input.shape[1]  # number of pixels in y
	driver = gdal.GetDriverByName('GTiff')
	dataset = driver.Create(tiff_output, x_pixels, y_pixels, array_input.shape[0], gdal.GDT_Float32)
	for i in range(array_input.shape[0]):
		dataset.GetRasterBand(i+1).WriteArray(array_input[i,:,:])
	img_ref = gdal.Open(ref_tiff_file)

	geotrans = img_ref.GetGeoTransform()  # get GeoTranform from existed 'data0'
	# print(type(geotrans))
	list_geotransform = list(geotrans)
	list_geotransform[1] = list_geotransform[1] * scale
	list_geotransform[5] = list_geotransform[5] * scale
	geotrans = tuple(list_geotransform)
	proj = img_ref.GetProjection()  # you can get from a exsited tif or import
	dataset.SetGeoTransform(geotrans)
	dataset.SetProjection(proj)
	dataset.FlushCache()
	dataset = None


def padding(img,window_size,stride,channels,ignore_edge_pixels):
	try:
		N_channel = channels
		x = window_size + bool((img.shape[0]+2*ignore_edge_pixels) // window_size) * (
					(((img.shape[0]+2*ignore_edge_pixels) - window_size) // stride) + 1 * bool(((img.shape[0]+2*ignore_edge_pixels) - window_size) % stride)) * stride
		y = window_size + bool((img.shape[1]+2*ignore_edge_pixels) // window_size) * (
					(((img.shape[1]+2*ignore_edge_pixels) - window_size) // stride) + 1 * bool(((img.shape[1]+2*ignore_edge_pixels) - window_size) % stride)) * stride
		pad = np.ones(x*y*N_channel)
		pad = pad.reshape(x,y, N_channel)
		pad[ignore_edge_pixels:img.shape[0]+ignore_edge_pixels, ignore_edge_pixels:img.shape[1]+ignore_edge_pixels,:] = img
		pad = np.array(pad).astype(np.uint8)
	except:
		x = window_size + bool(img.shape[0] // window_size) * (
				((img.shape[0] - window_size) // stride) + 1 * bool((img.shape[0] - window_size) % stride)) * stride
		y = window_size + bool(img.shape[1] // window_size) * (
				((img.shape[1] - window_size) // stride) + 1 * bool((img.shape[1] - window_size) % stride)) * stride
		pad = np.ones(x*y)
		pad = pad.reshape(x,y)
		pad[:img.shape[0], :img.shape[1]] = img
		pad = np.array(pad).astype(np.uint8)
	return pad


def unpadding(img, resize_shape,ignore_edge_pixels):
	return img[:,:,ignore_edge_pixels:resize_shape[1]+ignore_edge_pixels,ignore_edge_pixels:resize_shape[0]+ignore_edge_pixels]


def show_img(_id, image):
	plt.figure(_id, figsize=(10,10))
	plt.imshow(image)


def predict_img(net,
				full_img,
				scale_factor=1,
				use_dense_crf=False,
				use_gpu=True):

	img_height = full_img.size[1]
	img_width = full_img.size[0]

	img = resize_and_crop(full_img, scale=scale_factor)
	img = normalize(img)
	img = hwc_to_chw(img)
	X_out = torch.from_numpy(img).unsqueeze(0)

	if use_gpu:
		X_out = X_out.cuda()

	with torch.no_grad():
		output = net(X_out)
		num_channels =  output.shape[1]
		final_mask_np = np.empty([num_channels,img_width,img_height])
		for i in range(num_channels):
			probs = F.sigmoid(output)[:,i, :, :]
			tf = transforms.Compose(
				[
					transforms.ToPILImage(),
					transforms.Resize((img_width,img_height)),
					transforms.ToTensor()
				]
			)
			probs = tf(probs.cpu())
			mask_np = probs.squeeze().cpu().numpy()
			final_mask_np[i,:,:] = mask_np

	if use_dense_crf:
		mask_np = dense_crf(np.array(full_img).astype(np.uint8), final_mask_np)

	return final_mask_np


def mask_to_image(mask):

	return Image.fromarray((mask * 255).astype(np.uint8))

def test_segmentation(list_input_imgs,model_path,output_dir,net = "Dinknet34",num_classes = 1,window_size=512,stride = 256,ignore_edge_pixels = 50,mask_threshold = [.5,.7],scale = 1,YCbCr = False,gpu_num = 0,no_crf = True,use_only_cpu = False,save_rgb_image = True):

	if "34" in net:
		net = DinkNet34(num_classes=num_classes)
	elif "50" in net:
		net = DinkNet50(num_classes=num_classes)
	elif "101" in net:
		net = DinkNet101(num_classes=num_classes)

	model_id = model_path.split(os.sep)[-2] + model_path.split(os.sep)[-1]

	print ("Model ID", model_id)
	if not use_only_cpu:
		# print("Using CUDA version of the net, prepare your GPU !")
		net.cuda(gpu_num%torch.cuda.device_count())
		net.load_state_dict(torch.load(model_path),strict=False)
	else:
		# print("Using CPU version of the net, this may be very slow")
		net.cpu()
		net.load_state_dict(torch.load(model_path, map_location='cpu'))

	print("Model loaded !")
	for im_path in list_input_imgs:
		start = time.time()
		ref_tiff_file = im_path
		im_id = im_path.split(os.sep)[-1].split('.')[0]
		print(im_path)
		main_dir = os.path.join(output_dir,im_id)
		os.makedirs(main_dir,exist_ok=True)
		out_path_tif = main_dir + '/{}_{}_map.tif'.format(im_id, model_id)
		if True:
		# if not os.path.exists(out_path_png):
			im = io.imread(im_path)
			h, w = im.shape[0:2]
			w /= scale
			h /= scale
			resize_shape = (int(w), int(h))
			# resize_shape = (int(h), int(w))

			if scale != 1:
				im = io_resize(im, (int(h), int(w)),preserve_range = True)


			if save_rgb_image:
				# print (im.shape)
				# print (np.rollaxis(im,2,0).shape)
				out_path_resized_image = main_dir + "/resized.tif"
				new_im = np.rollaxis(np.rollaxis(im,2,0),1,2)
				georeferencing_from_array(new_im, im_path, out_path_resized_image,scale)
				# io.imsave(out_path_resized_image, im)

			im = padding(im,window_size,3)

			# Divide the image into window segments
			all_images = {}
			patch_count = 0

			for ix in range(0, im.shape[0] - window_size + stride, stride):
				for iy in range(0, im.shape[1] - window_size + stride, stride):
					try:
						part = im[ix:ix+window_size, iy:iy+window_size]
						input_patch = part
						patch_count = patch_count + 1
						all_images[(ix, iy)] = input_patch
					except:
						pass

			# Create empty prediction matrix

			prob_map = np.zeros((num_classes,)+im.shape[:2])
			counts = np.zeros(im.shape[:2])

			# Create batches
			batch_size = 1
			coords = list(all_images.keys())

			count = 0
			for ix in range(0, len(coords), batch_size):
				try:
					count = count+ 1
					batch_coords = coords[ix:ix+batch_size]
					im_batch = [all_images[px] for px in batch_coords]
					img = Image.fromarray(np.uint8(im_batch[0]))
					if YCbCr:
						img=img.convert('YCbCr')
					heat_map = predict_img(net=net,
									   full_img=img,
									   use_dense_crf= not no_crf,
									   use_gpu=not use_only_cpu)
					heat_map = np.array(heat_map)
					x, y, w, h = batch_coords[0][0], batch_coords[0][1], window_size, window_size
					prob_map[:,x+ignore_edge_pixels:x+w-ignore_edge_pixels, y+ignore_edge_pixels:y+w-ignore_edge_pixels] += heat_map[:,ignore_edge_pixels:window_size-ignore_edge_pixels,ignore_edge_pixels:window_size-ignore_edge_pixels]
					counts[x+ignore_edge_pixels:x+w-ignore_edge_pixels, y+ignore_edge_pixels:y+w-ignore_edge_pixels] += 1.0

				except Exception:
					traceback.print_exc()

			counts[counts==0]=1
			prob = np.divide(prob_map, counts)
			final_out = np.empty_like(prob)

			for j in range(num_classes):
				class_mask_threshold = mask_threshold[j]
				if class_mask_threshold:
					final_out[j,:,:] = np.array((prob[j,:,:]>class_mask_threshold)*255, dtype='uint8')
				else:
					final_out[j,:,:] = np.array(prob[j,:,:] * 255, dtype='uint8')

			unpadded_array = unpadding(final_out,resize_shape)
			try:
				georeferencing_from_array(unpadded_array, ref_tiff_file, out_path_tif,scale)
			except:
				traceback.print_exc()

			end = time.time()
			print("Time taken to process the image: {}".format(end - start))

# test_segmentation(["/media/orion/7205d165-9a5c-4884-bb2e-8582055ab06e/Sarthak/ALib/ml_testing/segmentation/test_in/13_aoi_combined.tif"],
# 				  "/media/orion/7205d165-9a5c-4884-bb2e-8582055ab06e/Sarthak/Segmentation_Pipeline/checkpoints/Experiment_30_YCbCr/CP_latest_50.pth",
# 				  "/media/orion/7205d165-9a5c-4884-bb2e-8582055ab06e/Sarthak/ALib/ml_testing/segmentation/test_out_2",YCbCr=True,scale = 2
# 				  )
