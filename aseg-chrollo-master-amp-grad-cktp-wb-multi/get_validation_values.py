import argparse
from distutils.version import LooseVersion
import torch
from config import cfg
from utils import setup_logger
from eval import main as validate_main
from trains import Task
from torch.utils.tensorboard import SummaryWriter
#epoch_range = [1, 28] # range of epochs for whcih we need to get validation values
validation_interval = 1 # get validation values after every 'validation_interval' value
# ranges fro 1 2 5 200 and 4class-normal 4class-aug-normal 

if __name__ == '__main__':

    assert LooseVersion(torch.__version__) >= LooseVersion('0.4.0'), \
            'PyTorch>=0.4.0 is required'

    parser = argparse.ArgumentParser(
        description="PyTorch Semantic Segmentation Validation"
    )
    parser.add_argument(
        "--cfg",
        default="config/ade20k-resnet50dilated-ppm_deepsup.yaml",
        metavar="FILE",
        help="path to config file",
        type=str,
    )
    parser.add_argument(
        "--gpu",
        default=0,
        help="gpu to use"
    )

    parser.add_argument(
        "--epoch_range",
        default="1,100",
        help="Epoch range for validation"
    )

    parser.add_argument(
        "opts",
        help="Modify config options using the command-line",
        default=None,
        nargs=argparse.REMAINDER,
    )
    args = parser.parse_args()
    task = Task.init('Validation', 'VAL_{}'.format(args.cfg.split('/')[-1]))
    epoch_range = args.epoch_range.split(',')
    cfg.merge_from_file(args.cfg)
    cfg.merge_from_list(args.opts)
    task.connect(cfg)
    writer = SummaryWriter()
    # cfg.freeze()

    logger = setup_logger(distributed_rank=0)   # TODO
    logger.info("Loaded configuration file {}".format(args.cfg))
    logger.info("Running with config:\n{}".format(cfg))
    
    # epoch_id = cfg.VAL.checkpoint.split(".")[0].split("_")[-1]

    # if not os.path.isdir(os.path.join(cfg.DIR, "result_epoch_" + epoch_id)):
    #     print("not directory")
    #     os.makedirs(os.path.join(cfg.DIR, "result_epoch_" + epoch_id)) 
#    epoch_range = args.epoch_range.split(',')

    start_epoch = int(epoch_range[0])
    end_epoch = int(epoch_range[1])
#    print(start_epoch,end_epoch)
    while start_epoch <= end_epoch:
        print("Running validation on epoch {}".format(start_epoch))
        validate_main(cfg, args.gpu, str(start_epoch),writer)
        start_epoch += validation_interval


