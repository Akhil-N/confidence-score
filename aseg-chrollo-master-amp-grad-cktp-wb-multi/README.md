# aseg-pipeline

Attentive Semantic Segmentation pipeline.

To use gradient checkpoint functionality: 
	make [arch_encoder: "hrnet_gckpt"] in the config file.

To use augmentations.py install: 
    pip install albumentations
    
    
Using gradient checkpoint with automatic mixed precision:

    - Edit torch.utils.checkpoint.py available in your Virtual Environment.
    - Perform forward pass inside backward function of "CheckpointFunction" class inside autocast context. For clarity see below:
    
    "autocast = torch.cuda.amp.autocast
     with torch.enable_grad():
        with autocast():
            outputs = ctx.run_function(*detached_inputs)"

## For AMP setup install requirement.txt libraries in virtual enviorment.

