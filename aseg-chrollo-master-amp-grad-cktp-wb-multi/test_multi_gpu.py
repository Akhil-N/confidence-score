# System libs
import os
import argparse
from distutils.version import LooseVersion
# Numerical libs
import numpy as np
import torch
import torch.nn as nn
from scipy.io import loadmat
import csv
# Our libs
from dataset import TestDataset
from models import ModelBuilder, SegmentationModule
from lib.nn import user_scattered_collate, async_copy_to
from lib.utils import as_numpy
from PIL import Image
from config import cfg
from skimage import io
from test_seg import padding,unpadding
import traceback
from osgeo import gdal
import time
from multiprocessing import Pool
import multiprocessing
from utils import AverageMeter, colorEncode, accuracy, intersectionAndUnion, setup_logger


logger = setup_logger(distributed_rank=0)

colors = loadmat('data/color150.mat')['colors']
names = {}
with open('data/object150_info.csv') as f:
    reader = csv.reader(f)
    next(reader)
    for row in reader:
        names[int(row[0])] = row[5].split(";")[0]

def rotate_array_270(np_array):
    return np.rot90(np_array, 3, axes=(2,3)).copy()
def rotate_array_180(np_array):
    return np.rot90(np_array, 2, axes=(2,3)).copy()
def rotate_array_90(np_array):
    return np.rot90(np_array, 1, axes=(2,3)).copy()
def flip_array_vertical(np_array):
    return np.flip(np_array, axis=0).copy()
def flip_array_horizontal(np_array):
    return np.flip(np_array, axis=1).copy()

def augument_image(img,mode = 0):
    img_aug_list = [torch.from_numpy(img)]
    if mode ==0:
        return img_aug_list
    img_vertical = torch.from_numpy(flip_array_vertical(img))
    img_90 = torch.from_numpy(rotate_array_90(img))
    img_aug_list.extend([img_vertical, img_90])
    if mode ==1:
        return img_aug_list
    img_horizontal = torch.from_numpy(flip_array_horizontal(img))
    img_180 = torch.from_numpy(rotate_array_180(img))
    img_270 = torch.from_numpy(rotate_array_270(img))
    img_aug_list.extend([img_horizontal, img_180, img_270])
    return img_aug_list

def reshape_predictions(prediction, index):
    prediction = prediction.detach().cpu()
    pred = np.array(prediction)
    if index == 1:
        pred = flip_array_vertical(pred)
    elif index == 2:
        pred = rotate_array_270(pred)
    elif index == 3:
        pred = flip_array_horizontal(pred)
    elif index == 4:
        pred = rotate_array_180(pred)
    elif index == 5:
        pred = rotate_array_90(pred)
    return torch.from_numpy(pred)

def visualize_result(data, pred,out_path,bounds,use_gt = False,gt_path = None):
    img = data

    # colorize prediction
    # if use_gt:
    Image.fromarray(pred+1).save(out_path + "_pred.png")
    pred_color = colorEncode(pred, colors).astype(np.uint8)
    # Image.fromarray(pred_color).save(out_path+"_single.png")
    # aggregate images and save
    if gt_path:
        pths = out_path.split('/')
        os.makedirs('/'.join(pths[:-1]+["combined"]),exist_ok=True)
        combinedOutPth = '/'.join(pths[:-1]+["combined"]+[pths[-1]])
        seg = np.array(Image.open(gt_path))-1
        seg_color = colorEncode(seg, colors)
        im_vis = np.concatenate((img, seg_color,pred_color), axis=1)
        Image.fromarray(im_vis).save(combinedOutPth + "_combined.jpg")
    else:
        im_vis = np.concatenate((img, pred_color), axis=1)
        # print ("info",info)
        pths = out_path.split('/')
        os.makedirs('/'.join(pths[:-1]+["combined"]),exist_ok=True)
        combinedOutPth = '/'.join(pths[:-1]+["combined"]+[pths[-1]])
        Image.fromarray(im_vis).save(combinedOutPth+ "_combined.jpg")
    if bounds:
        # gdal.Translate(out_path+"_single.tif",out_path+"_single.png",outputBounds = [bounds["left"],bounds["top"],bounds["right"],bounds["bottom"]], outputSRS = "EPSG:4326")
        gdal.Translate(out_path + "_pred.tif", out_path + "_pred.png",
                       outputBounds=[bounds["left"], bounds["top"], bounds["right"], bounds["bottom"]],
                       outputSRS="EPSG:4326")


def test(segmentation_module, loader, gpu,image_data,aug_mode,window_size,num_classes,ignore_edge_pixels,use_gt = False,gt_path = None):
    segmentation_module.eval()
    prob_map = np.zeros((1,num_classes) + image_data.shape[:2])
    # counts = np.zeros(image_data.shape[:2])
    for batch_data in loader:
        # process data
        path_for_later = batch_data[0]["info"]
        batch_data = batch_data[0]
        segSize = (batch_data['img_ori'].shape[0],
                   batch_data['img_ori'].shape[1])
        img_resized_list = batch_data['img_data']

        with torch.no_grad():
            scores = torch.zeros(1, cfg.DATASET.num_class, segSize[0], segSize[1])
            scores = async_copy_to(scores, gpu)

            for img in img_resized_list:

                img = np.array(img)
                img_aug_list = augument_image(img,aug_mode)
                # print(img_aug_list)
                for index in range(len(img_aug_list)):
                    # print("index {}".format(index))
                    feed_dict = batch_data.copy()
                    feed_dict['img_data'] = img_aug_list[index]
                    # print("input img tensor shape {}".format(img_aug_list[index].shape))
                    del feed_dict['img_ori']
                    del feed_dict['info']
                    del feed_dict['out_path']
                    del feed_dict['full_bounds']
                    del feed_dict['position']
                    feed_dict = async_copy_to(feed_dict, gpu)
                    # forward pass
                    pred_tmp = segmentation_module(feed_dict, segSize=segSize)
                    if(isinstance(pred_tmp,list)): pred_tmp = pred_tmp[1]
                    # print ("type",type(pred_tmp))
                    pred_tmp = reshape_predictions(pred_tmp, index)
                    # print (type(pred_tmp))
                    # print("pred_tmp shape {}".format(pred_tmp.shape))
                    # print ("pred_tmp.max()",pred_tmp.max())
                    scores = scores.cpu()
                    # print (scores)
                    scores = scores + pred_tmp
        # print("scores.numpy()",scores.numpy())
        scores_numpy = scores.numpy()
        position = batch_data["position"]
        # print ("ddddd")
        prob_map[0,:,position[0]+ignore_edge_pixels:position[0]+window_size-ignore_edge_pixels,position[1]+ignore_edge_pixels:position[1]+window_size-ignore_edge_pixels] = scores_numpy[0,:,ignore_edge_pixels:window_size-ignore_edge_pixels,ignore_edge_pixels:window_size-ignore_edge_pixels]
        # print ("ccccccccc")
    # print ("bbbbb")
    orig_img = batch_data["info"]
    # print ("orig_img.shape[1],orig_img.shape[0]",orig_img.shape[1],orig_img.shape[0])
    unpadded_array = unpadding(prob_map, (orig_img.shape[1],orig_img.shape[0]),ignore_edge_pixels)
    final_scores = torch.tensor(unpadded_array)
    _, pred = torch.max(final_scores, dim=1)
    pred = as_numpy(pred.squeeze(0).cpu())
    pred = np.int32(pred)
    visualize_result(
        orig_img,
        pred,batch_data["out_path"],batch_data["full_bounds"], use_gt, gt_path
    )
    return pred

def main(cfg, gpu,num_classes,aug_mode,window_size=1400,stride = 500,ignore_edge_pixels = 0,use_gt = False,gt_path = None):
    torch.cuda.set_device(gpu)
    time_main = time.time()
    # Network Builders
    net_encoder = ModelBuilder.build_encoder(cfg,
        arch=cfg.MODEL.arch_encoder,
        fc_dim=cfg.MODEL.fc_dim,
        weights=cfg.MODEL.weights_encoder)
    net_decoder = ModelBuilder.build_decoder(
        arch=cfg.MODEL.arch_decoder,
        fc_dim=cfg.MODEL.fc_dim,
        num_class=cfg.DATASET.num_class,
        weights=cfg.MODEL.weights_decoder,
        use_softmax=True)
    crit = nn.NLLLoss(ignore_index=-1)
    segmentation_module = SegmentationModule(net_encoder, net_decoder, crit)
    # Dataset and Loader
    for entry in cfg.list_test:

        im_orig = entry["fpath_img"]
        im = padding(im_orig, window_size,stride, 3,ignore_edge_pixels)
        all_images = {}
        patch_count = 0

        for ix in range(0, im.shape[0] - window_size + stride, stride):
            for iy in range(0, im.shape[1] - window_size + stride, stride):
                try:
                    part = im[ix:ix + window_size, iy:iy + window_size]
                    input_patch = part
                    patch_count = patch_count + 1
                    all_images[(ix, iy)] = input_patch
                except:
                    traceback.print_exc()

        # Create batches
        # batch_size = 1
        coords = list(all_images.keys())
        final_dict = []
        count = 0
        for ix in range(0, len(coords), 1):
            try:
                count = count + 1
                batch_coords = coords[ix:ix + 1]
                im_batch = [all_images[px] for px in batch_coords]
                img = Image.fromarray(np.uint8(im_batch[0]))
                final_dict += [{"fpath_img": im_orig,"position":coords[ix],"img_pil_object":img,"full_bounds":entry["bounds"],"out_path":entry["out_path"]}]
            except Exception:
                traceback.print_exc()

        dataset_test = TestDataset(
            final_dict,
            cfg.DATASET,cfg)

        loader_test = torch.utils.data.DataLoader(
            dataset_test,
            batch_size=cfg.TEST.batch_size,
            shuffle=False,
            collate_fn=user_scattered_collate,
            num_workers=0,
            drop_last=True)

        segmentation_module.cuda()

        # Main loop
        pred = test(segmentation_module, loader_test, gpu,im,aug_mode,window_size,num_classes,ignore_edge_pixels,use_gt,gt_path)

    # print('Inference done!')
    return pred

def full_main(imgs,config,image_bounds,out_path,num_classes,start_gpu_id,aug_mode,window_size=1400,stride = 500,ignore_edge_pixels = 0,use_gt = False,ckpt_num = None,gpu_subtract = 0,gt_path = None):
    assert LooseVersion(torch.__version__) >= LooseVersion('0.4.0'), \
        'PyTorch>=0.4.0 is required'

    time_start = time.time()
    cfg.merge_from_file(config)
    temp_list = ['TEST.result',out_path]
    cfg.merge_from_list(temp_list)
    # cfg.freeze()

    # logger.info("Loaded configuration file {}".format(config))
    # logger.info("Running with config:\n{}".format(cfg))

    cfg.MODEL.arch_encoder = cfg.MODEL.arch_encoder.lower()
    cfg.MODEL.arch_decoder = cfg.MODEL.arch_decoder.lower()

    # absolute paths of model weights

    if ckpt_num:
        cfg.MODEL.weights_encoder = os.path.join(
            cfg.DIR, 'encoder_' + "epoch_{}.pth".format(ckpt_num))
        cfg.MODEL.weights_decoder = os.path.join(
            cfg.DIR, 'decoder_' + "epoch_{}.pth".format(ckpt_num))

    else:
        cfg.MODEL.weights_encoder = os.path.join(
            cfg.DIR, 'encoder_' + cfg.TEST.checkpoint)
        cfg.MODEL.weights_decoder = os.path.join(
            cfg.DIR, 'decoder_' + cfg.TEST.checkpoint)

    # assert os.path.exists(cfg.MODEL.weights_encoder) and \
    #     os.path.exists(cfg.MODEL.weights_decoder), "checkpoint does not exist!"

    # os.makedirs(out_path, exist_ok=True)
    cfg.list_test = [{'fpath_img': imgs,'bounds':image_bounds,"out_path":out_path}]
    gpu_id = start_gpu_id+multiprocessing.current_process()._identity[0]-1-gpu_subtract
    pred = main(cfg,gpu_id,num_classes,aug_mode,window_size,stride,ignore_edge_pixels,use_gt,gt_path)
    print ("Prediction done for {} in {} seconds".format(out_path,time.time()-time_start))
    # print ("Total time taken :",)
    # torch.cuda.empty_cache()
    # time.sleep(3)
    return pred

def multi_main(input_dir,config_file,gpus_to_use,out_dir_path,num_classes,aug_mode = 0,use_gt = False,start_ckpt = None, end_ckpt = None,window_size=1400,stride = 500,ignore_edge_pixels = 0,keep_preds = True):
    if not start_ckpt or not end_ckpt:
        start_ckpt = -1
        end_ckpt = 0
    for ckpt_num in range(start_ckpt,end_ckpt):
        os.makedirs(out_dir_path,exist_ok=True)
        if not use_gt:
            img_list = os.listdir(input_dir)
        else:
            try:
                img_list = os.listdir(os.path.join(input_dir,"images"))
            except:
                raise Exception("Folder named imgs should be present when use_gt set to True")
        multi_options_list = []
        if str(type(gpus_to_use))=="<class 'int'>":
            start_gpu_id = gpus_to_use
            total_gpus_to_use = 1
        else:
            start_gpu_id = int(gpus_to_use.split("-")[0])
            total_gpus_to_use = int(gpus_to_use.split("-")[1])-start_gpu_id+1

        if ckpt_num != -1:
            out_dir = os.path.join(out_dir_path, "epoch_{}".format(ckpt_num))
            os.makedirs(os.path.join(out_dir),
                        exist_ok=True)
        else:
            out_dir = out_dir_path

        for i in range(len(img_list)):
            id = img_list[i].split(".")[0]

            if img_list[i].split(".")[1] != "tif":
                bounds = None
            else:
                if use_gt:
                    ds = gdal.Open(os.path.join(input_dir,"images",img_list[i]))
                else:
                    ds = gdal.Open(os.path.join(input_dir, img_list[i]))
                gt = ds.GetGeoTransform()
                xsize, ysize = ds.RasterXSize, ds.RasterYSize
                bounds = {}
                bounds["left"] = gt[0]
                bounds["bottom"] = gt[3] + xsize * gt[4] + ysize * gt[5]
                bounds["right"] = gt[0] + xsize * gt[1] + ysize * gt[2]
                bounds["top"] = gt[3]
            if use_gt:
                img_data = io.imread(os.path.join(input_dir,"images", img_list[i]))
                gt_path = os.path.join(os.path.join(input_dir, "annotations"),id+"_gt."+img_list[i].split(".")[1])
            else:
                img_data = io.imread(os.path.join(input_dir, img_list[i]))
                gt_path = None
            output_path = os.path.join(out_dir, id)
            if ckpt_num == -1:
                multi_options_list += [(img_data, config_file, bounds, output_path, num_classes, start_gpu_id, aug_mode,
                                        window_size, stride, ignore_edge_pixels, use_gt, None,0)]
            else:
                multi_options_list+=[(img_data,config_file,bounds,output_path,num_classes,start_gpu_id,aug_mode,window_size,stride,ignore_edge_pixels,use_gt,ckpt_num,(ckpt_num-start_ckpt)*total_gpus_to_use,gt_path)]
        pool_list = Pool(total_gpus_to_use)
        pool_list.starmap(full_main, multi_options_list)
        pool_list.terminate()
        if use_gt:
            gt_root = os.path.join(input_dir, "annotations")
            acc_meter = AverageMeter()
            intersection_meter = AverageMeter()
            union_meter = AverageMeter()
            for i in range(len(img_list)):
                id = img_list[i].split(".")[0]
                label_path = os.path.join(gt_root,id+"_gt."+img_list[i].split(".")[1])
                pred_path = os.path.join(out_dir,id+"_pred.png")
                pred = np.array(Image.open(pred_path),dtype="uint8")
                seg_label = np.array(Image.open(label_path),dtype="uint8")
                acc, pix = accuracy(pred, seg_label)
                intersection, union = intersectionAndUnion(pred, seg_label, num_classes+1) #If class id
                acc_meter.update(acc, pix)
                intersection_meter.update(intersection)
                union_meter.update(union)
            # print (intersection_meter,union_meter)
            iou = intersection_meter.sum / (union_meter.sum + 1e-10)
            iou_values = []
            iou_sum = 0
            # print (iou)
            for i, _iou in enumerate(iou):
                if i!=0:
                    print('class [{}], IoU: {:.4f}'.format(i, _iou))
                    iou_values.append('class [{}], IoU: {:.4f}'.format(i, _iou))
                    iou_sum += _iou
            print('[Eval Summary]:')
            print('Mean IoU: {:.4f}, Accuracy: {:.2f}%'
                  .format(iou_sum/num_classes, acc_meter.average() * 100))
            with open(os.path.join(out_dir_path, 'validation_values.txt'), 'a') as val_file:
                val_file.write(
                    'Epoch: {}, Mean IoU: {:.4f}, Accuracy: {:.2f}%, IoU values: {}\n'
                    .format(ckpt_num, iou_sum/num_classes, acc_meter.average() * 100, iou_values))

        if not keep_preds:
            list_imgs_out = os.listdir(out_dir)
            for img in list_imgs_out:
                if "pred" in img:
                    os.remove(os.path.join(out_dir,img))



input_dir = "/media/catila/a5ee59fd-ca0b-4ee4-bc60-0a51e7ec42bd/akhil/lawns_buildings/testData/images"

config_file = "config/attentive-hrnetv2.yaml"
out_path = "/media/catila/a5ee59fd-ca0b-4ee4-bc60-0a51e7ec42bd/akhil/lawns_buildings/results"
num_classes = 3
aug_mode = 0
start_ckpt = 183
end_ckpt = 184 # Will not run on this ckpt
gpus = 0
window_size = 1024
stride = 660
ignore_edge_pixels = 0
keep_preds = True
multi_main(input_dir,config_file,gpus,out_path,num_classes,aug_mode,use_gt=False,start_ckpt = start_ckpt,end_ckpt = end_ckpt,window_size= window_size,stride=stride,ignore_edge_pixels=ignore_edge_pixels,keep_preds = keep_preds)

