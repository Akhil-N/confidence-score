from yacs.config import CfgNode as CN

# -----------------------------------------------------------------------------
# Config definition
# -----------------------------------------------------------------------------

_C = CN()
_C.DIR = "ckpt/ade20k-resnet50dilated-ppm_deepsup"

# -----------------------------------------------------------------------------
# Dataset
# -----------------------------------------------------------------------------
_C.DATASET = CN()
_C.DATASET.root_dataset = "./data/"
_C.DATASET.list_train = "./data/training.odgt"
_C.DATASET.list_val = "./data/validation.odgt"
_C.DATASET.list_wandb = "./data/forwandb.odgt"

_C.DATASET.num_class = 150
# multiscale train/test, size of short edge (int or tuple)
_C.DATASET.imgSizes = (300, 375, 450, 525, 600)
# maximum input image size of long edge
_C.DATASET.imgMaxSize = 1000
# maxmimum downsampling rate of the network
_C.DATASET.padding_constant = 8
# downsampling rate of the segmentation label
_C.DATASET.segm_downsampling_rate = 8
# randomly horizontally flip images when train/test
_C.DATASET.random_flip = True


# -----------------------------------------------------------------------------
# Augmentation
# -----------------------------------------------------------------------------

# Probability distribution coefficients
_C.AUG = CN()
_C.AUG.do_aug = False

_C.AUG.aug_prob_coeff = 0.1
# Depth of augmentation chains. -1 denotes stochastic depth in [1, 3]
_C.AUG.mixture_depth = -1 
# Number of augmentation chains to mix per augmented example
_C.AUG.mixture_width = 3
# Severity of base augmentation operators
_C.AUG.aug_severity = 1


# -----------------------------------------------------------------------------
# Model
# -----------------------------------------------------------------------------
_C.MODEL = CN()
# architecture of net_encoder
_C.MODEL.arch_encoder = "resnet50dilated"
# architecture of net_decoder
_C.MODEL.arch_decoder = "ppm_deepsup"
# weights to finetune net_encoder
_C.MODEL.weights_encoder = ""
# weights to finetune net_decoder
_C.MODEL.weights_decoder = ""
# number of feature channels between encoder and decoder
_C.MODEL.fc_dim = 2048
# number of channels in input layer
_C.MODEL.no_channels_input = 3
# Model use pretrained 
_C.MODEL.pretrained = False
#
# -----------------------------------------------------------------------------
# Training
# -----------------------------------------------------------------------------
_C.TRAIN = CN()
_C.TRAIN.batch_size_per_gpu = 2
# epochs to train for
_C.TRAIN.num_epoch = 20
# epoch to start training. useful if continue from a checkpoint
_C.TRAIN.start_epoch = 0
# iterations of each epoch (irrelevant to batch size)
_C.TRAIN.epoch_iters = 5000

_C.TRAIN.optim = "SGD"
_C.TRAIN.lr_encoder = 0.02
_C.TRAIN.lr_decoder = 0.02
# power in poly to drop LR
_C.TRAIN.lr_pow = 0.9
# momentum for sgd, beta1 for adam
_C.TRAIN.beta1 = 0.9
# weights regularizer
_C.TRAIN.weight_decay = 1e-4
# the weighting of deep supervision loss
_C.TRAIN.deep_sup_scale = 0.4
# fix bn params, only under finetuning
_C.TRAIN.fix_bn = False
# number of data loading workers
_C.TRAIN.workers = 16
# frequency to display
_C.TRAIN.disp_iter = 20
# manual seed
_C.TRAIN.seed = 304
# class-wise loss weights
_C.TRAIN.loss_weights = [1, 1, 1, 1, 1]
# Mean normalization values
_C.TRAIN.mean_normalize_values = [0.350, 0.348, 0.312]
# Standard Deviation normalization values
_C.TRAIN.stdDev_normalize_values = [0.229, 0.221, 0.211]
# run validation script after every 'n' epoch
_C.TRAIN.validate_frequency = 50
_C.TRAIN.wandb_frequency = 50


# -----------------------------------------------------------------------------
# Augmentation
# -----------------------------------------------------------------------------
_C.AUGUMENTATION = CN()
# augmentation variables
_C.AUGUMENTATION.flip_left_right = True
_C.AUGUMENTATION.flip_top_bottom = False
_C.AUGUMENTATION.rotate_90 = True
_C.AUGUMENTATION.rotate_270 = False

# -----------------------------------------------------------------------------
# Validation
# -----------------------------------------------------------------------------
_C.VAL = CN()
# currently only supports 1
_C.VAL.batch_size = 1
# output visualization during validation
_C.VAL.visualize = False
# the checkpoint to evaluate on
_C.VAL.checkpoint = "epoch_20.pth"

# -----------------------------------------------------------------------------
# Testing
# -----------------------------------------------------------------------------
_C.TEST = CN()
# currently only supports 1
_C.TEST.batch_size = 1
# the checkpoint to test on
_C.TEST.checkpoint = "epoch_20.pth"
# folder to output visualization results
_C.TEST.result = "./"
