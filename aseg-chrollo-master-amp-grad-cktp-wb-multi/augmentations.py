# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Base augmentations operators."""

import numpy as np
from PIL import Image
from PIL import ImageOps
import random
from albumentations.augmentations import transforms as alb
import skimage
import cv2

# ImageNet code should change this value


IMAGE_SIZE = 1400

def int_parameter(level, maxval):
  """Helper function to scale `val` between 0 and maxval .

  Args:
    level: Level of the operation that will be between [0, `PARAMETER_MAX`].
    maxval: Maximum value that the operation can have. This will be scaled to
      level/PARAMETER_MAX.

  Returns:
    An int that results from scaling `maxval` according to `level`.
  """
  return int(level * maxval / 10)


def float_parameter(level, maxval):
  """Helper function to scale `val` between 0 and maxval.

  Args:
    level: Level of the operation that will be between [0, `PARAMETER_MAX`].
    maxval: Maximum value that the operation can have. This will be scaled to
      level/PARAMETER_MAX.

  Returns:
    A float that results from scaling `maxval` according to `level`.
  """
  return float(level) * maxval / 10.


def sample_level(n):
  return np.random.uniform(low=0.1, high=n)


def autocontrast(pil_img, _):
  return ImageOps.autocontrast(pil_img, _)


def equalize(pil_img, _):
    return ImageOps.equalize(pil_img)


def posterize(pil_img, level):
  level = int_parameter(sample_level(level), 4)
  # return ImageOps.posterize(pil_img, 4 - level)
  temp = random.choice([4,5,6])
  return ImageOps.posterize(pil_img, temp)


def rotate(pil_img, level):
  degrees = int_parameter(sample_level(level), 30)
  if np.random.uniform() > 0.5:
    degrees = -degrees
  return pil_img.rotate(degrees, resample=Image.BILINEAR)


def solarize(pil_img, level):
  level = int_parameter(sample_level(level), 256)
  return ImageOps.solarize(pil_img, 256 - level)

def colorizer(pil_img, level):
  level = int_parameter(sample_level(level), 256)
  return ImageOps.solarize(pil_img, 256 - level)

def shear_x(pil_img, level):
  level = float_parameter(sample_level(level), 0.3)
  if np.random.uniform() > 0.5:
    level = -level
  return pil_img.transform((IMAGE_SIZE, IMAGE_SIZE),
                           Image.AFFINE, (1, level, 0, 0, 1, 0),
                           resample=Image.BILINEAR)


def shear_y(pil_img, level):
  level = float_parameter(sample_level(level), 0.3)
  if np.random.uniform() > 0.5:
    level = -level
  return pil_img.transform((IMAGE_SIZE, IMAGE_SIZE),
                           Image.AFFINE, (1, 0, 0, level, 1, 0),
                           resample=Image.BILINEAR)


def translate_x(pil_img, level):
  level = int_parameter(sample_level(level), IMAGE_SIZE / 3)
  if np.random.random() > 0.5:
    level = -level
  return pil_img.transform((IMAGE_SIZE, IMAGE_SIZE),
                           Image.AFFINE, (1, 0, level, 0, 1, 0),
                           resample=Image.BILINEAR)


def translate_y(pil_img, level):
  level = int_parameter(sample_level(level), IMAGE_SIZE / 3)
  if np.random.random() > 0.5:
    level = -level
  return pil_img.transform((IMAGE_SIZE, IMAGE_SIZE),
                           Image.AFFINE, (1, 0, 0, 0, 1, level),
                           resample=Image.BILINEAR)


def invert(pil_img, _):
  return ImageOps.invert(pil_img)

def adjust_gamma(pil_img, _):
    image = np.array(pil_img)
    invGamma = 1.0 / 1.3
    table = np.array([((i / 255.0) ** invGamma) * 255 for i in np.arange(0, 256)]).astype("uint8")
    return Image.fromarray(cv2.LUT(image, table))


def noise(pil_img, _):
    img = np.array(pil_img) / 255.0
    modes = ["gaussian", "speckle", "poisson"]
    mode = np.random.choice(modes)

    if mode == "gaussian":
        gimg = ((skimage.util.random_noise(img, mode=mode, var=0.001)) * 255).astype("uint8")
    elif mode=="speckle":
        gimg = ((skimage.util.random_noise(img, mode=mode, var=0.01)) * 255).astype("uint8")
    else:
        gimg = ((skimage.util.random_noise(img, mode=mode)) * 255).astype("uint8")

    return Image.fromarray(gimg)


def blur(pil_img, blur_type):
    image = np.array(pil_img)
    if blur_type=="gaussian":
        out = cv2.GaussianBlur(image, (5, 5), cv2.BORDER_DEFAULT)
    elif blur_type=="bilateral":
        out = cv2.bilateralFilter(image, 5, 75, 75, cv2.BORDER_DEFAULT)
    else:
        raise NameError("blur_type is invalid")

    return Image.fromarray(out)


def hsv(pil_img, _):
    img = np.array(pil_img)
    aug = alb.HueSaturationValue(always_apply=True, p=1)
    out = aug.apply(img, 5, 5, 10)
    return Image.fromarray(out)


augmentations = [autocontrast, equalize, posterize, hsv, noise, adjust_gamma]
#shear_x, shear_y,
 #   translate_x, translate_y
#]

if __name__ == "__main__":
    path = "/media/taurus/HDD/SR/Segmentation_Improvement/AugMix/Images/845.png"
    out_pth = ""
    img_pil = Image.open(path)
    # img_np = np.float32(np.array(img_pil))
    # img_np = np.uint8((np.array(img_pil)))
    # print(img_np.shape)
    # temp_result = posterize(img_pil, 0.1)
    temp_result = posterize(img_pil, 0)
    print(type(temp_result))
    Image._show(temp_result)
    # img = Image.open(temp_result)
    # img.show()
    
