from copy import copy
from collections import Counter
import os
import fiona
from shapely import geometry
from rtree import index
from osgeo import gdal, osr, ogr
import traceback
from matplotlib import pyplot as plt
import numpy as np
import math

def remove_features(shp_path, retaining_DN_value = 4):    
    ds = ogr.Open(shp_path, update=True)  # True allows to edit the shapefile
    lyr = ds.GetLayer()
    i = 0
    for _ in lyr:
        if _['DN'] != retaining_DN_value:
            lyr.DeleteFeature(i)
        i += 1
    ds.Destroy()   

def add_perimeter_field(shp_path):
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dataSource = driver.Open(shp_path, 1)
    layer = dataSource.GetLayer()
    new_field = ogr.FieldDefn("Perimeter", ogr.OFTReal)
    new_field.SetWidth(32)
    new_field.SetPrecision(8)
    layer.CreateField(new_field)
    for feature in layer:        
        perimeter = feature.GetGeometryRef().Boundary().Length()        
        # print('perimteter ', perimeter)
        feature.SetField("Perimeter", perimeter)
        layer.SetFeature(feature)
    dataSource = None 

def add_ratio_field(shp_path):
    source = ogr.Open(shp_path, update=True)
    layer = source.GetLayer()    
    # Add a new field
    new_field = ogr.FieldDefn('ratio', ogr.OFTReal)
    layer.CreateField(new_field)    
    for i in layer:        
        ratio = i['polis']/i['perimeter']                            
        i.SetField( "ratio", ratio)
        layer.SetFeature(i)             
    source = None

def raster_to_shape(main_path):    
    input_files = os.listdir(main_path)   
    print("vectorizing with gdal")    
    for input_file in input_files:
        # if 'single_updated.tif' in input_file:  
        # if 'single.tif' in input_file:
        if 'pred.tif' in input_file:                        
            ext = '.tif'
            img_name = input_file
            img_path = os.path.join(main_path, input_file)
            out_path = main_path
            polygonize_raster(img_name, img_path, out_path, ext)
            print('completed ', input_file)


def polygonize_raster(img_name, img_path, out_path, ext):
    try:
        sourceRaster = gdal.Open(img_path)
        band = sourceRaster.GetRasterBand(1)                
        driver = ogr.GetDriverByName("ESRI Shapefile")
        outShp = os.path.join(out_path, img_name.replace(ext, '.shp'))
        # If shapefile already exist, delete it
        if os.path.exists(outShp):
            driver.DeleteDataSource(outShp)
        outDatasource = driver.CreateDataSource(outShp)
        # get proj from raster
        srs = osr.SpatialReference()
        srs.ImportFromWkt(sourceRaster.GetProjectionRef())
        # create layer with proj
        outLayer = outDatasource.CreateLayer(outShp, srs)
        # Add class column (0,255) to shapefile
        newField = ogr.FieldDefn('DN', ogr.OFTInteger)
        outLayer.CreateField(newField)
        gdal.Polygonize(band, None, outLayer, 0, [], callback=None)
        outDatasource.Destroy()
        sourceRaster = None        
    except Exception as e:
        print('gdal Polygonize Error: ' + str(e))

def simplify_geom(shp_path, tolerance_value = 0.000004): 
    shp_file = ogr.Open(shp_path, update=1)    
    lyr = shp_file.GetLayerByIndex(0)    
    i = 0                        
    while i < len(lyr):        
        j = lyr[i].Clone()
        # print(i, len(lyr), j.GetFID())
        geom = j.GetGeometryRef()        
        j.SetGeometry(geom.SimplifyPreserveTopology(tolerance_value))                
        lyr.SetFeature(j)
        i += 1
    shp_file.Destroy()
    # print('completed ', shp_path)


def generate_txt_file(shp_path, txt_path, global_ratio_list):
    ratio_list = []
    with open(txt_path, 'a') as txt_file:
        # txt_file.write('Perimeter_values')
        ds = ogr.Open(shp_path, update=False)  # True allows to edit the shapefile
        lyr = ds.GetLayer()
        i = 0
        for _ in lyr:
            ratio = _['ratio'] 
            txt_file.write('{}\n'.format(ratio)) 
            ratio_list.append(ratio)
            global_ratio_list.append(ratio)
            i+=1
        print('mean: {}'.format(np.mean(ratio_list)))    
        txt_file.write('mean: {}\n'.format(np.mean(ratio_list)))    
        print('median: {}'.format(np.median(ratio_list)))              
        txt_file.write('median: {}\n'.format(np.median(ratio_list)))              
    ds.Destroy()
    # print('ratio_list', ratio_list)
    # print('global length ', len(global_ratio_list))
    return ratio_list

def generate_global_txt(global_ratio_list, txt_path):
    with open(txt_path, 'a') as txt_file:
        print('global mean: {}'.format(np.mean(global_ratio_list)))    
        txt_file.write('mean: {}\n'.format(np.mean(global_ratio_list)))   
        print('global median: {}'.format(np.median(global_ratio_list)))               
        txt_file.write('median: {}\n'.format(np.median(global_ratio_list)))              


def save_histogram(data_list, save_path):    
    bins = math.ceil((max(data_list) - min(data_list))/.001)
    plt.hist(data_list, bins=bins)
    # plt.hist(data_list, bins='auto', density=True)
    plt.xlabel("Polis/Perimeter")
    plt.ylabel("Frequency") 
    plt.xlim(xmin=0.0001, xmax = 0.02)  
       
    plt.savefig(save_path)


def compare_polys(poly_a, poly_b):
    """Compares two polygons via the "polis" distance metric.

    See "A Metric for Polygon Comparison and Building Extraction
    Evaluation" by J. Avbelj, et al.

    Input:
        poly_a: A Shapely polygon.
        poly_b: Another Shapely polygon.

    Returns:
        The "polis" distance between these two polygons.
    """
    try:
        bndry_a = poly_a.exterior
    except:
        bndry_a = poly_a[0].exterior
    try:
        bndry_b = poly_b.exterior
    except:
        bndry_b = poly_b[0].exterior
    dist = polis(bndry_a.coords, bndry_b)
    dist += polis(bndry_b.coords, bndry_a)
    return dist
    


def polis(coords, bndry):
    """Computes one side of the "polis" metric.

    Input:
        coords: A Shapley coordinate sequence (presumably the vertices
                of a polygon).
        bndry: A Shapely linestring (presumably the boundary of
        another polygon).
    
    Returns:
        The "polis" metric for this pair.  You usually compute this in
        both directions to preserve symmetry.
    """
    sum = 0.0
    for pt in (geometry.Point(c) for c in coords[:-1]): # Skip the last point (same as first)
        sum += bndry.distance(pt)
    return sum/float(2*len(coords))


def shp_to_list(shpfile):
    """Dumps all the geometries from the shapefile into a list.

    This makes it quick to build the spatial index!
    """
    with fiona.open(shpfile) as src:
        return [geometry.shape(rec['geometry']) for rec in src]


def score(in_ref, in_cmp, out):
    """Given two polygon vector files, calculate the polis score
    between them. The third argument specifies the output file, which
    contains the same geometries as in_cmp, but with the polis score
    assigned to the geometry.

    We consider the first vector file to be the reference.    
    """
    # Read in all the geometries in the reference shapefile.
    ref_polys = shp_to_list(in_ref)

    # Build a spatial index of the reference shapes.
    idx = index.Index((i, geom.bounds, None) for i, geom in enumerate(ref_polys))
    
    # Input data to measure.
    hits = []
    with fiona.open(in_cmp) as src:
        meta = copy(src.meta)
        meta['schema']['properties'] = {'polis': 'float:15.8', 'fp': 'int'}
        with fiona.open(out, 'w', **src.meta) as sink:
            for rec in src:
                cmp_poly = geometry.shape(rec['geometry'])
                ref_pindices = [i for i in idx.nearest(cmp_poly.bounds)]
                fp = 0  # whether or not the current prediction is false positive
                # Limit how many we check, if given an excessive
                # number of ties (aka if someone put in a huge
                # bounding box that covered a ton of geometries.)
                if len(ref_pindices) > 5:                     
                    ref_pindices = ref_pindices[:5]
                
                scores = [compare_polys(cmp_poly, ref_polys[i]) for i in ref_pindices]
                polis_score = min(scores)
                # print(polis_score)
                hits.append(ref_pindices[scores.index(polis_score)]) 
                # print('appending ', ref_pindices[scores.index(polis_score)])               
                if not cmp_poly.intersects(ref_polys[ref_pindices[scores.index(polis_score)]]):
                    fp = 1
                # print('ref poly bounds', ref_polys[ref_pindices[scores.index(polis_score)]].bounds)
                # print('cmp poly bounds', cmp_poly.bounds)
                # print(cmp_poly.intersects(ref_polys[ref_pindices[scores.index(polis_score)]]))
                sink.write({'geometry':rec['geometry'],
                            'properties':{'polis': polis_score, 'fp': fp}})
    # print('completed ', out)
                

    # Summarize results.
    # print("Number of matches: {}".format(len(hits)))
    # print("Number of misses: {}".format(len(ref_polys) - len(hits)))
    # print("Duplicate matches: {}".format(sum([1 for i in Counter(hits).values() if i > 1])))

if __name__ == "__main__":
    # score()

    # pred_shp_dir = '/media/ursamajor/1df17740-bab5-4762-9749-b349b34bd2f3/FM_global/phase3/polis_metric/results_final/2048_amp/pred_shps'
    # pred_shps = os.listdir(pred_shp_dir)
    # for pred_shp in pred_shps:
    #     if pred_shp.endswith('.shp'):
    #         pred_shp_path = os.path.join(pred_shp_dir, pred_shp)
    #         # ref_shp_path = pred_shp_path.replace('single_updated', 'final')
    #         ref_shp_path = pred_shp_path.replace('single', 'final')
    #         ref_shp_path = ref_shp_path.replace('pred_shps', 'gt_shps')
    #         out_path = ref_shp_path.replace('gt_shps', 'polis_reverse')
    #         out_path = out_path.replace('final.shp', 'polis_reverse.shp')

    #         ref_shp_path, pred_shp_path = pred_shp_path, ref_shp_path

    #         # print(pred_shp_path)
    #         # print(ref_shp_path)
    #         # print(out_path)
    #         try:
    #             print('starting ', pred_shp)
    #             score(ref_shp_path, pred_shp_path, out_path)
    #         except Exception as e:
    #             print('error ', e)
    
    
    ref_shp_path = '/media/ursamajor/1df17740-bab5-4762-9749-b349b34bd2f3/FM_global/phase3/polis_metric/temp/new/perfect_pred.shp'
    predicted_shp_path = '/media/ursamajor/1df17740-bab5-4762-9749-b349b34bd2f3/FM_global/phase3/polis_metric/temp/new/gt.shp'
    out_path = '/media/ursamajor/1df17740-bab5-4762-9749-b349b34bd2f3/FM_global/phase3/polis_metric/temp/new/polis/polis_perfect_pred.shp'

    score(ref_shp_path, predicted_shp_path, out_path)
