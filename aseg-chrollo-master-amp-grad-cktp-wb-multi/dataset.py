import os
import json
import torch
from torchvision import transforms
import numpy as np
from PIL import Image
#import gdal
import augmentations
import math
import random


def imresize(im, size, interp='bilinear'):
    if interp == 'nearest':
        resample = Image.NEAREST
    elif interp == 'bilinear':
        resample = Image.BILINEAR
    elif interp == 'bicubic':
        resample = Image.BICUBIC
    else:
        raise Exception('resample method undefined!')
    return im.resize(size, resample)

def user_collate_fn(batch):
    assert(len(batch) == 1)
    return batch[0]


class BaseDataset(torch.utils.data.Dataset):
    def __init__(self, odgt, opt, cfg, **kwargs):
        # parse options
        self.imgSizes = opt.imgSizes
        self.imgMaxSize = opt.imgMaxSize
        # max down sampling rate of network to avoid rounding during conv or pooling
        self.padding_constant = opt.padding_constant

        # parse the input list
        self.parse_input_list(odgt, **kwargs)

        # mean and std
        # original values
        # self.normalize = transforms.Normalize(
        #     mean=[0.485, 0.456, 0.406, 0.4],
        #     std=[0.229, 0.224, 0.225, 0.2])
        # Attentive values
        # self.normalize = transforms.Normalize(
        #     mean=[0.350, 0.348, 0.312],
        #     std=[0.229, 0.221, 0.211])
        # rgb2lab values
        # self.normalize = transforms.Normalize(
        #     mean=[0.463, 0.150, 0.214],
        #     std=[0.188, 0.023, 0.042])
        self.cfg = cfg
        mean_normalize_values = cfg.TRAIN.mean_normalize_values
        stdDev_normalize_values = cfg.TRAIN.stdDev_normalize_values        
        self.normalize = transforms.Normalize(mean = mean_normalize_values, std = stdDev_normalize_values)

    def parse_input_list(self, odgt, world_size=1, rank=0, start_idx=-1, end_idx=-1):
        if isinstance(odgt, list):
            self.list_sample = odgt
        elif isinstance(odgt, str):
            self.list_sample = [json.loads(x.rstrip())
                                for x in open(odgt, 'r')]

        num_total = len(self.list_sample)
        if world_size > 1:
            self.num_sample = int(math.ceil(num_total * 1.0 / world_size))
            self.start_idx = rank * self.num_sample
            self.end_idx = min(self.start_idx + self.num_sample, num_total)
        else:
            self.num_sample = int(math.ceil(num_total * 1.0 / world_size))
            self.start_idx = 0
            self.end_idx = num_total
            
        print('Dataset Samples #total: {}, #process [{}]: {}-{}'
              .format(num_total, rank, self.start_idx, self.end_idx))

    def img_transform(self, img):
        # 0-255 to 0-1

        #check for shape of mean array with image shape
        # mean_array = self.cfg.TRAIN.mean_normalize_values
        # print(img.size)

        # assert len(mean_array) == img.size[2] , "Number of Channels in Image inconsistent with mean and std array" 
        img = np.float32(np.array(img) / 255)
        img = img.transpose((2, 0, 1))  # for PIL Type image
        img = self.normalize(torch.from_numpy(img.copy()))        
        return img

    def segm_transform(self, segm):
        # to tensor, -1 to 149
        segm = torch.from_numpy(np.array(segm)).long() - 1
        return segm

    # Round x to the nearest multiple of p and x' >= x
    def round2nearest_multiple(self, x, p):
        return ((x - 1) // p + 1) * p

    

    def aug(self, image):
        """Perform AugMix augmentations and compute mixture.

        Args:
            image: PIL Image input image           

        Returns:
            mixed: Augmented and mixed image.
        """
        #Augumentation constants 
        aug_prob_coeff = self.cfg.AUG.aug_prob_coeff
        mixture_width = self.cfg.AUG.mixture_width
        mixture_depth = self.cfg.AUG.mixture_depth
        aug_severity = self.cfg.AUG.aug_severity    

        ws = np.float32(np.random.dirichlet([aug_prob_coeff] * mixture_width))
        m = np.float32(np.random.beta(aug_prob_coeff, aug_prob_coeff))  # will always be between 0 and 1

        mix = torch.zeros_like(self.img_transform(image))   # will return a tensor filled with zeros with same size as image
        # print('mix shape ', mix.shape)
        for i in range(mixture_width):
            image_aug = image.copy()
            depth = mixture_depth if mixture_depth > 0 else np.random.randint(1, 4)
            new_ag = augmentations.augmentations.copy()          
            for _ in range(depth):
                op = np.random.choice(new_ag)                
                new_ag.remove(op)
                image_aug = op(image_aug, aug_severity)
            # Preprocessing commutes since all coefficients are convex                        
            mix += ws[i] * self.img_transform(image_aug)

        mixed = (1 - m) * self.img_transform(image) + m * mix
        return mixed


class TrainDataset(BaseDataset):
    def __init__(self, root_dataset, odgt, opt, cfg, batch_per_gpu=1, **kwargs):
        super(TrainDataset, self).__init__(odgt, opt, cfg, **kwargs)
        self.root_dataset = root_dataset
        # down sampling rate of segm labe
        self.segm_downsampling_rate = opt.segm_downsampling_rate
        self.batch_per_gpu = batch_per_gpu

        # classify images into two classes: 1. h > w and 2. h <= w
        self.batch_record_list = [[], []]

        # override dataset length when trainig with batch_per_gpu > 1
        self.cur_idx = self.start_idx
        # self.if_shuffled = False
        self.cfg = cfg

    def shuffle(self, seed):
        random.Random(seed).shuffle(self.list_sample)

    def _get_sub_batch(self):
        while True:
            # get a sample record
            this_sample = self.list_sample[self.cur_idx]
            if this_sample['height'] > this_sample['width']:
                self.batch_record_list[0].append(
                    this_sample)  # h > w, go to 1st class
            else:
                self.batch_record_list[1].append(
                    this_sample)  # h <= w, go to 2nd class

            # update current sample pointer
            self.cur_idx += 1
            if self.cur_idx >= self.end_idx:
                self.cur_idx = self.start_idx
                # np.random.shuffle(self.list_sample)

            if len(self.batch_record_list[0]) == self.batch_per_gpu:
                batch_records = self.batch_record_list[0]
                self.batch_record_list[0] = []
                break
            elif len(self.batch_record_list[1]) == self.batch_per_gpu:
                batch_records = self.batch_record_list[1]
                self.batch_record_list[1] = []
                break
        return batch_records

    def __getitem__(self, index):    
        # NOTE: random shuffle for the first time. shuffle in __init__ is useless
        # if not self.if_shuffled:
        #     np.random.seed(index)
        #     np.random.shuffle(self.list_sample)
        #     self.if_shuffled = True

        # get sub-batch candidates
        batch_records = self._get_sub_batch()

        # resize all images' short edges to the chosen size
        if isinstance(self.imgSizes, list) or isinstance(self.imgSizes, tuple):
            this_short_size = np.random.choice(self.imgSizes)
        else:
            this_short_size = self.imgSizes

        # calculate the BATCH's height and width
        # since we concat more than one samples, the batch's h and w shall be larger than EACH sample
        batch_widths = np.zeros(self.batch_per_gpu, np.int32)
        batch_heights = np.zeros(self.batch_per_gpu, np.int32)
        for i in range(self.batch_per_gpu):
            img_height, img_width = batch_records[i]['height'], batch_records[i]['width']
            this_scale = min(
                this_short_size / min(img_height, img_width),
                self.imgMaxSize / max(img_height, img_width))
            batch_widths[i] = img_width * this_scale
            batch_heights[i] = img_height * this_scale
        
        no_channels_image = self.cfg.MODEL.no_channels_input


        # Here we must pad both input image and segmentation map to size h' and w' so that p | h' and p | w'
        batch_width = np.max(batch_widths)
        batch_height = np.max(batch_heights)
        batch_width = int(self.round2nearest_multiple(
            batch_width, self.padding_constant))
        batch_height = int(self.round2nearest_multiple(
            batch_height, self.padding_constant))

        assert self.padding_constant >= self.segm_downsampling_rate, \
            'padding constant must be equal or large than segm downsamping rate'
        batch_images = torch.zeros(
            self.batch_per_gpu, no_channels_image, batch_height, batch_width)
        batch_segms = torch.zeros(
            self.batch_per_gpu,
            batch_height // self.segm_downsampling_rate,
            batch_width // self.segm_downsampling_rate).long()

        for i in range(self.batch_per_gpu):
            this_record = batch_records[i]

            # load image and label
            image_path = os.path.join(
                self.root_dataset, this_record['fpath_img'])
            segm_path = os.path.join(
                self.root_dataset, this_record['fpath_segm'])

            img = Image.open(image_path).convert('RGB')
            # img = gdal.Open(image_path).ReadAsArray()
            # img = np.array(img)


            # img = Image.open(image_path).convert('CMYK')
            # img.putalpha(1)
            segm = Image.open(segm_path)
            assert(segm.mode == "L")
            assert(img.size[0] == segm.size[0])
            assert(img.size[1] == segm.size[1])

            # Augmentation
            if self.cfg.AUGUMENTATION.flip_left_right:
                if np.random.choice([0, 1]):
                    img = img.transpose(Image.FLIP_LEFT_RIGHT)
                    segm = segm.transpose(Image.FLIP_LEFT_RIGHT)
            # if self.cfg.AUGUMENTATION.flip_top_bottom:
            #     if np.random.choice([0, 1]):
            #         img = img.transpose(Image.FLIP_TOP_BOTTOM)
            #         segm = segm.transpose(Image.FLIP_TOP_BOTTOM)
            if self.cfg.AUGUMENTATION.rotate_90:
                if np.random.choice([0, 1]):
                    img = img.transpose(Image.ROTATE_90)
                    segm = segm.transpose(Image.ROTATE_90)
            # if self.cfg.AUGUMENTATION.rotate_270:
            #     if np.random.choice([0, 1]):
            #         img = img.transpose(Image.ROTATE_270)
            #         segm = segm.transpose(Image.ROTATE_270)

            # note that each sample within a mini batch has different scale param
            img = imresize(
                img, (batch_widths[i], batch_heights[i]), interp='bilinear')
            segm = imresize(
                segm, (batch_widths[i], batch_heights[i]), interp='nearest')

            # further downsample seg label, need to avoid seg label misalignment
            segm_rounded_width = self.round2nearest_multiple(
                segm.size[0], self.segm_downsampling_rate)
            segm_rounded_height = self.round2nearest_multiple(
                segm.size[1], self.segm_downsampling_rate)
            segm_rounded = Image.new(
                'L', (segm_rounded_width, segm_rounded_height), 0)
            segm_rounded.paste(segm, (0, 0))
            segm = imresize(
                segm_rounded,
                (segm_rounded.size[0] // self.segm_downsampling_rate,
                 segm_rounded.size[1] // self.segm_downsampling_rate),
                interp='nearest')

            # image transform, to torch float tensor 3xHxW
            # image_preprocessed = self.img_transform(img)          
            # img = self.aug(img, image_preprocessed)
#            img = self.aug(img)
            if self.cfg.AUG.do_aug:
#                print('using augmix')
                img = self.aug(img)
            else:
#                print('not using augmix')
                img = self.img_transform(img)

            # assert image_preprocessed.shape == img.shape, "Augumentation failed"
            # segm transform, to torch long tensor HxW
            segm = self.segm_transform(segm)
            # put into batch arrays
            batch_images[i][:, :img.shape[1],
                            :img.shape[2]] = img
            batch_segms[i][:segm.shape[0], :segm.shape[1]] = segm

        output = dict()
        output['img_data'] = batch_images
        output['seg_label'] = batch_segms
        return output

    def __len__(self):
        # It's a fake length due to the trick that every loader maintains its own list
        # return int(1e10)
        return self.num_sample
        # return self.num_sampleclass


class ValDataset(BaseDataset):
    def __init__(self, root_dataset, odgt, opt, cfg, **kwargs):
        super(ValDataset, self).__init__(odgt, opt, cfg, **kwargs)
        self.root_dataset = root_dataset

    def __getitem__(self, index):
        this_record = self.list_sample[index]
        # load image and label
        image_path = os.path.join(self.root_dataset, this_record['fpath_img'])
        segm_path = os.path.join(self.root_dataset, this_record['fpath_segm'])
        img = Image.open(image_path).convert('RGB')
        segm = Image.open(segm_path)
        assert(segm.mode == "L")
        assert(img.size[0] == segm.size[0])
        assert(img.size[1] == segm.size[1])

        ori_width, ori_height = img.size

        img_resized_list = []
        for this_short_size in self.imgSizes:
            # calculate target height and width
            scale = min(this_short_size / float(min(ori_height, ori_width)),
                        self.imgMaxSize / float(max(ori_height, ori_width)))
            target_height, target_width = int(
                ori_height * scale), int(ori_width * scale)

            # to avoid rounding in network
            target_width = self.round2nearest_multiple(
                target_width, self.padding_constant)
            target_height = self.round2nearest_multiple(
                target_height, self.padding_constant)

            # resize images
            img_resized = imresize(
                img, (target_width, target_height), interp='bilinear')

            # image transform, to torch float tensor 3xHxW
            img_resized = self.img_transform(img_resized)
            img_resized = torch.unsqueeze(img_resized, 0)
            img_resized_list.append(img_resized)

        # segm transform, to torch long tensor HxW
        segm = self.segm_transform(segm)
        batch_segms = torch.unsqueeze(segm, 0)

        output = dict()
        output['img_ori'] = np.array(img)
        output['img_data'] = [x.contiguous() for x in img_resized_list]
        output['seg_label'] = batch_segms.contiguous()
        output['info'] = this_record['fpath_img']
        return output

    def __len__(self):
        return self.num_sample

#WandbDataset class
class WandbDataset(BaseDataset):
    def __init__(self, root_dataset, odgt, opt, cfg, **kwargs):
        super(WandbDataset, self).__init__(odgt, opt, cfg, **kwargs)
        self.root_dataset = root_dataset

    def __getitem__(self, index):
        this_record = self.list_sample[index]
        # load image and label
        image_path = os.path.join(self.root_dataset, this_record['fpath_img'])
        segm_path = os.path.join(self.root_dataset, this_record['fpath_segm'])
        img = Image.open(image_path).convert('RGB')
        segm = Image.open(segm_path)
        assert(segm.mode == "L")
        assert(img.size[0] == segm.size[0])
        assert(img.size[1] == segm.size[1])

        ori_width, ori_height = img.size

        img_resized_list = []
        for this_short_size in self.imgSizes:
            # calculate target height and width
            scale = min(this_short_size / float(min(ori_height, ori_width)),
                        self.imgMaxSize / float(max(ori_height, ori_width)))
            target_height, target_width = int(
                ori_height * scale), int(ori_width * scale)

            # to avoid rounding in network
            target_width = self.round2nearest_multiple(
                target_width, self.padding_constant)
            target_height = self.round2nearest_multiple(
                target_height, self.padding_constant)

            # resize images
            img_resized = imresize(
                img, (target_width, target_height), interp='bilinear')

            # image transform, to torch float tensor 3xHxW
            img_resized = self.img_transform(img_resized)
            img_resized = torch.unsqueeze(img_resized, 0)
            img_resized_list.append(img_resized)

        # segm transform, to torch long tensor HxW
        segm = self.segm_transform(segm)
        batch_segms = torch.unsqueeze(segm, 0)

        output = dict()
        output['img_ori'] = np.array(img)
        output['img_data'] = [x.contiguous() for x in img_resized_list]
        output['seg_label'] = batch_segms.contiguous()
        output['info'] = this_record['fpath_img']
        return output

    def __len__(self):
        return self.num_sample



class TestDataset(BaseDataset):
    def __init__(self, odgt, opt, cfg, **kwargs):
        super(TestDataset, self).__init__(odgt, opt, cfg, **kwargs)        

    def __getitem__(self, index):
        this_record = self.list_sample[index]
        # load image
        # image_path = this_record['fpath_img']
        # img = Image.open(image_path).convert('RGB')
        img = this_record["img_pil_object"]
        ori_width, ori_height = img.size

        img_resized_list = []
        for this_short_size in self.imgSizes:
            # calculate target height and width
            scale = min(this_short_size / float(min(ori_height, ori_width)),
                        self.imgMaxSize / float(max(ori_height, ori_width)))
            target_height, target_width = int(
                ori_height * scale), int(ori_width * scale)

            # to avoid rounding in network
            target_width = self.round2nearest_multiple(
                target_width, self.padding_constant)
            target_height = self.round2nearest_multiple(
                target_height, self.padding_constant)

            # resize images
            img_resized = imresize(
                img, (target_width, target_height), interp='bilinear')

            # image transform, to torch float tensor 3xHxW
            img_resized = self.img_transform(img_resized)
            img_resized = torch.unsqueeze(img_resized, 0)
            img_resized_list.append(img_resized)

        output = dict()
        output['img_ori'] = np.array(img)
        output['img_data'] = [x.contiguous() for x in img_resized_list]
        output['info'] = this_record['fpath_img']
        output['position'] = this_record['position'] 
        output['full_bounds'] = this_record['full_bounds']
        output['out_path'] = this_record['out_path']       
        return output

    def __len__(self):
        return self.num_sample