# System libs
import os
import time
import argparse
from distutils.version import LooseVersion
# Numerical libs
import numpy as np
import torch
import torch.nn as nn
from scipy.io import loadmat
# Our libs
from config import cfg
from dataset import WandbDataset
from models import ModelBuilder, SegmentationModule
from utils import AverageMeter, colorEncode, accuracy, intersectionAndUnion, setup_logger
from lib.nn import user_scattered_collate, async_copy_to
from lib.utils import as_numpy
from PIL import Image
from tqdm import tqdm
import cv2
import wandb #wandb lib for weights and biases

def evaluate(segmentation_module, dataset, cfg, gpu, epoch_id, writer):
    

    class_labels = {0:"tree",1:"road",2:"parking",3:"solar",4:"building"} #class labels for wandb
    examples = []
    for i in range(0,len(dataset)):
        batch_data = dataset[i]
        seg_label = as_numpy(batch_data['seg_label'][0]).astype(np.uint8)
        img_resized_list = batch_data['img_data']
        torch.cuda.synchronize()
        tic = time.perf_counter()
        with torch.no_grad():
            segSize = (seg_label.shape[0], seg_label.shape[1])
            scores = torch.zeros(1, cfg.DATASET.num_class, segSize[0], segSize[1])
            scores = async_copy_to(scores, gpu)

            for img in img_resized_list:
                feed_dict = batch_data.copy()
                feed_dict['img_data'] = img
                del feed_dict['img_ori']
                del feed_dict['info']
                feed_dict = async_copy_to(feed_dict, gpu)

                # forward pass
                # scores_tmp = segmentation_module(feed_dict, segSize=segSize)
                scores_tmp, loss = segmentation_module(feed_dict, segSize=segSize)
                loss = loss.mean()
                scores = scores + scores_tmp / len(cfg.DATASET.imgSizes)

            _, pred = torch.max(scores, dim=1)
            pred = as_numpy(pred.squeeze(0).cpu()).astype(np.uint8)

            imgg = batch_data['img_ori']
            seg = seg_label
            #wandb every image is appending examples list
            examples.append(wandb.Image(imgg, masks={
                        "predictions" : {
                            "mask_data" : pred,
                            "class_labels" : class_labels
                        },
                        "ground_truth" : {
                        "mask_data" : seg,
                        "class_labels" : class_labels
                        }}))
            
    #Code that logs images 
    wandb.log({"Prediction":examples})


        
def main(cfg, gpu, epoch_id, writer):
# def main(cfg, gpu):

    #WANDB
    #wandb.init()
    #wandb.config.update(cfg)

    torch.cuda.set_device('cuda:0')

    # absolute paths of model weights
    cfg.MODEL.weights_encoder = os.path.join(
    cfg.DIR, 'encoder_epoch_' + epoch_id + '.pth')
    cfg.MODEL.weights_decoder = os.path.join(
        cfg.DIR, 'decoder_epoch_' + epoch_id + '.pth')
    
    assert os.path.exists(cfg.MODEL.weights_encoder) and \
        os.path.exists(cfg.MODEL.weights_decoder), "checkpoint does not exitst!"
    assert os.path.exists(
            cfg.MODEL.weights_decoder), "checkpoint does not exitst!"

    # Network Builders
    net_encoder = ModelBuilder.build_encoder(
	cfg,
        arch=cfg.MODEL.arch_encoder.lower(),
        fc_dim=cfg.MODEL.fc_dim,
        weights=cfg.MODEL.weights_encoder)    
    net_decoder = ModelBuilder.build_decoder(
        arch=cfg.MODEL.arch_decoder.lower(),
        fc_dim=cfg.MODEL.fc_dim,
        num_class=cfg.DATASET.num_class,
        weights=cfg.MODEL.weights_decoder,
        use_softmax=True)
    # torch.save(net_encoder.state_dict(),'/home/dorado/Desktop/Old_insurtech_data/encoder_epoch_43.pth')
    # torch.save(net_decoder.state_dict(),'/home/dorado/Desktop/Old_insurtech_data/decoder_epoch_43.pth')
    # import ipdb; ipdb.set_trace()
    # crit = nn.NLLLoss(ignore_index=-1)
    loss_weights_list = cfg.TRAIN.loss_weights
    if (all(loss_weight == loss_weights_list[0] for loss_weight in loss_weights_list)):
        print("all loss weights are equal")
        crit = nn.NLLLoss(ignore_index=-1)
    else:
        loss_weights = torch.tensor(loss_weights_list)     
        crit = nn.NLLLoss(ignore_index=-1, weight = loss_weights)

    segmentation_module = SegmentationModule(net_encoder, net_decoder, crit)
    #wandb.watch(segmentation_module,log="all")
    # Dataset and Loader
    dataset_val = WandbDataset(
        cfg.DATASET.root_dataset,
        cfg.DATASET.list_wandb,
        cfg.DATASET,
        cfg)
    
    segmentation_module.cuda()

    # Main loop
    evaluate(segmentation_module, dataset_val, cfg, gpu, epoch_id, writer)    

    print('Evaluation Done!')


if __name__ == '__main__':
    assert LooseVersion(torch.__version__) >= LooseVersion('0.4.0'), \
        'PyTorch>=0.4.0 is required'

    parser = argparse.ArgumentParser(
        description="PyTorch Semantic Segmentation Validation"
    )
    parser.add_argument(
        "--cfg",
        default="config/ade20k-resnet50dilated-ppm_deepsup.yaml",
        metavar="FILE",
        help="path to config file",
        type=str,
    )
    parser.add_argument(
        "--gpu",
        default=0,
        help="gpu to use"
    )
    parser.add_argument(
        "opts",
        help="Modify config options using the command-line",
        default=None,
        nargs=argparse.REMAINDER,
    )
    args = parser.parse_args()

    cfg.merge_from_file(args.cfg)
    cfg.merge_from_list(args.opts)
    # cfg.freeze()

    logger = setup_logger(distributed_rank=0)   # TODO
    logger.info("Loaded configuration file {}".format(args.cfg))
    logger.info("Running with config:\n{}".format(cfg))
    from torch.utils.tensorboard import SummaryWriter
    writer = SummaryWriter()

    # absolute paths of model weights
    # cfg.MODEL.weights_encoder = os.path.join(
    #     cfg.DIR, 'encoder_' + cfg.VAL.checkpoint)
    # cfg.MODEL.weights_decoder = os.path.join(
    #     cfg.DIR, 'decoder_' + cfg.VAL.checkpoint)
    # assert os.path.exists(cfg.MODEL.weights_encoder) and \
    #     os.path.exists(cfg.MODEL.weights_decoder), "checkpoint does not exitst!"
    epoch_id = cfg.VAL.checkpoint.split(".")[0].split("_")[-1]

    if not os.path.isdir(os.path.join(cfg.DIR, "result_epoch_" + epoch_id)):
        print("not directory")
        os.makedirs(os.path.join(cfg.DIR, "result_epoch_" + epoch_id))           

    main(cfg, args.gpu, epoch_id, writer)
