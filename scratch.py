import sys
sys.path.append("Shift_Net_pytorch")
from data.data_loader import CreateDataLoader
from options.train_options import TrainOptions

opt = TrainOptions().parse()


data_loader = CreateDataLoader(opt)
dataset = data_loader.load_data()