import cv2 as cv,numpy as np,os
from os.path import *
from glob import glob
from tqdm import tqdm

def replaceBlackPixels(x: np.array, replaceValue: int = 2) -> np.array:
    tmp = x.copy()
    tmp[x == 0] = replaceValue
    return tmp


def maskInputImageWithAnnotations(img: np.array or str, annotation: np.array or str,
                                label_pixel: int, replaceBlackPexelValue: int = 2,
                                maskValue : int = 0) -> np.array:
    x = img.copy()
    x[annotation == label_pixel] = maskValue
    return x


if __name__ == "__main__":
    dir = '/home/pictor/workspace/Akhil/Confidence/Data/ResTrueGreenV2'
    imgsDir = 'clipped_tifs'
    masksDir = 'gt_3_classes'
    imgsExt,masksExt = '.tif','_gt.tif'
    outDir = '/home/pictor/workspace/Akhil/Confidence/Data/RecData'
    saveMaskedGrayImageDir = join(outDir,'masked_greyscale_inputs')
    saveMaskedColorImageDir = join(outDir,'masked_color_inputs')
    input_to_grayscale = True
    label_pixel = 2
    replaceBlackPexelValue = 0
    maskValue = 255
    os.makedirs(join(outDir,'color_inputs'),exist_ok=True)
    if input_to_grayscale:
        os.makedirs(join(outDir,'grayscale_inputs'),exist_ok=True)

    os.makedirs(saveMaskedGrayImageDir,exist_ok=True)
    os.makedirs(saveMaskedColorImageDir,exist_ok=True)

    imgsPth = join(dir, imgsDir)
    annotationsPth = join(dir, masksDir)
    imgs = glob(imgsPth+f"/*{imgsExt}")
    annotations = list(map(lambda x: x.replace(imgsDir,masksDir).replace(imgsExt, masksExt),imgs))

    for imagePth,annotPth in tqdm(zip(imgs,annotations)):
        img = cv.imread(imagePth)
        annot = cv.imread(annotPth,0)
        imgNm = basename(imagePth)
        if replaceBlackPexelValue:
            img = replaceBlackPixels(img, replaceValue=replaceBlackPexelValue)

        maskedColorInput = maskInputImageWithAnnotations(img = img,
                                                        annotation = annot,
                                                        label_pixel= label_pixel,
                                                        replaceBlackPexelValue= replaceBlackPexelValue,
                                                        maskValue=maskValue)
        if input_to_grayscale: 
            gray_img = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
            maskedGrayInput = cv.cvtColor(maskedColorInput,cv.COLOR_BGR2GRAY)
            cv.imwrite(join(outDir,'grayscale_inputs', imgNm),gray_img)
            cv.imwrite(join(saveMaskedGrayImageDir,imgNm),maskedGrayInput)
    
        cv.imwrite(join(saveMaskedColorImageDir,imgNm),maskedColorInput)
        cv.imwrite(join(outDir,'color_inputs', imgNm),img)