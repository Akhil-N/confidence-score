import torch
from torch.utils.data import Dataset
from skimage import io
import random,numpy as np
from PIL import Image

def binary_acc(y_pred, y_test):
    y_pred_tag = torch.log_softmax(y_pred, dim = 1)
    _, y_pred_tags = torch.max(y_pred_tag, dim = 1)
    correct_results_sum = (y_pred_tags == y_test).sum().float()
    acc = correct_results_sum/y_test.shape[0]
    acc = torch.round(acc * 100)
    return acc
    
class RegDataset(Dataset):

    def __init__(self, df, transform=None,
                    labelCol='label',
                    modelling = 'classification',
                    device = torch.device('cpu'),
                    flip = False):
        
        self.data_df = df
        self.labelCol = labelCol
        self.transform = transform
        self.modelling = modelling
        self.device = device
        self.flip = flip

    def __len__(self):
        return len(self.data_df)

    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()

        input_img_pth = self.data_df.iloc[idx].InputImg
        rec_path = self.data_df.iloc[idx].RecInput
        pred_mask_path = self.data_df.iloc[idx].predMask
        
        inpt_image = io.imread(input_img_pth)
        rec_image = io.imread(rec_path)
        pred_mask_image = io.imread(pred_mask_path)
        diffImg = inpt_image-rec_image

        inpt_image = Image.fromarray(inpt_image)
        pred_mask_image = Image.fromarray(pred_mask_image)
        diffImg = Image.fromarray(diffImg)

        if self.flip and np.random.random()>0.5:
            aug = random.choice([Image.ROTATE_270,Image.ROTATE_180,Image.ROTATE_90,Image.FLIP_TOP_BOTTOM,Image.FLIP_LEFT_RIGHT])
            inpt_image.transpose(aug)
            pred_mask_image.transpose(aug)
            diffImg.transpose(aug)
        
        sample = {
            'image': inpt_image,
            'diff':diffImg,
            'pred_mask' : pred_mask_image,
            'Y': self.data_df.iloc[idx][self.labelCol]
            }
        if self.transform:
            sample['image'] = self.transform['preprocess_rgb'](sample['image']).to(self.device)

            sample['diff'] = self.transform['preprocess_rgb'](sample['diff']).to(self.device)

            sample['pred_mask'] = self.transform['preprocess_grayscale'](sample['pred_mask']).to(self.device)

        return sample